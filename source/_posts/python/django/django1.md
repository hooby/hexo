---
title: django的使用1-简介
layout: page
date: 2017/1/30
updated: 2017/2/13
comments: false
tags: 
  - django
categories: 
  - Python
---

<!-- toc -->

[TOC]

# 简介

Django 是一个用python语言写的**web开发框架**，最初被设计用于具有快速开发需求的新闻类站点，目的是要实现简单快捷的网站开发。
著名的MVC模式：所谓MVC就是把web应用分为模型(M),控制器(C),视图(V)三层；他们之间以一种插件似的，松耦合的方式连接在一起。

模型负责业务对象与数据库的对象(ORM),视图负责与用户的交互(页面)，控制器(C)接受用户的输入调用模型和视图完成用户的请求。

Django的MTV模式本质上与MVC模式没有什么差别，也是各组件之间为了保持松耦合关系，只是定义上有些许不同，Django的MTV分别代表：

- Model(模型)：负责业务对象与数据库的对象(ORM)
- Template(模版)：负责如何把页面展示给用户
- View(视图)：负责业务逻辑，并在适当的时候调用Model和Template

此外，Django还有一个url分发器，它的作用是将一个个URL的页面请求分发给不同的view处理，view再调用相应的Model和Template



## Django安装与基本使用

### Django安装

```
pip install django
```

### 创建项目

```
django-admin startproject your-project-name

# example -----------------------------------------------------------
(dj) alex@Alex:~/github$ django-admin startproject my_project
(dj) alex@Alex:~/github$ tree my_project
my_project
├── manage.py
└── my_project
    ├── asgi.py
    ├── __init__.py
    ├── settings.py
    ├── urls.py
    └── wsgi.py

```

这些目录和文件的用处是：

- manage.py: 一个让你用各种方式管理 Django 项目的命令行工具
- my_project/ 目录包含你的项目，它是一个纯 Python 包。它的名字就是当你引用它内部任何东西时需要用到的 Python 包名。 
- my_project/__init__.py：一个空文件，告诉 Python 这个目录应该被认为是一个 Python 包。
- my_project/settings.py：Django 项目的配置文件。
- my_project/urls.py：Django 项目的 URL 声明，路由配置。
- my_project/asgi.py：作为你的项目的运行在 ASGI 兼容的 Web 服务器上的入口。
- my_project/wsgi.py：作为你的项目的运行在 WSGI 兼容的Web服务器上的入口。

### 创建应用

```
python manage.py startapp app01

# example -----------------------------------------------------------
(dj) alex@Alex:~/github$ cd my_project
(dj) alex@Alex:~/github/my_project$ ls
manage.py  my_project
(dj) alex@Alex:~/github/my_project$ python manage.py startapp app01
(dj) alex@Alex:~/github/my_project$ tree 
.
├── app01
│   ├── admin.py
│   ├── apps.py
│   ├── __init__.py
│   ├── migrations
│   │   └── __init__.py
│   ├── models.py
│   ├── tests.py
│   └── views.py
├── manage.py
└── my_project
    ├── asgi.py
    ├── __init__.py
    ├── settings.py
    ├── urls.py
    └── wsgi.py

```

### 启动项目

```
python manage.py runserver

# example -----------------------------------------------------------
(dj) alex@Alex:~/github/my_project$ python manage.py runserver
Watching for file changes with StatReloader
Performing system checks...
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```

这里省略了host 和port的设置。`python manage.py runserver 0.0.0.0:8000`



### manage.py 命令

项目中大量关于项目和应用的管理都可以通过 `python manage.py ...` 来处理。

```
(dj) alex@Alex:~/github/my_project$ python manage.py 

Type 'manage.py help <subcommand>' for help on a specific subcommand.

Available subcommands:

[auth]
    changepassword
    createsuperuser

[contenttypes]
    remove_stale_contenttypes

[django]
    check
    compilemessages
    createcachetable
    dbshell
    diffsettings
    dumpdata
    flush
    inspectdb
    loaddata
    makemessages
    makemigrations
    migrate
    sendtestemail
    shell
    showmigrations
    sqlflush
    sqlmigrate
    sqlsequencereset
    squashmigrations
    startapp
    startproject
    test
    testserver

[sessions]
    clearsessions

[staticfiles]
    collectstatic
    findstatic
    runserver

```



## Model(模型)

Django使用一种叫[对象关系映射器](https://en.wikipedia.org/wiki/Object-relational_mapping)的技术,来处理数据和模型之间的的关系。

简单粗暴的理解是Django的模型就类似于MySQL的table。一个class就是描述一个table；而一个实例，就是table里面的一条数据；而类的方法就是对数据的处理。

### 设计模型

```
文件：my_project/my_project/models.py

from django.db import models

class Reporter(models.Model):
    full_name = models.CharField(max_length=70)

    def __str__(self):
        return self.full_name

class Article(models.Model):
    pub_date = models.DateField()
    headline = models.CharField(max_length=200)
    content = models.TextField()
    reporter = models.ForeignKey(Reporter, on_delete=models.CASCADE)

    def __str__(self):
        return self.headline
```

这里定义了一个reporter的table， 其中有一个字段full_name.



### 应用数据模型

接下来，运行 Django 命令行实用程序以自动创建数据库表：

```
$ python manage.py makemigrations
$ python manage.py migrate
```

这里他会根据 `my_project/settings.py` 里面配置的数据连接，然后创建数据库和表。



### 享用便捷的 API

这里的意思就是Model有很多个方法，这些方法可以很方便的对数据进行处理。下面通过manage.py shell来演示一下:

```
python manage.py shell
>>> from news.models import Article, Reporter
>>> Reporter.objects.all()
<QuerySet []>

# Create a new Reporter.
>>> r = Reporter(full_name='John Smith')
>>> r.save()
>>> r.id
1
>>> Reporter.objects.all()
<QuerySet [<Reporter: John Smith>]>
>>> r.full_name
'John Smith'
>>> Reporter.objects.get(id=1)
<Reporter: John Smith>


# Create an article.
>>> from datetime import date
>>> a = Article(pub_date=date.today(), headline='Django is cool',
...     content='Yeah.', reporter=r)
>>> a.save()
>>> Article.objects.all()
<QuerySet [<Article: Django is cool>]>
>>> r = a.reporter
>>> r.full_name
'John Smith'
>>> r.article_set.all()
<QuerySet [<Article: Django is cool>]>
>>> r.delete()
```



## URLconf 

简单来说，URLconf 就是路由。将不同的请求连接到不同的 View(视图) 上面。可以有很多种匹配方式。下面这个 URLconf 适用于前面 `Reporter`/`Article` 的例子：

```
文件：my_project/my_project/urls.py

from django.urls import path

from . import views

urlpatterns = [
    path('articles/<int:year>/', views.year_archive),
    path('articles/<int:year>/<int:month>/', views.month_archive),
    path('articles/<int:year>/<int:month>/<int:pk>/', views.article_detail),
]
```



## View(视图)

通常来说，一个视图的工作就是：从参数获取数据，装载一个模板，然后将根据获取的数据对模板进行渲染。下面是一个 `year_archive` 的视图样例：

```
文件：my_project/app01/views.py

from django.shortcuts import render

from .models import Article

def year_archive(request, year):
    a_list = Article.objects.filter(pub_date__year=year)
    context = {'year': year, 'article_list': a_list}
    return render(request, 'news/year_archive.html', context)
```



## Template(模版)

上面的代码加载了 `news/year_archive.html` 模板。

模板就是要返回给用户的页面框架，这个模板可以传递很多参数，比如一个博客的首页。它需要很多条文章标题来丰富这个页面，所以传递的参数就可以是一系列文章的标题。 这样模板和内容就独立开来了。最后吧模板和内容加载一起渲染一下，就可以返回到浏览器给用户看了。

例如上面的模板可以是这样：

```
{% extends "base.html" %}

{% block title %}Articles for {{ year }}{% endblock %}

{% block content %}
<h1>Articles for {{ year }}</h1>

{% for article in article_list %}
    <p>{{ article.headline }}</p>
    <p>By {{ article.reporter.full_name }}</p>
    <p>Published {{ article.pub_date|date:"F j, Y" }}</p>
{% endfor %}
{% endblock %}
```

这个模板有一些语法，比如这个模板其实是继承了`base.html`模板，里面的for轮训，if判断。变量传递等。





参考：

- https://docs.djangoproject.com/





