---
title: django的使用3-编写视图
layout: page
date: 2017/2/12
updated: 2017/4/2
comments: false
tags: 
  - django
categories: 
  - Python
---

<!-- toc -->

[TOC]

# 编写视图

一个视图函数（或简称为视图）是一个 Python 函数，它接受 Web 请求并返回一个 Web 响应。这个响应可以是 Web 页面的 HTML 内容，或者重定向，或者404错误，或者 XML 文档，或一个图片...或是任何内容。视图本身包含返回响应所需的任何逻辑。这个代码可以存在任何地方，只要它在你的 Python 路径上就行。可以说，不需要其他东西，这里并没有魔法。为了将代码放置在某处，约定将视图放在名为 `views.py` 的文件里，这个文件放置在项目或应用目录里。



## 一个简单的视图








参考：

- https://docs.djangoproject.com/





