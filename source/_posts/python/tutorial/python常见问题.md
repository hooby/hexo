---
title:  Python常见问题总结
layout: page
date: 2016/4/16
updated: 2016/6/13
comments: false
mathjax: true
tags: 
- python
categories: 
- Python基础
---

<!-- toc -->

[TOC]

# 1.lambda

Python 内置函数

------

## 描述

python 使用 lambda 来创建匿名函数。

## 语法

lambda 函数的语法只包含一个语句，如下：

```
lambda [arg1 [,arg2,.....argn]]:expression
```

## 注意

```
lambda (x, y): x + y
# tuple parameter unpacking is not supported in python3
```

这样的使用在python3中已经被废弃了，会提示上面注释中的错误

那么元组tuple如何作为参数呢，will be translated into:

```
lambda x_y: x_y[0] + x_y[1]
# 即在传入参数的时候，整个变量作为元组的参数，使用的时候，引用整个元组的位置参数
```



# 2.map()

Python 内置函数

------

## 描述

**map()** 会根据提供的函数对指定序列做映射。

第一个参数 function 以参数序列中的每一个元素调用 function 函数，返回包含每次 function 函数返回值的新列表。

## 语法

map() 函数语法：

```
map(function, iterable, ...)
```

## 参数

- function -- 函数，有两个参数
- iterable -- 一个或多个序列

## 返回值

Python 2.x 返回列表。

Python 3.x 返回迭代器。

## 注意

```
def f(x, y): return (x, y)
l1 = [ 0, 1, 2, 3, 4, 5, 6 ]
l2 = [ 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat' ]

```

在python2中：

```
map(f, l1, l2)  
[(0, 'Sun'), (1, 'Mon'), (2, 'Tue'), (3, 'Wed'), (4, 'Thu'), (5, 'Fri'), (6, 'Sat')]  
```

在python3中：

```
map(f, l1, l2)  
<map object at 0x000001B5C5B79198>
# python3 map返回的是一个iterable
# 所以在python3中要记得把iterable给list化
list(map(f, l1, l2))
[(0, 'Sun'), (1, 'Mon'), (2, 'Tue'), (3, 'Wed'), (4, 'Thu'), (5, 'Fri'), (6, 'Sat')]
```

# 3.reduce()

------

## 描述

**reduce()** 函数会对参数序列中元素进行累积。

函数将一个数据集合（链表，元组等）中的所有数据进行下列操作：用传给 reduce 中的函数 function（有两个参数）先对集合中的第 1、2 个元素进行操作，得到的结果再与第三个数据用 function 函数运算，最后得到一个结果。

## 语法

reduce() 函数语法：

```
reduce(function, iterable[, initializer])
```

## 参数

- function -- 函数，有两个参数
- iterable -- 可迭代对象
- initializer -- 可选，初始参数

## 返回值

返回函数计算结果。



在 Python3 中，reduce() 函数已经被从全局名字空间里移除了，它现在被放置在 fucntools 模块里，如果想要使用它，则需要通过引入 functools 模块来调用 reduce() 函数：

```
from functools import reduce
```

**实例：**

```
from functools import reduce
def add(x,y):
    return x + y
print (reduce(add, range(1, 101)))
```

输出结果为：

```
5050
```