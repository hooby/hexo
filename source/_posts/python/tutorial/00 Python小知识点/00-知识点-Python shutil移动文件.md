---
title:  Python shutil移动文件
layout: page
date: 2016/3/29
updated: 2016/6/13
comments: false
tags: 
- python
categories: 
- Python基础
---

<!-- toc -->

[TOC]

## Python 终端输出字体设置



shutil可以实现文件的复制，移动

```python
# 复制文件：
shutil.copyfile("oldfile","newfile") 
# oldfile和newfile都只能是文件

shutil.copy("oldfile","newfile") 
# oldfile只能是文件夹，newfile可以是文件，也可以是目标目录
 
# 复制文件夹：
shutil.copytree("olddir","newdir") 
# olddir和newdir都只能是目录，且newdir必须不存在
 
# 重命名文件（目录）
os.rename("oldname","newname") 
# 文件或目录都是使用这条命令
 
# 移动文件（目录）
shutil.move("oldpos","newpos") 


```

