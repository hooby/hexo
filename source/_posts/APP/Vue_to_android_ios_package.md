---
title: Vue项目打包成Android和iOS安装包
layout: page
date: 2019/10/13
updated: 2019/10/13
comments: false
tags: 
- Vue
- cordova
- Android
- iOS
categories: 
- cordova
---

<!-- toc -->

[TOC]

## 准备环境配置

准备环境需要安装`Android Sdk ` , `jdk` , `Gradle` 



### 安装Android Sdk

Android Sdk 可以通过安装 android-studio， 然后再编辑器里面选择安装SDK即可。

也可以通过sdk-tools来安装，这种方式无需界面，对于远程Linux的操作非常方便。下面介绍这种方式：

下载sdk-tolls: https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip

解压到指定目录

```
cd /home/alex/android-sdk
unzip sdk-tools-linux-4333796.zip
```

编辑 `~/.profile` 配置环境变量

```
export ANDROID_HOME=/home/alex/android-sdk
export PATH=$ANDROID_HOME/platform-tools:$ANDROID_HOME/tools:$ANDROID_HOME/tools/bin:$PATH
```

**sdkmanager --list**即可查看所有已经安装的和未安装的packages，如下：

```
Installed packages:
  Path                              | Version | Description                       | Location                         
  -------                           | ------- | -------                           | -------                          
  build-tools;24.0.3                | 24.0.3  | Android SDK Build-Tools 24.0.3    | build-tools/24.0.3/              
  build-tools;25.0.3                | 25.0.3  | Android SDK Build-Tools 25.0.3    | build-tools/25.0.3/              
  emulator                          | 26.1.3  | Android Emulator                  | emulator/                        
  extras;android;m2repository       | 47.0.0  | Android Support Repository        | extras/android/m2repository/     
  extras;google;m2repository        | 57      | Google Repository                 | extras/google/m2repository/      
  extras;m2reposi...ut-solver;1.0.2 | 1       | Solver for ConstraintLayout 1.0.2 | extras/m2reposi...t-solver/1.0.2/
  extras;m2reposi...nt-layout;1.0.2 | 1       | ConstraintLayout for Android 1... | extras/m2reposi...t-layout/1.0.2/
  patcher;v4                        | 1       | SDK Patch Applier v4              | patcher/v4/                      
  platform-tools                    | 26.0.0  | Android SDK Platform-Tools 26     | platform-tools/                  
  platforms;android-24              | 2       | Android SDK Platform 24           | platforms/android-24/            
  platforms;android-25              | 3       | Android SDK Platform 25           | platforms/android-25/            
  tools                             | 26.0.2  | Android SDK Tools                 | tools/                           
 
Available Packages:
  Path                              | Version      | Description                      
  -------                           | -------      | -------                          
  add-ons;addon-g..._apis-google-15 | 3            | Google APIs                      
  add-ons;addon-g..._apis-google-16 | 4            | Google APIs                      
  add-ons;addon-g..._apis-google-17 | 4            | Google APIs                      
  add-ons;addon-g..._apis-google-18 | 4            | Google APIs                      
  add-ons;addon-g..._apis-google-19 | 20           | Google APIs                      
  add-ons;addon-g..._apis-google-21 | 1            | Google APIs                      
  add-ons;addon-g..._apis-google-22 | 1            | Google APIs                      
  add-ons;addon-g..._apis-google-23 | 1            | Google APIs                      
  add-ons;addon-g..._apis-google-24 | 1            | Google APIs                      
  add-ons;addon-g...e_gdk-google-19 | 11           | Glass Development Kit Preview    
  build-tools;19.1.0                | 19.1.0       | Android SDK Build-Tools 19.1     
  build-tools;20.0.0                | 20.0.0       | Android SDK Build-Tools 20       
  build-tools;21.1.2                | 21.1.2       | Android SDK Build-Tools 21.1.2   
  build-tools;22.0.1                | 22.0.1       | Android SDK Build-Tools 22.0.1   
  build-tools;23.0.1                | 23.0.1       | Android SDK Build-Tools 23.0.1   
  build-tools;23.0.2                | 23.0.2       | Android SDK Build-Tools 23.0.2   
  build-tools;23.0.3                | 23.0.3       | Android SDK Build-Tools 23.0.3   
  build-tools;24.0.0                | 24.0.0       | Android SDK Build-Tools 24       
  build-tools;24.0.1                | 24.0.1       | Android SDK Build-Tools 24.0.1   
  build-tools;24.0.2                | 24.0.2       | Android SDK Build-Tools 24.0.2   
  build-tools;24.0.3                | 24.0.3       | Android SDK Build-Tools 24.0.3   
  build-tools;25.0.0                | 25.0.0       | Android SDK Build-Tools 25       
  build-tools;25.0.1                | 25.0.1       | Android SDK Build-Tools 25.0.1   
  build-tools;25.0.2                | 25.0.2       | Android SDK Build-Tools 25.0.2   
  build-tools;25.0.3                | 25.0.3       | Android SDK Build-Tools 25.0.3   
  build-tools;26.0.0                | 26.0.0       | Android SDK Build-Tools 26       
  build-tools;26.0.1                | 26.0.1       | Android SDK Build-Tools 26.0.1   
  cmake;3.6.4111459                 | 3.6.4111459  | CMake 3.6.4111459                
  docs                              | 1            | Documentation for Android SDK    
  emulator                          | 26.1.3       | Android Emulator                 
  extras;android;gapid;1            | 1.0.3        | GPU Debugging tools              
  extras;android;gapid;3            | 3.1.0        | GPU Debugging tools              
  extras;android;m2repository       | 47.0.0       | Android Support Repository       
  extras;google;auto                | 1.1          | Android Auto Desktop Head Unit...
  extras;google;g...e_play_services | 43           | Google Play services
```





- `下载地址`  `http://www.android-studio.org/` 解压到/usr/local/目录下 
- `运行` 到android-studio文件夹下的bin `./studio.sh`
- 配置`Android SDK`环境变量,`vim ~/.profile`中加入下面代码

```
export ANDROID_HOME=$HOME/Android/Sdk (不存在请单独安装Android Sdk)
export PATH=${PATH}:${ANDROID_HOME}/tools
export PATH=${PATH}:${ANDROID_HOME}/platform-tools
```

**sdkmanager --install**

我们可以通过sdkmanager --install命令来进行packages组件的安装
sdkmanager --install "platforms;android-26"
sdkmanager --install "build-tools;25.0.3" 
sdkmanager --install "extras;google;m2repository"
sdkmanager --install "extras;android;m2repository" 

安装完成后，Android sdk目录结构如下所示：

```
total 32
drwxr-xr-x 4 jenkins root 4096 Aug 15 17:30 build-tools
drwxr-xr-x 7 jenkins root 4096 Aug 14 15:52 emulator
drwxr-xr-x 5 jenkins root 4096 Aug 15 17:31 extras
drwxr-xr-x 2 jenkins root 4096 Aug 14 15:52 licenses
drwxr-xr-x 3 jenkins root 4096 Aug 14 15:52 patcher
drwxr-xr-x 4 jenkins root 4096 Aug 15 17:30 platforms
drwxr-xr-x 5 jenkins root 4096 Aug 14 15:08 platform-tools
drwxr-xr-x 6 jenkins root 4096 Aug 14 15:52 tools
```

**sdkmanager --help**

我们可以通过sdkmanager --help命令来查看其帮助



### 安装 java jdk

- `下载地址` `https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html`
- 解压到`/usr/local/`目录下
- 配置Java jdk的环境变量 `vim /etc/profile`

```
export JAVA_HOME=/usr/local/jdk1.8.0_231
export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
export PATH=$JAVA_HOME/bin:$PATH
```

- `激活` `source /etc/profile`
- 检查是否安装成功 `终端输入java和javac`



### 安装Gradle

- `必须满足要求` `Java JDK或JRE版本8或更高版本`
- `下载地址` `https://services.gradle.org/distributions/ ` gradle-x.x-bin.zip
- `创建目录并解压` `mkdir /opt/gradle` 
- `配置环境变量` `vim /etc/profile` 

```
export PATH=$PATH:/opt/gradle/gradle-x.xx/bin
```

- `激活` `source /etc/profile`



### 安装cordova与vue3.0

- cordova `sudo npm install -g cordova`
- vue3.0  `sudo npm install -g @vue/cli@3.0`  检查 `vue -V`



## APP打包设置



### 前端代码打包静态文件

1. 白屏，在前端工程文件`vue.config.js` 添加 

```
module.exports = {
  publicPath: './',
```

2. 在前端工程文件 `/Projects/src/router.js` 修改 `mode: "hash"`

3. 调用不了后端接口，前端工程文件中

  `Projects/src/main.js` 中加

```
axios.defaults.baseURL = 'https://backend.server.com'
```

4. 生成静态文件 `dist` 目录

   ```
   npm run bulid
   ```



### 创建cordova项目,并打包

- `cordova create app_name com.dls package_name`  (app_name为目录名称,  `com.dls.package_name` 为 `包名`)
- 将生成的静态文件`dist`，移动到cordova项目的app_name目录下,改名为 `www `
- 在soober目录下进行打包 `cordova platforms add android --save`



### 检查环境配置

- `到项目根目录下` `cordova requirements` 确保所需环境都对



### 配置 icon 和 启动页

在config.xml 文件中添加如下配置，并将res文件夹放置在Cordova项目（Soober）根目录下

```
<?xml version='1.0' encoding='utf-8'?>
<widget id="com.dls.assistant" version="1.3.0" xmlns="http://www.w3.org/ns/widgets" xmlns:cdv="http://cordova.apache.org/ns/1.0">
    <name>soober</name>
    <description>
        A sample Apache Cordova application that responds to the deviceready event.
    </description>
    <author email="dev@cordova.apache.org" href="http://cordova.io">
        Apache Cordova Team
    </author>
    <content src="index.html" />
    <plugin name="cordova-plugin-whitelist" spec="1" />
    <access origin="*" />
    <allow-navigation href="*" />
    <allow-intent href="http://*/*" />
    <allow-intent href="https://*/*" />
    <allow-intent href="tel:*" />
    <allow-intent href="sms:*" />
    <allow-intent href="mailto:*" />
    <allow-intent href="geo:*" />
    <platform name="android">
        <allow-intent href="market:*" />
        <icon src="res/icon/android/ldpi.png" density="ldpi" />
        <icon src="res/icon/android/mdpi.png" density="mdpi" />
        <icon src="res/icon/android/hdpi.png" density="hdpi" />
        <icon src="res/icon/android/xhdpi.png" density="xhdpi" />
        <icon src="res/icon/android/xxhdpi.png" density="xxhdpi" />
        <icon src="res/icon/android/xxxhdpi.png" density="xxxhdpi" />

        <splash density="hdpi" src="res/screen/android/splash-land-hdpi.png" />  
        <splash density="ldpi" src="res/screen/android/splash-land-ldpi.png" />  
        <splash density="mdpi" src="res/screen/android/splash-land-mdpi.png" />  
        <splash density="xhdpi" src="res/screen/android/splash-land-xhdpi.png" />  
        <splash density="land-hdpi" src="res/screen/android/splash-land-hdpi.png" />  
        <splash density="land-ldpi" src="res/screen/android/splash-land-ldpi.png" />  
        <splash density="land-mdpi" src="res/screen/android/splash-land-mdpi.png" />  
        <splash density="land-xhdpi" src="res/screen/android/splash-land-xhdpi.png" />  
        <splash density="port-hdpi" src="res/screen/android/splash-port-hdpi.png" />  
        <splash density="port-ldpi" src="res/screen/android/splash-port-ldpi.png" />  
        <splash density="port-mdpi" src="res/screen/android/splash-port-mdpi.png" />  
        <splash density="port-xhdpi" src="res/screen/android/splash-port-xhdpi.png" />

        <!-- <preference name="android-manifest/application/activity/@android:theme" value="@style/WelcomeStyle" /> -->

        <!-- <preference name="StatusBarOverlaysWebView" value="true" /> -->
        <!-- <preference name="StatusBarStyle" value="lightContent" /> -->

        <!-- 自动隐藏启动页面AutoHideSplashScreen(默认为: true) -->
        <!-- <preference name="AutoHideSplashScreen" value="false" /> -->

        <!-- 显示启动页面的时间长度SplashScreenDelay(默认为: 3000) -->
        <preference name="SplashScreenDelay" value="1500" />

        <!-- 淡入淡出效果FadeSplashScreen(默认为: true) -->
        <!-- <preference name="FadeSplashScreen" value="false"/> -->
        
        <!-- 淡入淡出效果的执行时间长度FadeSplashScreenDuration(默认为: 500) -->
        <preference name="FadeSplashScreenDuration" value="500"/>

        <!-- 启动页面是否允许旋转(默认为: true) -->
        <preference name="ShowSplashScreenSpinner" value="false"/>


        <!-- 以图片原始比例显示图片，否则拉伸图片来填充 -->
        <preference name="SplashMaintainAspectRatio" value="true" />

        <!-- 设置为false时，在下次打开APP时，还会显示启动页面。 -->
        <!-- <preference name="SplashShowOnlyFirstTime" value="false" /> -->


    </platform>
    <platform name="ios">
        <allow-intent href="itms:*" />
        <allow-intent href="itms-apps:*" />
        <!-- iOS 8.0+ -->
        <!-- iPhone 6 Plus  -->
        <icon src="res/icon/ios/icon-60@3x.png" width="180" height="180" />
        <!-- iOS 7.0+ -->
        <!-- iPhone / iPod Touch  -->
        <icon src="res/icon/ios/icon-60.png" width="60" height="60" />
        <icon src="res/icon/ios/icon-60@2x.png" width="120" height="120" />
        <!-- iPad -->
        <icon src="res/icon/ios/icon-76.png" width="76" height="76" />
        <icon src="res/icon/ios/icon-76@2x.png" width="152" height="152" />
        <!-- Spotlight Icon -->
        <icon src="res/icon/ios/icon-40.png" width="40" height="40" />
        <icon src="res/icon/ios/icon-40@2x.png" width="80" height="80" />
        <!-- iOS 6.1 -->
        <!-- iPhone / iPod Touch -->
        <icon src="res/icon/ios/icon.png" width="57" height="57" />
        <icon src="res/icon/ios/icon@2x.png" width="114" height="114" />
        <!-- iPad -->
        <icon src="res/icon/ios/icon-72.png" width="72" height="72" />
        <icon src="res/icon/ios/icon-72@2x.png" width="144" height="144" />
        <!-- iPad Pro -->
        <icon src="res/icon/ios/icon-167.png" width="167" height="167" />
        <!-- iPhone Spotlight and Settings Icon -->
        <icon src="res/icon/ios/icon-small.png" width="29" height="29" />
        <icon src="res/icon/ios/icon-small@2x.png" width="58" height="58" />
        <icon src="res/icon/ios/icon-small@3x.png" width="87" height="87" />
        <!-- iPad Spotlight and Settings Icon -->
        <icon src="res/icon/ios/icon-50.png" width="50" height="50" />
        <icon src="res/icon/ios/icon-50@2x.png" width="100" height="100" />
        <!-- iPad Pro -->
        <icon src="res/icon/ios/icon-83.5@2x.png" width="167" height="167" />

        <!-- Ohters -->
        <icon src="res/icon/ios/icon-20.png" width="20" height="20" />
        <icon src="res/icon/ios/icon-1024.png" width="1024" height="1024" />


        <splash src="res/screen/ios/Default~iphone.png" width="320" height="480"/>  
        <splash src="res/screen/ios/Default@2x~iphone.png" width="640" height="960"/>  
        <splash src="res/screen/ios/Default-Portrait~ipad.png" width="768" height="1024"/>  
        <splash src="res/screen/ios/Default-Portrait@2x~ipad.png" width="1536" height="2048"/>  
        <splash src="res/screen/ios/Default-Landscape~ipad.png" width="1024" height="768"/>  
        <splash src="res/screen/ios/Default-Landscape@2x~ipad.png" width="2048" height="1536"/>  
        <splash src="res/screen/ios/Default-568h@2x~iphone.png" width="640" height="1136"/>  
        <splash src="res/screen/ios/Default-667h.png" width="750" height="1334"/>  
        <splash src="res/screen/ios/Default-736h.png" width="1242" height="2208"/>  
        <splash src="res/screen/ios/Default-Landscape-736h.png" width="2208" height="1242"/>
        <splash src="res/screen/ios/Default-2436h.png" width="1125" height="2436"/>  
        <splash src="res/screen/ios/Default-Landscape-2436h.png" width="2436" height="1125"/>

        <preference name="StatusBarOverlaysWebView" value="true" />
        <preference name="StatusBarStyle" value="blackTranslucent" />
        <preference name="WebViewBounce" value="false" />
        <preference name="DisallowOverscroll" value="true" />

        <!-- 自动隐藏启动页面AutoHideSplashScreen（默认为: true ） -->
        <preference name="AutoHideSplashScreen" value="true" />

        <!-- 显示启动页面的时间长度SplashScreenDelay(默认为: 3000) -->
        <preference name="SplashScreenDelay" value="0" />

        <!-- 淡入淡出效果FadeSplashScreen(默认为：true) -->
        <!-- <preference name="FadeSplashScreen" value="false"/> -->
        
        <!-- 淡入淡出效果的执行时间长度FadeSplashScreenDuration(默认为：500) -->
        <!-- <preference name="FadeSplashScreenDuration" value="1000"/> -->

        <!-- iOS上禁止启动页面 -->
        <preference name="FadeSplashScreenDuration" value="0"/>

        <!-- 启动页面是否允许旋转（默认为：true） -->
        <!-- <preference name="ShowSplashScreenSpinner" value="false"/> -->
        
        <!-- 使用 WKWebViewEngine -->
        <!-- <feature name="CDVWKWebViewEngine">
            <param name="ios-package" value="CDVWKWebViewEngine" />
        </feature>
        <preference name="CordovaWebViewEngine" value="CDVWKWebViewEngine" /> -->

        <!-- <preference name="AllowUntrustedCerts" value="on" /> -->
        <!-- <preference name="InterceptRemoteRequests" value="all" /> -->
        <!-- <preference name="WKWebViewOnly" value="true" /> -->

    </platform>
</widget>

```

res目录结构:

```
alex@Alex:~/AndroidStudioProjects/app_name$ tree res/
res/
├── icon
│   ├── android
│   │   ├── hdpi.png
│   │   ├── ldpi.png
│   │   ├── mdpi.png
│   │   ├── xhdpi.png
│   │   ├── xxhdpi.png
│   │   └── xxxhdpi.png
│   └── ios
│       ├── icon-20@3x.png
│       ├── icon-29@2x.png
│       ├── icon-29@3x.png
│       ├── icon-29.png
│       ├── icon@2x.png
│       ├── icon-40@2x.png
│       ├── icon-40.png
│       ├── icon-50@2x.png
│       ├── icon-50.png
│       ├── icon-57@2x.png
│       ├── icon-57.png
│       ├── icon-60@2x.png
│       ├── icon-60@3x.png
│       ├── icon-60.png
│       ├── icon-72@2x.png
│       ├── icon-72.png
│       ├── icon-76@2x.png
│       ├── icon-76.png
│       ├── icon.png
│       ├── icon-small@2x.png
│       └── icon-small.png
└── screen
    ├── android
    │   ├── splash-land-hdpi.png
    │   ├── splash-land-ldpi.png
    │   ├── splash-land-mdpi.png
    │   ├── splash-land-xhdpi.png
    │   ├── splash-port-hdpi.png
    │   ├── splash-port-ldpi.png
    │   ├── splash-port-mdpi.png
    │   └── splash-port-xhdpi.png
    └── ios
        ├── Contents.json
        ├── Default-2436h.png
        ├── Default@2x~iphone.png
        ├── Default-568h@2x~iphone.png
        ├── Default-667h.png
        ├── Default-736h.png
        ├── Default~iphone.png
        ├── Default-Landscape-2436h.png
        ├── Default-Landscape@2x~ipad.png
        ├── Default-Landscape-736h.png
        ├── Default-Landscape~ipad.png
        ├── Default-Portrait@2x~ipad.png
        └── Default-Portrait~ipad.png

# 有时候图片会有alpha, 在iOS提交到APP store的时候会被拒绝.解决办法执行如下命令,修改所有icon的png.
# for i in `ls *.png`; do convert $i -background black -alpha remove -alpha off $i; done
```



### Android 启动页 黑屏问题解决

- 修改 `platforms/android/app/src/main/AndroidManifest.xml` 文件

```
android:theme="@style/WelcomeStyle"
```

- 修改 `platforms/android/app/src/main/res/values/styles.xml` 文件

```
<resources>
    <style name="WelcomeStyle" parent="@android:style/Theme.DeviceDefault.NoActionBar">
        <!--指向启动前图片-->
        <item name="android:windowBackground">@drawable/splash</item>
        <item name="android:windowNoTitle">true</item>
        <item name="android:windowFullscreen">false</item>
        <!--不让windowBackground延申到navigation bar区域-->
        <item name="android:windowDrawsSystemBarBackgrounds">false</item>
        <!--适配Android P刘海屏-->
        <item name="android:windowLayoutInDisplayCutoutMode">shortEdges</item>
    </style>
    <style name="Appwelcome" parent="android:Theme.Translucent.NoTitleBar.Fullscreen">
    </style>
</resources>
```



- 添加 `platforms/android/app/src/main/res/drawable/splash.xml`  文件

```
<?xml version="1.0" encoding="utf-8"?>
<layer-list xmlns:android="http://schemas.android.com/apk/res/android"
    android:opacity="opaque">
<!--    <item android:drawable="@android:color/white" />-->
    <item android:gravity="fill">
        <shape android:shape="rectangle">
            <solid android:color="@android:color/white"/>
        </shape>
    </item>
</layer-list>
```



### 浏览器唤醒打开APP

修改 `platforms/android/app/src/main/AndroidManifest.xml` 文件

```
        <activity android:configChanges="orientation|keyboardHidden|keyboard|screenSize|locale|smallestScreenSize|screenLayout|uiMode" android:label="@string/activity_name" android:launchMode="singleTop" android:name="MainActivity" android:theme="@style/WelcomeStyle" android:windowSoftInputMode="adjustResize">
        ...
		<!-- # 添加内容-开始 -->
            <intent-filter>
                <action android:name="android.intent.action.VIEW" />
                <category android:name="android.intent.category.DEFAULT" />
                <category android:name="android.intent.category.BROWSABLE" />
                <data android:scheme="wxea5a9199cd873171" />
            </intent-filter>
         <!-- # 添加内容-结束 -->

        </activity>
```




### 配置权限与安装对应插件

内置打开网页, 打开摄像头, 唤起支付等功能需要安装对应的插件. 同时配置相应的权限.

| 功能            | 插件名称                                    | 需要配置权限 | 其它            |
| ------------- | --------------------------------------- | ------ | ------------- |
| 支付            | cordova-plugin-alipay-v2                |        | 添加appid       |
| 摄像头           | cordova-plugin-camera                   | 是      |               |
| 内置浏览          | cordova-plugin-inappbrowser             |        |               |
| 启动页           | cordova-plugin-splashscreen             |        |               |
|               | cordova-fix-blackscreen                 |        |               |
|               | cordova-plugin-whitelist                |        |               |
| 返回键           | cordova-plugin-background-mode          |        | Android系统返回键  |
| ios web引擎     | cordova-plugin-wkwebview-engine         |        | 有bug先不用       |
|               | cordova-plugin-wkwebview-file-xhr       |        | 有bug先不用       |
| Android web引擎 | cordova-plugin-crosswalk-webview@latest |        | 有bug先不用       |
| 微信支付、登录、分享    | cordova-plugin-wechat                   | 是      | 添加wechatappid |



#### 配置插件

```
cordova plugin add cordova-plugin-alipay-v2 --variable APP_ID=xxx
cordova plugin add cordova-plugin-camera
cordova plugin add cordova-plugin-inappbrowser
cordova plugin add cordova-plugin-splashscreen
cordova plugin add cordova-fix-blackscreen
cordova plugin add cordova-plugin-background-mode
cordova plugin add cordova-plugin-wechat --variable wechatappid=微信APPid
# cordova plugin add cordova-plugin-wkwebview-engine
# cordova plugin add cordova-plugin-crosswalk-webview@latest --save
cordova plugin list
```



#### 配置权限

iOS和Android的权限配置分别在打包目录文件中设置,所以先要生成打包目录文件.



**Android:**

生成打包目录文件`cordova platform add android --save`

在`platforms/android/CordovaLib/AndroidManifest.xml`  文件添加权限

```
<?xml version="1.0" encoding="utf-8"?>

<manifest xmlns:android="http://schemas.android.com/apk/res/android"
      package="org.apache.cordova" android:versionName="1.0" android:versionCode="1">
    <!-- <uses-permission android:name="android.permission.RECEIVE_SMS"/> -->
    <!-- <uses-permission android:name="android.permission.RECORD_AUDIO"/> -->
    <uses-permission android:name="android.permission.CAMERA"/>
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
    <!-- <uses-permission android:name="android.permission.READ_PHONE_STATE"/> -->
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>
    <!-- <uses-permission android:name="android.permission.BROADCAST_SMS"/> -->
    <uses-permission android:name="android.permission.INTERNET"/>
    <!-- <uses-permission android:name="android.permission.READ_SMS"/> -->
    <application android:usesCleartextTraffic="true">
    </application>
</manifest>
```

**iOS:**

生成打包目录文件`cordova platform add ios@latest --save`

在`ios/app_name/app_name-Info.plist`文件中添加

```
<dict
    <!-- <key>NSLocationAlwaysUsageDescription</key>
    <string>程序需要访问您的位置，以便APP始终获取您的位置信息</string> -->
    <key>NSCameraUsageDescription</key>
    <string>程序想要调用你的摄像头,以便APP可以获取图片.</string>
    <key>NSPhotoLibraryUsageDescription</key>
    <string>程序想要访问你的相册,以便APP可以获取图片.</string>
    ...
    <!-- <key>UIBackgroundModes</key>
    <array>
    <string>去掉麦克风使用</string>
    </array> -->
    <key>LSApplicationQueriesSchemes</key>
    <array>
      <string>wechat</string>
      <string>weixin</string>
    </array>
</dict>
```



## 运行和打包apk

在线运行Android项目:

```
cordova run android --device 
```

浏览器调试工具

```
chrome://inspect/#**devices**
```

打包发布apk文件:

```
cordova build android --release
```



Android APP打包keystore：

```
alex@Alex:~/AndroidStudioProjects/my_app$ keytool -genkey -v -keystore ./my_app.keystore -alias my_app -keyalg RSA -validity 3650

alex@Alex:~/AndroidStudioProjects/my_app$ ls
config.xml  hooks  node_modules  package.json  package-lock.json  platforms  plugins  res  my_app.keystore  www
alex@Alex:~/AndroidStudioProjects/my_app$ cat build.json
{
  "android": {
    "release": {
      "keystore": "my_app.keystore",
      "alias": "my_app",
      "storePassword": "storePassword",
      "password": "password"
    }
  }
}
```




# ios项目免证书真机调试

- 将`platforms/ios`下的ios文件夹压缩成ios.zip

- 拷贝到mac电脑上使用xocde打开

- 点击左上角的Xcode,选择Preferences-->Accounts-->左下角加号(填写自己的ippid)-->Manage Certificates-->点击弹出框的左下角+证书(Apple Development)-->

- 使用usb连上手机,点击项目名称,在TARGETS(Debug)下选择signing&Capabilites中的team中选择自己的账号,如果报错,就将自己的包名使用唯一

- 点击xcode的运行按钮



## xcode run and build



![96619796](Vue_to_android_ios_package/96619796.jpg)







![584220193](Vue_to_android_ios_package/584220193.jpg)





![844538922](Vue_to_android_ios_package/844538922.jpg)







![1341378044](Vue_to_android_ios_package/1341378044.jpg)





![2073041873](Vue_to_android_ios_package/2073041873.jpg)

