---
title: Windows10 Cordova环境搭建打包Android安装包
layout: page
date: 2020/2/10
updated: 2020/2/10
comments: false
tags: 
- Android
- Windows10 
categories: 
- cordova
---

<!-- toc -->

[TOC]

之前写了**Vue项目打包成Android和iOS安装包**，不过那是基于Linux的环境。最近疫情紧张，都在家里远程办公，,于是整理了Windows10下Cordova环境搭建以及打包Android安装包。

### Cordova环境依赖:

- win10系统
- Node环境
- Java环境
- AndroidStudio
- Ant
- Gradle

#### 安装node环境:

1.使用node官网网址下载node包，最好使用稳定版本。https://nodejs.org/

![js_download_page](Windows10_install_Cordova_for_Android_APP/js_download_page.png)

2.一路安装next，然后在CMD中使用命令查看node版本

```
node -v
npm -v
```



![js_npm_version](Windows10_install_Cordova_for_Android_APP/js_npm_version.png)



#### Java环境配置：

jdk8下载地址：<https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html>

Windows 10 就下载 **jdk-8u241-windows-x64.exe**这个文件。

记住安装地址，我是安装在D盘下

![jdk_install_path](Windows10_install_Cordova_for_Android_APP/jdk_install_path.png)



**第一步**：在桌面上依次 右键单击计算机—属性—高级系统设置—环境变量。

**第二步**：新建一个名为JAVA_HOME的系统变量，第二栏的值即为你自己jdk的安装路径，这里的是我的，自己的依照自己的情况来。



![java_home](Windows10_install_Cordova_for_Android_APP/java_home.png)



**第三步**：新建一个名为CLASSPATH的系统变量，在第二栏一字不差地输入 %JAVA_HOME%\lib\dt.jar;%JAVA_HOME%\lib\tools.jar。看下一个图。



第四步：找到一个叫PATH的系统变量，双击其编辑，在最后加上下图中圈出的语句，然后确定。

![java_path](Windows10_install_Cordova_for_Android_APP/java_path.png)



**第五步**：使用CMD检查，使用javac命令，配置成功则出现以下

![javav](Windows10_install_Cordova_for_Android_APP/javav.png)



#### 安装安卓SDK:

第一步：推荐安装AndroidStudio，会自动配置SDK以及SDK-tools等相关的，可以在编辑器里选择要下载使用的SDK版本，安装好后，仿照配置Java环境的方式配置ANDROID_HOME。

安装AS：

![android-studio-install](Windows10_install_Cordova_for_Android_APP/android-studio-install.png)



安装SDK：

![sdk-install](Windows10_install_Cordova_for_Android_APP/sdk-install.png)

SDK Tools：

![sdk-tools](Windows10_install_Cordova_for_Android_APP/sdk-tools.png)



如果你无法访问Google，那么恭喜你，估计你很难完整的下载到SDK。下面告诉你如何解决：

修改DNS，进入网站<http://ping.chinaz.com/>，进行 `dl.google.com` ping检查，选择大陆响应时间最短的IP地址，用这个IP添加到host里面，hosts文件地址：`C:\WINDOWS\System32\drivers\etc\hosts`

![ping-dl-google-com](Windows10_install_Cordova_for_Android_APP/ping-dl-google-com.png)



![host-dl-google-com](Windows10_install_Cordova_for_Android_APP/host-dl-google-com.png)

```
203.208.41.41 dl.google.com
```



第二步：配置其他安卓环境变量，也是在Path中进行配置

```
ANDROID_HOME C:\Users\alex\AppData\Local\Android\Sdk
%ANDROID_HOME%\build-tools\29.0.3
%ANDROID_HOME%\platform-tools
%ANDROID_HOME%\tools
```



![android-home](Windows10_install_Cordova_for_Android_APP/android-home.png)



**注意**：在配置`build-tools`的时候，版本号不要搞错了。随着你安装时间的不同，这个版本号会不一样。

![sdk-build-version](Windows10_install_Cordova_for_Android_APP/sdk-build-version.png)

![android-path](Windows10_install_Cordova_for_Android_APP/android-path.png)





第三步：终端中输入adb，出现下图表示成功



![adb-version](Windows10_install_Cordova_for_Android_APP/adb-version.png)



#### 安装Ant

第一步：https://ant.apache.org/bindownload.cgi 进行安装，选择1.10.*版本的，该版本对应的JDK8。

![ant](Windows10_install_Cordova_for_Android_APP/ant.png)



解压目录：

![install-path](Windows10_install_Cordova_for_Android_APP/install-path.png)



第二步：配置Ant环境变量

```
ANT_HOME D:\Program Files\Android\apache-ant-1.10.7
path %ANT_HOME%/bin
```




第三步：检测安装是否成功

```
ant -version
```



![ant-version](Windows10_install_Cordova_for_Android_APP/ant-version.png)



#### 安装Gradle

第一步：下载并解压 https://services.gradle.org/distributions/ 下载记住自己解压的地址。
特别注意要下载.bin.zip的版本

第二步：配置Gradle环境变量

```
GRADLE_HOME D:\Program Files\Android\gradle-6.1.1
path %GRADLE_HOME%/bin
```



第三步：检测安装是否成功

```
gradle -version
```



![gradle-version](Windows10_install_Cordova_for_Android_APP/gradle-version.png)


到此为止，在Windows10下所有的cordava环境依赖全部安装完毕了，来看看所有的变量都添加了之后的样子：

![sys-env-path](Windows10_install_Cordova_for_Android_APP/sys-env-path.png)



由于默认情况下执行 gradle 各种命令是去国外的 gradle 官方镜像源获取需要安装的具体软件信息，所以在不使用代理的情况下，从国内访问国外服务器的速度相对比较慢。阿里旗下维护着一个国内 maven 镜像源，同样适用于 gradle。

找到 (用户家目录)/.gradle/init.gradle 文件，如果找不到 init.gradle 文件，自己新建一个

修改/添加 init.gradle 文件内的 repositories 配置

```
allprojects {
    repositories {
        maven {
            url "http://maven.aliyun.com/nexus/content/groups/public"
        }
    }
}
```



### 安装cordova

#### 全局安装

```
npm install -g cordova
```



#### cordova添加Android

```
cordova platform add android
```

![android-platform-add](Windows10_install_Cordova_for_Android_APP/android-platform-add.png)



#### cordova运行Android调试

```
cordova run android --device
```

![android-platform-run-01](Windows10_install_Cordova_for_Android_APP/android-platform-run-01.png)



![android-platform-run-02](Windows10_install_Cordova_for_Android_APP/android-platform-run-02.png)



#### **浏览**器调试工具:

```
chrome://inspect/#**devices**
```

![chrome-inspect](Windows10_install_Cordova_for_Android_APP/chrome-inspect.png)



#### cordova打包APP

```
cordova build android --release
```

![android-platform-build-01](Windows10_install_Cordova_for_Android_APP/android-platform-build-01.png)



![android-platform-build-02](Windows10_install_Cordova_for_Android_APP/android-platform-build-02.png)



参考： https://blog.csdn.net/weixin_43765499/article/details/89142608