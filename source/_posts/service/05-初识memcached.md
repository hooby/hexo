---
title: 初识memcached
layout: page
date: 2015/12/12
updated: 2016/6/13
comments: false
tags: 
- memcached
categories: 
- service
---

<!-- toc -->

[TOC]

```
memcached：

    memcached is a high-performance, distributed memory object caching system, generic in nature, but intended for use in speeding up dynamic web applications by alleviating database load.


    缓存服务器：
        缓存：cache，无持久存储功能；
        bypass缓存
        k/v cache，仅支持存储流式化数据；
        
    LiveJournal旗下的Danga Interactive研发，
    
        特性：
            k/v cache：仅可序列化数据；存储项：k/v；
            智能性一半依赖于客户端（调用memcached的API开发程序），一半依赖于服务端；
            分布式缓存：互不通信的分布式集群；
                分布式系统请求路由方法：取模法，一致性哈希算法；
            算法复杂度：O(1)
            清理过期缓存项：
                缓存耗尽：LRU 
                缓存项过期：惰性清理机制
            
    安装配置：
        由CentOS 7 base仓库直接提供：
            监听的端口：
                11211/tcp, 11211/udp 
                
        主程序：/usr/bin/memcached
        配置文件：/etc/sysconfig/memcached
        Unit File：memcached.service 
        
        协议格式：memcached协议
            文本格式
            二进制格式
            
        命令：
            统计类：stats, stats items, stats slabs, stats sizes
            存储类：set, add, replace, append, prepend
                命令格式：<command name> <key> <flags> <exptime> <bytes>  
                <cas unique>
            检索类：get, delete, incr/decr
            清空：flush_all
            
            示例：
                telnet> add KEY <flags> <expiretime> <bytes> \r
                telnet> VALUE
                
        memcached程序的常用选项：
            -m <num>：Use <num> MB memory max to use for object storage; the default is 64 megabytes.
            -c <num>：Use <num> max simultaneous connections; the default is 1024.
            -u <username>：以指定的用户身份来运行进程；
            -l <ip_addr>：监听的IP地址，默认为本机所有地址；
            -l <ip_addr>：监听的IP地址，默认为本机所有地址；
            -p <num>：监听的TCP端口， the default is port 11211.
            -U <num>：Listen on UDP port <num>, the default is port 11211, 0 is off.
            -M：内存耗尽时，不执行LRU清理缓存，而是拒绝存入新的缓存项，直到有多余的空间可用时为止；
            -f <factor>：增长因子；默认是1.25；
            -t <threads>：启动的用于响应用户请求的线程数；
            
        memcached默认没有认证机制，可借用于SASL进行认证；
            SASL：Simple Authentication Secure Layer
            
        API:
            php-pecl-memcache
            php-pecl-memcached
            python-memcached
            libmemcached
            libmemcached-devel
            
        命令行工具：
            memcached-tool  SERVER:PORT  COMMAND
```

