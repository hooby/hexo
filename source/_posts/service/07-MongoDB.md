---
title: MongoDB
layout: page
date: 2016/1/1
updated: 2016/6/13
comments: false
tags: 
- MongoDB
categories: 
- service
toc: true
---

<!-- toc -->

[TOC]

MongoDB 是一个基于分布式文件存储的数据库。由 C++ 语言编写。旨在为 WEB 应用提供可扩展的高性能数据存储解决方案。

MongoDB 是一个介于关系数据库和非关系数据库之间的产品，是非关系数据库当中功能最丰富，最像关系数据库的。



# install

下载完安装包，并解压 **tgz**。

```
curl -O https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-3.0.6.tgz    # 下载
tar -zxvf mongodb-linux-x86_64-3.0.6.tgz                                   # 解压

mv  mongodb-linux-x86_64-3.0.6/ /usr/local/mongodb                         # 将解压包拷贝到指定目录
```

MongoDB 的可执行文件位于 bin 目录下，所以可以将其添加到 **PATH** 路径中：

```
export PATH=/usr/local/mongodb/bin:$PATH
```



# docker 

这年头有了docker之后，运行MongoDB简直不能太简单了，非常适合开发使用。

```
docker pull mongodb:3.4
docker run --name mongodb-dev -p 27017:27017 -v /srv/monggo/mongodb:/data/db -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=password -d mongo:3.4
```



# mongodb shell

MongoDB Shell是MongoDB自带的交互式Javascript shell,用来对MongoDB进行操作和管理的交互式环境，在MongoDB安装目录下的bin目录，执行mongo命令文件。

当你进入mongoDB后台后，它默认会链接到 test 文档（数据库）：

```
docker exec -it mongodb-dev bash
    mongo
        use admin 
        db.auth('admin','password')
    exit
exit
```

下面是一些常见命令：

```
# 切换到admin database
use admin

# auth
db.auth("admin", "sg-ai")

# show database name
show dbs
show tables
show collections
db.tablename.find()
db.tablename.find([query] , [fields])
db.tablename.find({},{})
db.tablename.find({},{"pid": 1, "id": 1})
db.mytable1.find({"userName": "superman"},{})
db.mytable1.remove({"userName": "superman"},{})
db.mytable2.find()

# createUser and backup
db.createUser({ "user" : "alex", "pwd" : "superman", "roles" : [  { role: "readWrite", db: "finance" } ] })
# mongodump --authenticationDatabase=databasename -u alex -p superman -d tablename -o /data/db/backup

```



# adminMongo

[adminMongo](https://github.com/mrvautin/adminMongo) 是一个链接MongoDB数据库的可视化工具，开发使用效果极佳。

![adminMongo_dbview](07-MongoDB/adminMongo_dbview.png)



## run

adminMongo 默认使用 http://0.0.0.0:1234 ， 运行之前先安装npm。

```
git clone -b master --depth 1 https://github.com/mrvautin/adminMongo.git
cd adminMongo && npm install
npm start
```



# backup/restore

数据备份有多重要自然不用多说，下面简要介绍一下导入导出的命令：

导出mongodump:

```
mongodump --authenticationDatabase=database -u username -p password -d auth-database -o /data/db/backup
```

导入mongorestore:

```
mongorestore --drop -u admin -p superman --authenticationDatabase=admin -d mydatabase /data/db/backup/mydata
```

导入导出都可以按照database或者collection来操作，具体看帮助。