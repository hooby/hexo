---
title: git常见问题
layout: page
date: 2016/8/26
updated: 2017/12/13
comments: false
tags: 
- git
categories: 
- service
toc: true
---

<!-- toc -->
[TOC]

## 1.git删除文件

当我们想要删除某个文件,直接删除是没有用的.

```
[root@vultr hexo]# git status 
# On branch master
nothing to commit (working directory clean) 
[root@vultr hexo]# rm source/_posts/test.txt 
rm: remove regular empty file `source/_posts/test.txt'? y
[root@vultr hexo]# git status 
# On branch master
# Changed but not updated:
#   (use "git add/rm <file>..." to update what will be committed)
#   (use "git checkout -- <file>..." to discard changes in working directory)
#
#	deleted:    source/_posts/test.txt
#
no changes added to commit (use "git add" and/or "git commit -a")
[root@vultr hexo]# git add . 
[root@vultr hexo]# git commit -m "delete test file "
# On branch master
# Changed but not updated:
#   (use "git add/rm <file>..." to update what will be committed)
#   (use "git checkout -- <file>..." to discard changes in working directory)
#
#	deleted:    source/_posts/test.txt
#
no changes added to commit (use "git add" and/or "git commit -a")
```

直接使用`rm` 删除的仅仅是工作区的文件,并没有删除暂存区,所以`commit` 不会生效. 由于平时使用基本都是修改或者增加新文件,大家习惯用 `git add ` 命令,而忽略了 `git rm` , 其实在`git status` 都有提示,仔细看看就知道了.

正确的做法应该是下面这样:

```
[root@vultr hexo]# git rm source/_posts/test.txt
rm 'source/_posts/test.txt'
[root@vultr hexo]# git status 
# On branch master
# Changes to be committed:
#   (use "git reset HEAD <file>..." to unstage)
#
#	deleted:    source/_posts/test.txt
#
[root@vultr hexo]# git commit -m "delete test"
[master 63184bb] delete test
 0 files changed, 0 insertions(+), 0 deletions(-)
 delete mode 100644 source/_posts/test.txt
[root@vultr hexo]# git status 
# On branch master
# Your branch is ahead of 'origin/master' by 1 commit.
#
nothing to commit (working directory clean)
[root@vultr hexo]# git push origin master 
```



## 2.假大空的`ahead of commits`

在部署的环境中,我们根本不会修改代码,但是当你用`git status` 查看的时候发现会有 `Your branch is ahead of 'origin/master' by 8 commits.` 这样的提示:

```
[root@vultr hexo]# git status 
# On branch master
# Your branch is ahead of 'origin/master' by 8 commits.
#
nothing to commit (working directory clean)
[root@vultr hexo]# ls 
_config.yml  db.json  hexo.txt  Makefile  node_modules  package.json  package-lock.json  public  README.md  scaffolds  source  themes
[root@vultr hexo]# git push origin master 
Everything up-to-date
```

然而这都是幻象,当你`git push` 准备提交的时候发现,根本没有commit. 完全可以不用理会,如果你觉得碍眼,就`git push` 意思一下.



## 3.  git clone ssh报错

ssh 获取代码的方式比https的方便，无需手动输入密码。因此如果有ssh的方式，大家都愿意用ssh。那么接下来看看我在Linux上部署ssh方式获取git代码的坑吧。

首先来看看最常见的报错

```
$git clone ssh://git@git.gitlab.com:2222/test/test_project.git
Permission denied (publickey).
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
```

这个错误很明显，要用gitlab或者GitHub 你需要把ssh key（也就是`~/.ssh/id_rsa.pub`） 文件内容告诉gitlab，你才有权限获取代码啊。

![add_ssh_key_](10-git problem/add_ssh_key.png)



什么？ 还报这个错误啊？那接着往下看吧。

首先看看你的配置文件

```
$cat ~/.ssh/config 
# Added by Warewulf 
Host *
   IdentityFile ~/.ssh/cluster
   StrictHostKeyChecking=no
```

原来我的配置里面认证使用的是 `~/.ssh/cluster` 文件，那我吧cluster.pub 这个文件添加到ssh key可以吗？答案是不行的，warewulf技术认证使用的是dsa加密，而ssh key使用的是rsa加密。但是我又不能直接改掉这个cluster，改了它集群其他技术就用不了了，怎么办？增加一个认证文件就可以啦！

```
$cat .ssh/config 
# Added by Warewulf
Host *
   IdentityFile ~/.ssh/cluster
   IdentityFile ~/.ssh/id_rsa
   StrictHostKeyChecking=no
```

结果so esay，然而排错的过程却是痛苦的。下面简单介绍一下稍好 一点的排错方式。

在添加完 `~/.ssh/id_rsa` 之后还是报错的时候，你可以通过 ssh来调试

```
$ssh -v git@git.gitlab.com -p 2222
OpenSSH_6.6.1, OpenSSL 1.0.1e-fips 11 Feb 2013
debug1: Reading configuration data ~/.ssh/config
debug1: ~/.ssh/config line 2: Applying options for *
debug1: Reading configuration data /etc/ssh/ssh_config
debug1: /etc/ssh/ssh_config line 56: Applying options for *
debug1: Connecting to git.gitlab.com [xxx.xx.xx.x] port 2222.
debug1: Connection established.
...
Permission denied (publickey).
```

注意这里，我用的gitlab搭建改了ssh的端口2222，犯过愚蠢的错误 `ssh -v git@git.gitlab.com:2222` ，然后一直报错，找不到原因，但回过头来想，这只是一个ssh命令，端口应该用参数 `-p` ...

如果上面的命令报了`Permission denied (publickey).` 这样的错误那就是你添加的ssh key没有被系统找到。那我们就手动添加来试试。

```
$ssh-agent bash               #进入一个新的bash环境
$ssh-add ~/.ssh/id_rsa        #手动添加ssh key
$ssh -v git@git.gitlab.com -p 2222  #再次尝试连接gitlab
...
Welcome to GitLab, xxx!       # 到这里说明连接上了gitlab
debug1: client_input_channel_req: channel 0 rtype exit-status reply 0
debug1: client_input_channel_req: channel 0 rtype eow@openssh.com reply 0
debug1: channel 0: free: client-session, nchannels 1
Connection to git.gitlab.com closed.
...
```

到这一步发现，诶`~/.ssh/id_rsa ` 这个文件没有被系统作为认证文件，手动添加之后就可以连接，那就可以从系统为什么没有添加这个认证文件入手了。就是上面说的配置文件`~/.ssh/config `  和  `/etc/ssh/ssh_config`

关于配置文件的说明我也是网上找的，看我[百度的](https://zhidao.baidu.com/question/712206549106448205.html)  和[ssh官网](https://www.ssh.com/ssh/sshd_config/) 







