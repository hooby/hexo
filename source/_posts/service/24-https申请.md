---
title: acme.sh配合letsencrypt配置泛域名
layout: page
date: 2017/10/3
updated: 2018/6/24
comments: false
tags: 
- https
categories: 
- https
toc: true
---

<!-- toc -->

[TOC]

MongoDB 是一个基于分布式文件存储的数据库。由 C++ 语言编写。旨在为 WEB 应用提供可扩展的高性能数据存储解决方案。

MongoDB 是一个介于关系数据库和非关系数据库之间的产品，是非关系数据库当中功能最丰富，最像关系数据库的。



# 1. 安装 acme.sh

```
curl  https://get.acme.sh | sh
```

将会安装到 **~/.acme.sh/** 目录下，以后所有的配置默认也在这个目录下



# 2. 生成证书

**acme.sh** 实现了 **acme** 协议支持的所有验证协议. 一般有两种方式验证: http 和 dns 验证，这里仅介绍 DNS 方式

具体 [dnsapi 链接](https://github.com/Neilpang/acme.sh/tree/master/dnsapi)), 这里以 阿里云 为例：

首先获取你的阿里云API Key: [ak-console.aliyun.com/#/accesskey](https://usercenter.console.aliyun.com/#/manage/ak)

之后在你的终端配置文件中设置：

```
export Ali_Key="Your AccessKey ID"
export Ali_Secret="Your Access Key Secret"
```

之后直接使用如下命令发起申请(注意这里第一个域名为顶级域名，后面个为泛域名。)：

```
acme.sh --issue --dns dns_ali -d example.com -d *.example.com *.second.example.com
```



```

curl https://get.acme.sh | sh -s email=xxx@xx.com
exit 
# tencent api token address:
# https://console.dnspod.cn/account/token

export DP_Id="xxx"
export DP_Key="xxx"
acme.sh --issue --dns dns_dp -d lsbgb.com -d *.lsbgb.com
acme.sh --issue --dns dns_dp -d lsbgb.com -d *.lsbgb.com --fullchain-file /etc/nginx/ssl/lsbgb.com/fullchain.cer --key-file /etc/nginx/ssl/lsbgb.com/lsbgb.com.key --dnssleep
acme.sh --issue --dns dns_dp -d k8s.lsbgb.com -d *.k8s.lsbgb.com
acme.sh --issue --dns dns_dp -d k8s.lsbgb.com -d *.k8s.lsbgb.com --fullchain-file /etc/nginx/ssl/k8s.lsbgb.com/fullchain.cer --key-file /etc/nginx/ssl/k8s.lsbgb.com/k8s.lsbgb.com.key --dnssleep

# crontab -l
# cat ~/.acme.sh/account.conf
```



## 三级域名申请

申请了顶级域名和二级域名，但在使用三级域名的时候还是会报不安全的错误，所以如果你想要用三级域名的话，也必须在申请的时候写上

```
acme.sh --issue --dns dns_ali -d example.com -d *.example.com *.second.example.com
```



如果你想单独的申请三级域名，并不想与顶级域名搞在一起，则可以使用如下方式：

```
acme.sh --issue --dns dns_ali -d second.example.com *.second.example.com
```

注意：这种方式需要在dns中指定二级域名到你申请的主机。



## 更新证书

证书有效期是90天，目前证书在 60 天以后会自动更新, 你无需任何操作，不过都是自动的, 你不用关心.

如果你关闭了更新证书功能，手动执行上面的命令就好了，会自动把之前的证书覆盖掉。