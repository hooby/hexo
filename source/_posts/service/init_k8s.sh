yum install -y -q net-tools epel-release

echo 'PATH=/opt/k8s/bin:$PATH' >>/root/.bashrc
echo "tail -n 2 /root/.bashrc"
tail -n 2 /root/.bashrc


iptables -F
iptables -vnL
systemctl status firewalld.service
systemctl stop firewalld.service
systemctl disable firewalld.service

free -h
swapoff -a
sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
echo "cat /etc/fstab"
cat /etc/fstab

setenforce 0
echo "getenforce"
getenforce

sed -i 's/^SELINUX=.*/SELINUX=disabled/' /etc/selinux/config
echo "cat /etc/selinux/config"
cat /etc/selinux/config
modprobe br_netfilter


cat > /etc/sysctl.d/kubernetes.conf <<EOF
net.ipv4.ip_forward=1
net.ipv4.tcp_tw_recycle=0
vm.swappiness=0 # 禁止使用 swap 空间，只有当系统 OOM 时才允许使用它
vm.overcommit_memory=1 # 不检查物理内存是否够用
vm.panic_on_oom=0 # 开启 OOM
fs.inotify.max_user_instances=8192
fs.inotify.max_user_watches=1048576
fs.file-max=52706963
fs.nr_open=52706963
EOF

echo "cat /etc/sysctl.d/kubernetes.conf"
cat /etc/sysctl.d/kubernetes.conf

echo "sysctl -p /etc/sysctl.d/kubernetes.conf"
sysctl -p /etc/sysctl.d/kubernetes.conf


# 调整系统 TimeZone
timedatectl set-timezone Asia/Shanghai

# 将当前的 UTC 时间写入硬件时钟
timedatectl set-local-rtc 0

# 重启依赖于系统时间的服务
systemctl restart rsyslog 
systemctl restart crond

ntpdate cn.pool.ntp.org
cat > /etc/cron.hourly/99-ntp <<<EOF
#!/bin/sh
/usr/sbin/ntpdate cn.pool.ntp.org
EOF
chmod a+x /etc/cron.hourly/99-ntp

systemctl stop postfix && systemctl disable postfix


	

	
	
	
	