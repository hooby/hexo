---
title: docker镜像上增加安装包
layout: page
date: 2017/5/25
updated: 2017/11/13
comments: false
tags: 
- docker
categories: 
- docker
toc: true
---

 <!-- toc -->

[TOC]

## 前提

有时候我们会遇到这样的问题,一个镜像能满足我们的大部分需求,想在此基础上添加一部分功能.有三种方法

### 1.dockerfile构建

找到这个镜像的dockerfile,修改后build.



### 2.作为基础镜像

也是写一个dockerfile,只不过这次把这个有较多功能的镜像作为基础镜像,添加所需功能.然后build.



### 3.导出container为image

直接在运行的container中添加所需功能,然后commit成image.

#### 第一步

 使用root用户进入一个新容器，不要用` --rm `（否则当你退出容器的时候，容器没有了 你添加的功能自然就不复存在了）

```
docker run --user 0 -it --name superman sgdockerfilebox/mitosis_cpu:latest bash
```





#### 第二步

在container中直接添加你要的功能

````
apt update
apt  install ... 
npm install -g n 
n stable ...
pip3 install ...
````

 然后退出容器` exit`

#### 第三步

新的容器 `commit`成新的`image `
`docker commit superman sgdockerfilebox/wewo_cpu:v1`

 （这里的`sgdockerfilebox/wewo_cpu:v1 `名字和版本号自己定义）

查看一下`commit`的`image`

```
root@gyw:~# docker images 
REPOSITORY                         TAG                 IMAGE ID            CREATED             SIZE
sgdockerfilebox/wewo_cpu           v1                  e4f2c829d1eb        23 minutes ago      4.42GB
```



没有问题就可以删除 之前创建的容器了，
`docker rm superman`

#### 第四步

将新的image 保存成tar压缩文件，给其他同事使用，统一环境

`docker save sgdockerfilebox/wewo_cpu:v1 -o wewo_cpu.tar`

把 `wewo_cpu.tar` 给其他人吧。

#### 第五步

其他同事拿到 `wewo_cpu.tar `之后使用如下命令即可：
`docker load -i wewo_cpu.tar `

验证导入的`image`

```
root@gyw:~# docker images 
REPOSITORY                         TAG                 IMAGE ID            CREATED             SIZE
sgdockerfilebox/wewo_cpu           v1                  e4f2c829d1eb        23 minutes ago      4.42GB
```

