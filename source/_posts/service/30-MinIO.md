---
title: MinIO
layout: page
date: 2017/8/5
updated: 2018/7/15
comments: false
tags: 
- MinIO
categories: 
- storage
- service
---

<!-- toc -->

[TOC]

# MinIO

MinIO 是一个基于Apache License v2.0开源协议的对象存储服务。它兼容亚马逊S3云存储服务接口，非常适合于存储大容量非结构化的数据，例如图片、视频、日志文件、备份数据和容器/虚拟机镜像等，而一个对象文件可以是任意大小，从几kb到最大5T不等。

MinIO是一个非常轻量的服务,可以很简单的和其他应用的结合，类似 NodeJS, Redis 或者 MySQL。



## docker install

官方强烈推荐使用docker安装。轻量服务嘛。

```
docker pull minio/minio
docker run -p 9000:9000 minio/minio server /data
```

其他方式参考[官方](https://docs.min.io/)咯， 安装由于很简单，这里就不写了。



## MinIO SDK

minio有支持各种语言的SDK，你甚至可以用它来链接aws的s3， 下面一Python为例，客户端连接服务。

首先安装minio模块

```
pip install minio
```

链接minio server

```
from minio import Minio
from minio.error import ResponseError

minioClient = Minio('minio.domain.com',
                  access_key='xxxxx',
                  secret_key='xxxxx',
                  secure=True)
```

链接aws s3 server

```
from minio import Minio
from minio.error import ResponseError

s3Client = Minio('s3.amazonaws.com',
                 access_key='YOUR-ACCESSKEYID',
                 secret_key='YOUR-SECRETACCESSKEY',
                 secure=True)
```

操作的话主要分为四类： `Bucket operations` ， `Object operations` ， `Presigned operations` 和 `Bucket policy` ，怎么样？ 是不是跟aws的s3一样一样的。具体参考 [python-client-api-reference](https://docs.min.io/docs/python-client-api-reference.html)

