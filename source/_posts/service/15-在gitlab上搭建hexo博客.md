---
title: 在gitlab上搭建hexo博客
layout: page
date: 2016/7/17
updated: 2016/9/12
comments: false
tags: 
- hexo
categories: 
- gitlab
toc: true
---

<!-- toc -->

[TOC]

这篇文章讲解怎样用hexo把你写的Markdown快速搭建成博客(呐，你正在看的这篇博客呢就是一个Markdown写出来的 `^_^` )。

## hexo

### 1.什么是hexo？

[Hexo](https://hexo.io/) 是一个快速、简洁且高效的博客框架。Hexo 使用 [Markdown](http://daringfireball.net/projects/markdown/)（或其他渲染引擎）解析文章，在几秒内，即可利用靓丽的主题生成静态网页。

### 2.安装前提

安装 Hexo 相当简单。然而在安装前，您必须检查电脑中是否已安装下列应用程序：

- [Node.js](http://nodejs.org/)
- [Git](http://git-scm.com/)

### 3.安装

```

```

就是这么简单！

### 4.使用

简单三部曲：

```shell
hexo init blog 
cd blog && npm install
hexo server
# 1.初始化一个hexo 取名blog
# 2.进入blog 构建要用到的模块包
# 3.启动服务, 打开浏览器输入：127.0.0.1:3000 看看吧
```

就这样，你没有写任何文档这个博客也就起来了。

这个初始化的hexo下面，目录结构URU型下：

```
.
├── _config.yml #配置信息
├── package.json #应用程序的信息，npm install就是安装这些应用，生成node_modules目录
├── scaffolds
├── source
|   ├── _drafts
|   └── _posts #资源文件
└── themes #主题
```



如果你想要把你的Markdown内容放入这个博客，简单，将你写的Markdown放入`blog/source/_post` 目录中即可。

### 5.生成器

上面的 `hexo server` 只是在你的本地主机上运行了一个临时的博客，方便你看效果，如果没有问题那你就把他生成静态文件，放到任何地方做成博客啦。那在生成静态文件的时候，有一些配置需要配好。

#### a._config.yml

hexo对配置文件有详细的说明，这里只需要介绍常用的几个

```
# Site
title: 网站标题
author: hooby
timezone: Asia/Shanghai

url: https://hooby.gitlab.io/hexo  #	网址
root: /hexo  #网站根目录

theme: maupassant  #选择用什么主题，后面会说
```

#### b.主题

主题就是你网站的风格咯，上[官网](https://hexo.io/themes/)挑一个咯。然后下载到 thems目录下面，同时在`_config.yml` 文件中的theme字段写上目录名称即可。

```
# git submodule add https://github.com/tufu9441/maupassant-hexo.git themes/maupassant
# cat _config.yml
...
theme: maupassant
...
```





#### c.生成静态页面

进入blog，执行：

```
hexo g
```

就是这么简单！

静态页面文件在`public` 目录。



## gitlab-pages

那如果你没有自己的服务器，也没有域名，没有关系啦，上gitlab申请一个账号，做一个自己的blog，这是我的[blog](https://hooby.gitlab.io/hexo/)。gitlab是提供代码管理的，同时提供的pages功能，这个功能就可以做blog。

当你把自己的代码和Markdown放在gitlab上面之后，创建一个文件 `.gitlab-ci.yml`  ,内容：

```
image: node:8.11.4

pages:
  # before_script:
  # # 拉取子模块
  # - git submodule sync --recursive
  # - git submodule update --init --recursive
  variables:
    GIT_SUBMODULE_STRATEGY: recursive

  script:
  - npm install
  - ./node_modules/hexo/bin/hexo generate
  artifacts:
    paths:
    - public
  only:
  - master

```

就这么简单。

上哪里去看你的blog？

在gitlab上面找到Settings --> Pages ，Access pages下面那个网址就是你的啦，进去看看吧。

