---
title: gitlab
layout: page
date: 2016/7/12
updated: 2017/11/13
comments: false
tags: 
- gitlab
categories: 
- gitlab
toc: true
---

<!-- toc -->

[TOC]

# GitLab

**GitLab**是用Ruby开发的完全免费的开源软件，按照 MIT 许可证分发的Git仓库管理工具，且具有wiki和issue跟踪功能。特别适合公司搭建自己的Git仓库，可局域网使用。



## install

如果不是特大公司，千人以上的开发人员。中小型企业直接使用docker运行gitlab基本是足够的了，公司使用两三年了，基本没有遇到什么问题。

运行示例：

```
docker pull gitlab-ce:9.5.10-ce.0

docker run --detach \
    --hostname git.domain.com \
    --env GITLAB_OMNIBUS_CONFIG="external_url 'https://git.domain.com/';" \
    --env GITLAB_HOST="git.domain.com" \
    --publish 80:80 \
    --publish 2289:22 \
    --name gitlab \
    --restart always \
    --volume /gitlab/config:/etc/gitlab \
    --volume /gitlab/logs:/var/log/gitlab \
    --volume /gitlab/data:/var/opt/gitlab \
    --log-opt max-size=100m --log-opt max-file=10 \
    gitlab-ce:9.5.10-ce.0
```



## backup

备份很简单，`itlab-rake gitlab:backup:create` 即可搞定，备份以`tar` 压缩包保存。格式如下：

`1545649962_2017_12_24_9.5.10_gitlab_backup.tar` ，中间有备份时间和gitlab版本。docker备份：

```
docker exec -t gitlab gitlab-rake gitlab:backup:create
ls /gitlab/data/backups/1545649962_2017_12_24_9.5.10_gitlab_backup.tar
```



## restore

恢复必须在相同的gitlab版本上面，比如上面的例子是在 9.5.10

```
# stop the server 
gitlab-ctl stop unicorn
gitlab-ctl stop sidekiq
gitlab-ctl status
cd /var/opt/gitlab/backup
gitlab-rake gitlab:backup:restore BACKUP=1545649962_2018_12_24_9.5.10
gitlab-ctl restart
gitlab-rake gitlab:check SANITIZE=true
```



参考： https://docs.gitlab.com/ee/raketasks/backup_restore.html



## Personal Access Tokens

这个非常有用。github中有gist，但是在gitlab中如果你的代码库不是设置成public，那么其他人是无法看到该文件的。有了这个private token之后，就可以了。

添加token：

![gitlab_token_set](14-gitlab/gitlab_token_set.png)



访问文件：

![gitlab_private_token](14-gitlab/gitlab_private_token.png)