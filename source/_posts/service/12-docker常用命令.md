---
title: docker常用命令
date: 2017/5/13
updated: 2017/11/13
comments: false
tags: 
- docker
categories: 
- service
toc: true
---

<!-- toc -->

[TOC]



## 1.基本概念

Docker 包括三个基本概念

- 镜像（`Image`）

  Docker 镜像是一个特殊的**文件系统**，除了提供容器运行时所需的程序、库、资源、配置等文件外，还包含了一些为运行时准备的一些配置参数（如匿名卷、环境变量、用户等）。镜像**不包含任何动态数据**，其内容在构建之后也**不会改变**。

- 容器（`Container`）

  容器是镜像运行时的实体。容器可以被创建、启动、停止、删除、暂停等。

- 仓库（`Repository`）

  一个集中的存储、分发镜像的服务。

理解了这三个概念，就理解了 Docker 的整个生命周期。



## 2.docker安装

[官方安装教程](https://docs.docker.com/engine/installation/#supported-platforms)

[nvidia-docker安装](https://github.com/NVIDIA/nvidia-docker/tree/v2.0.3) 

[阿里云镜像源docker安装方式](https://yq.aliyun.com/articles/110806?spm=a2c1q.8351553.0.0.9e3d4f2NpVgK)

```
#ubuntu
sudo apt-get update
sudo apt-get -y install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL http://mirrors.aliyun.com/docker-ce/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] http://mirrors.aliyun.com/docker-ce/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get -y update
sudo apt-get -y install docker-ce

#CentOS 
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
sudo yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
sudo yum makecache fast
sudo yum -y install docker-ce
sudo service docker start
```



##  3.docker命令

### a、镜像相关

```
docker image --help

Usage:	docker image COMMAND
Commands:
  ls          List images
  pull        Pull an image or a repository from a registry
  push        Push an image or a repository to a registry
  rm          Remove one or more images
  save        Save one or more images to a tar archive (streamed to STDOUT by default)
  load        Load an image from a tar archive or STDIN
  tag         Create a tag TARGET_IMAGE that refers to SOURCE_IMAGE
  build       Build an image from a Dockerfile
  
Run 'docker image COMMAND --help' for more information on a command.
```

#### 1.`docker images ` 

列出主机上已存在镜像

```
$ docker images 
REPOSITORY                              TAG                 IMAGE ID            CREATED             SIZE
172.16.10.10:5000/deepchem              cpu                 75105fd5d68d        11 days ago         6.98GB
172.16.10.10:5000/deepchem              2.0.0-cpu-worker    2b024a901b22        2 weeks ago         6.75GB
172.16.10.10:5000/python-3.5.4-alpine   v3                  954d2aa346fb        2 months ago        309MB
deepchemio/deepchem                     2.0.0-cpu           d0b0679dd7ce        3 months ago        6.02GB
crossbario/crossbar                     latest              01c84a0d6626        3 months ago        170MB
```

#### 2.docker pull 

下载新的镜像文件, 公共的image可在[docker hub](https://hub.docker.com/explore/)上查找

```
$ docker pull deepchemio/deepchem:2.0.0-cpu
2.0.0-cpu: Pulling from deepchemio/deepchem
1be7f2b886e8: Pull complete 
...
e0bd96390dd4: Pull complete 
Digest: sha256:80cfd1de4023a857566759688ba3ef9f719f6062c022a95dd78309156892610f
Status: Downloaded newer image for deepchemio/deepchem:2.0.0-cpu
```

#### 3.docker push  

推送本地主机上的镜像(image)到仓库(Repository)中 。

```
$ docker push hooby/deepchem:2.0.0-cpu
```

#### 4.docker rmi

删除本地镜像

```
docker rmi hooby/deepchem:2.0.0-cpu 
```

#### 5.docker save

将本地image保存为一个压缩包

```
docker save hooby/deepchem:2.0.0-cpu -o deepchem.tar
```

#### 6.docker load

导入image

```
docker load -i deepchem.tar
```

#### 7.docker history

查看image构建过程中的命令历史

```
docker history deepchemio/deepchem:2.0.0-cpu 
IMAGE               CREATED             CREATED BY                                      SIZE                COMMENT
d0b0679dd7ce        3 months ago        /bin/sh -c cd deepchem &&     git clean -fX     0B                  
<missing>           3 months ago        /bin/sh -c export LANG=en_US.UTF-8 &&     gi…   4.31GB    
...
```

#### 8.docker inspect

查看image的元数据信息

```
$ docker inspect deepchemio/deepchem:2.0.0-cpu 
[
    {
        "Id": "sha256:d0b0679dd7cee07b1ad4e95bff9ac01d79f1f742e04bce01dc9e7e459f76475d",
        "RepoTags": [
            "deepchemio/deepchem:2.0.0-cpu"
        ],
...
```

#### 9.docker tag 

从现有的image中创建一个新的tag标签

```
 docker image tag deepchemio/deepchem:2.0.0-cpu hooby/deepchem-cpu
```

这样 `deepchemio/deepchem:2.0.0-cpu` 和 `hooby/deepchem-cpu` 其实内容是一样的，只是名字不一样而已。

#### 10.docker build

通过dockerfile，构建image.

```
docker build -t alex/deepchem:v1.0 . 
```



###b、容器相关 

```
docker container --help 

Usage:	docker container COMMAND
Commands:
  ls          List containers
  run         Run a command in a new container
  start       Start one or more stopped containers
  restart     Restart one or more containers
  exec        Run a command in a running container
  rm          Remove one or more containers
  kill        Kill one or more running containers
  stop        Stop one or more running containers
  logs        Fetch the logs of a container
  rename      Rename a container
  commit      Create a new image from a container's changes

Run 'docker container COMMAND --help' for more information on a command.
```

 常用参数

```
-it                #以交互模式执行
-d/--detach        #后台执行
--rm               #container运行结束后自动删除
--name             #为container指定名称
-p/--publish       #映射主机端口到container中
-v/--volume        #挂载主机文件到container中
-u/--user          #以指定用户运行container
-w /--workdir      #指定在container中执行命令时所在位置
```



#### 1.docker ps 

显示container

```
$ docker ps #显示正在运行状态下的container
CONTAINER ID        IMAGE                            COMMAND                  CREATED             STATUS              PORTS                              NAMES
10cf2cd2d507        172.16.10.10:5000/deepchem:cpu   "bash"                   9 days ago          Up 6 days                                              deepchem-cpu
18b35ff44321        crossbario/crossbar              "crossbar start --cb…"   9 days ago          Up 6 days           8000/tcp, 0.0.0.0:8080->8080/tcp   crossbar-test
$ docker ps -a #显示所有container
CONTAINER ID        IMAGE                                         COMMAND                  CREATED             STATUS                   PORTS                              NAMES
10cf2cd2d507        172.16.10.10:5000/deepchem:cpu                "bash"                   9 days ago          Up 6 days                                                   deepchem-cpu
18b35ff44321        crossbario/crossbar                           "crossbar start --cb…"   9 days ago          Up 6 days                8000/tcp, 0.0.0.0:8080->8080/tcp   crossbar-test
5c0416e9e3d1        crossbario/crossbar:latest                    "crossbar start --cb…"   9 days ago          Exited (1) 9 days ago                                       crossbar_test
8caf97e2c03f        172.16.10.10:5000/deepchem:2.0.0-cpu-worker   "bash"                   11 days ago         Exited (0) 10 days ago                                      deepchem
```

#### 2.docker run

从image启动，运行一个container

```
docker run deepchemio/deepchem:2.0.0-cpu 
docker run -d --name deepchem deepchemio/deepchem:2.0.0-cpu 
docker run -d --name crossbar --rm -v node_config:/node -p 8080:8080 crossbario/crossbar
```

#### 3.docker stop

停掉一个正在运行的container

```
docker stop deepchem
```

#### 4.docker rm

删除停掉的container

```
docker rm deepchem
```

#### 5.docker start 

启动停掉的container

```
docker start deepchem 
```

#### 6.docker restart

重启或者启动停掉的container

```
docker restart deepchem
```

#### 7.docker exec 

在运行中的container中执行命令

```
docker exec -it --user 0 deepchem bash 
docker exec -w /src/backend/ deepchem python3 dl_start.py 
```

#### 8.docker commit

将container构建成一个image

```
docker commit deepchem alex_deepchem
```

#### 9.docker rename 

修改container的name

```
docker rename deepchem deepchem-cpu 
```

#### 10.docker port

显示container映射的端口

```
$ docker port crossbar-test 
8080/tcp -> 0.0.0.0:8080
```

#### 11.docker cp

复制主机文件到container中，或者从container中复制文件到主机目录。

```
docker cp test.file deepchem:/tmp
docker cp deepchem-cpu:/config.conf ./
```

#### 12.docker logs

查看container运行输出信息

```
docker logs crossbar 
2018-06-05T05:16:10+0000 [Controller      1]      __  __  __  __  __  __      __     __
2018-06-05T05:16:10+0000 [Controller      1]     /  `|__)/  \/__`/__`|__) /\ |__)  |/  \
2018-06-05T05:16:10+0000 [Controller      1]     \__,|  \\__/.__/.__/|__)/~~\|  \. |\__/
.....
```

#### 13.docker top

查看docker中的进程

```
$ docker top deepchem-cpu 
UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
root                30358               30340               0                   6月05                pts/0               00:00:00            bash
$ ps aux |grep 30358
root     30358  0.0  0.0  20080  3736 pts/0    Ss+  Jun05   0:00 bash
```

#### 14.docker container prune 

移除所有停掉的container



## 4.Dockerfile定制镜像

Dockerfile 是一个文本文件，其内包含了一条条的**指令**，每一条指令构建一层，因此每一条指令的内容，就是描述该层应当如何构建。

以定制 `nginx` 镜像为例，在一个空白目录中，建立一个文本文件，并命名为 `Dockerfile`：

```
$ mkdir mynginx
$ cd mynginx
$ touch Dockerfile
```

Dockerfile内容为：

```
FROM nginx
RUN echo '<h1>Hello, Docker!</h1>' > /usr/share/nginx/html/index.html
```

这个 Dockerfile 很简单，一共就两行。涉及到了两条指令，`FROM` 和 `RUN`。

再使用构建image命令，生成`mynginx:v1` 镜像

```
$ docker build -t mynginx:v1 . 
```

对于最后这个`.` 的理解可以参考[这里](https://yeasy.gitbooks.io/docker_practice/content/image/build.html)。简单来说就是除了Dockerfile和构建image需要用的文件，不要在这里放其他文件。

### FROM 指定基础镜像

所谓定制镜像，那一定是以一个镜像为基础，在其上进行定制。而 `FROM` 就是指定**基础镜像**，因此一个 `Dockerfile` 中 `FROM` 是必备的指令，并且必须是**第一条指令** 。

在 [Docker Store](https://store.docker.com/) 上有非常多的高质量的官方镜像，可以直接拿来使用或者作为基础镜像。



### RUN 执行命令

`RUN` 指令是用来执行命令行命令的。`RUN` 指令在定制镜像时是最常用的指令之一。其格式有两种：

- *shell* 格式：`RUN <命令>`，就像直接在命令行中输入的命令一样。刚才写的 Dockerfile 中的 `RUN` 指令就是这种格式。

```
RUN echo '<h1>Hello, Docker!</h1>' > /usr/share/nginx/html/index.html
```

- *exec* 格式：`RUN ["可执行文件", "参数1", "参数2"]`，这更像是函数调用中的格式。



### COPY 复制文件

`COPY` 指令将从构建上下文目录中 `<源路径>` 的文件/目录复制到新的一层的镜像内的 `<目标路径>` 位置。比如：

```
COPY package.json /usr/src/app/
```

格式：

- `COPY <源路径>... <目标路径>`
- `COPY ["<源路径1>",... "<目标路径>"]`



### CMD 容器启动命令

在启动容器的时候，需要指定所运行的程序及参数。`CMD` 指令就是用于指定默认的容器主进程的启动命令的。

`CMD` 指令的格式和 `RUN` 相似，也是两种格式：

- `shell` 格式：`CMD <命令>`
- `exec` 格式：`CMD ["可执行文件", "参数1", "参数2"...]`
- 参数列表格式：`CMD ["参数1", "参数2"...]`。在指定了 `ENTRYPOINT` 指令后，用 `CMD` 指定具体的参数。



### ENV 设置环境变量

设置环境变量，无论是后面的其它指令，如 `RUN`，还是运行时的应用，都可以直接使用这里定义的环境变量。

```
ENV VERSION=1.0 DEBUG=on NAME="Happy Feet"
```

格式有两种：

- `ENV <key> <value>`
- `ENV <key1>=<value1> <key2>=<value2>...`



### WORKDIR 指定工作目录

格式为 `WORKDIR <工作目录路径>`。

使用 `WORKDIR` 指令可以来指定工作目录（或者称为当前目录），以后各层的当前目录就被改为指定的目录，如该目录不存在，`WORKDIR` 会帮你建立目录。



### USER 指定当前用户

`USER` 切换到指定用户，这个用户必须是事先建立好的，否则无法切换。

```
RUN groupadd -r redis && useradd -r -g redis redis
USER redis
RUN [ "redis-server" ]
```

格式：`USER <用户名>`

`USER` 指令和 `WORKDIR` 相似，都是改变环境状态并影响以后的层。`WORKDIR` 是改变工作目录，`USER` 则是改变之后层的执行 `RUN`, `CMD` 以及 `ENTRYPOINT` 这类命令的身份。



## 5.集群docker使用

### 好处

1、速度快
2、节省带宽
3、属于私有，可以不公开

查看集群已经拥有的docker镜像

```
curl http://172.16.10.10:5000/v2/_catalog|python2 -m json.tool
```
查看集群已拥有的docker镜像有哪些tag
```
curl http://172.16.10.10:5000/v2/paddlepaddle/paddle/tags/list|python2 -m json.tool
```
集群docker镜像下载方法
```
docker pull 172.16.10.10:5000/paddlepaddle/paddle:0.10.0
```
集群docker镜像上传方法
```
docker push 172.16.10.10:5000/python:2.7 
```



## 6.一些补充

构建出来的image有些不是基于`centos` 或者`ubuntu` 的,而是Alpine 。

`docker pull` 的时候注意查看tag，例如`nvidia/cuda` 就有众多不同的tags: https://hub.docker.com/r/nvidia/cuda/tags/

不同系统安装软件包的命令也有所不同：

```
centos: yum install app
ubuntu: apt install app
alpine: apk add app
```

### 相关资源

- `docker` 入门：https://yeasy.gitbooks.io/docker_practice/content/
- `docker hub` : https://hub.docker.com/ 
- `docker docs` : https://docs.docker.com/ 
- `Alpine` 官网：<http://alpinelinux.org/>
- `Alpine` 官方仓库：<https://github.com/alpinelinux>
- `Alpine` 官方镜像：<https://hub.docker.com/_/alpine/>
- `Alpine` 官方镜像仓库：<https://github.com/gliderlabs/docker-alpine>