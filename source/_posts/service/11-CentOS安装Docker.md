---
title: CentOS安装Docker
layout: page
date: 2016/3/13
updated: 2017/11/13
comments: false
tags: 
- docker
categories: 
- service
toc: true
---

<!-- toc -->

[TOC]

## Docker 安装

docker 简单又实用,一起来学习吧.现在官方也给出了比较全面(各种系统各种版本)的安装方法.[链接在此](https://docs.docker.com/install/).下面简单记录一下centos下的安装.

### 卸载旧版本

```
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-selinux \
                  docker-engine-selinux \
                  docker-engine
```

### yum源安装

通过添加docker仓库,只要有网在哪里都可以下载.有版本更新也可以直接一条命令解决.

#### 安装依赖包

```
sudo yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2
```

#### 添加docker仓库

```
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
```

#### 一键安装

```
sudo yum install docker-ce
sudo usermod -aG docker your-user #普通用户要使用docker需要添加到docker组
```

#### 启动docker

```
sudo systemctl start docker
docker  version #查看版本
```



### rpm包安装

下载好的rpm安装快速,可离线安装.

```
sudo yum install /path/to/package.rpm
sudo usermod -aG docker your-user #普通用户要使用docker需要添加到docker组
sudo systemctl start docker
docker  version #查看版本
```



### 脚本安装



```
curl -fsSL get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker your-user #普通用户要使用docker需要添加到docker组
sudo systemctl start docker
docker  version #查看版本
```



### 镜像加速器

在国内下载docker镜像很可能会很慢,甚至有的都不能下载.使用加速器将会提升在国内获取Docker官方镜像的速度.其实就是阿里等先把官方的镜像下载到自己的机房,定时更新然后做成一个仓库站点,供国内使用,所以要快很多.添加方法很简单,通过修改daemon配置文件`/etc/docker/daemon.json`来使用加速器.

``` bash
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://wkl1kcn8.mirror.aliyuncs.com"] #我用的阿里云加速
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```



