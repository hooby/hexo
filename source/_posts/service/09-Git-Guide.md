---
title: Git 使用指南
layout: page
date: 2016/2/13
updated: 2017/12/13
comments: false
tags: 
- git
categories: 
- service
toc: true
---

<!-- toc -->
[TOC]

# Git 使用指南

`  git命令 `   `  版本控制 `  

## Git 简介

> Git是目前世界上最先进的分布式版本控制系统。
>
> 如果你是小白，可以先看[廖雪峰的官方教程](http://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000)  

![git操作流程图](09-Git-Guide/git操作流程图.PNG)


**专用名词**

- Workspace:  工作区
- Index  / Stage:  暂存区
- Repository:  仓库区 ( 本地版本库 )
- Remote:  远程仓库 ( 远程仓库 / 远程版本库 )




## 代码库

在当前目录新建一个Git代码库

~~~
git init
~~~

新建一个目录，将其初始化为Git代码库

~~~ 
git init [project-name]
~~~

克隆代码库

~~~
git clone [url]
~~~



## Git 配置

Git的配置文件为 `.git/config` ，它可以在用户主目录下（全局配置），也可以在项目目录下（项目配置）。

显示当前Git配置

~~~ 
git config --list
~~~

编辑Git配置文件

~~~
git config -e [--global] 
~~~

设置代码时的用户信息  **建议设置此项，后面的操作会很方便**

~~~
git config [--global] user.name "[name]"
~~~

~~~
git config [--global] user.email "[email address]"
~~~



## 增加/删除文件

**操作之前先执行  `git status ` 查看一下状态**

添加文件到暂存区  **建议使用这种方式进行操作**

```
git add [file1] [file2] ...
```

添加所有改动到暂存区

~~~
git add .
~~~

删除工作区文件，并且将这次删除放入暂存区

~~~
git rm [file1] [file2] ...
~~~

停止追踪指定文件，但该文件会保留在工作区

~~~
git rm --cached [file]
~~~



## 代码提交

**操作之前先执行  `git status ` 查看一下状态**

提交暂存区到仓库区

~~~
git commit -m [message]
~~~

提交暂存区的指定文件到仓库区

~~~
git commit [file1] [file2] ... -m [message]
~~~

重做上一次commit，并包括指定文件的新变化

~~~
git commit --amend [file1] [file2] ...
~~~



## 分支

列出所有本地分支

~~~
git branch
~~~

列出所有远程分支

~~~
git branch -r
~~~

列出所有本地分支和远程分支

~~~
git branch -a
~~~

新建一个分支，但依然停留在当前分支

~~~
git branch [branch-name]
~~~

新建一个分支，并切换到该分支

~~~
git checkout -b [branch]
~~~

切换到指定分支，并更新工作区

~~~
git checkout [branch-name]
~~~

合并指定分支到当前分支

~~~
git merge [branch]
~~~


删除本地分支

~~~
git branch -d [branch-name]
~~~

删除远程分支 

~~~
git push origin --delete [branch-name]
或者 
git branch -dr [remote/branch]
~~~



## 查看信息

查看改动的文件

~~~
git status
~~~

查看当前分支的版本日志

~~~
git log
~~~

查看单行日志

~~~
git log --pretty=oneline
~~~

显示暂存区和工作区的差异

~~~
git diff
~~~


显示今天写了多少行代码

~~~
git diff --shortstat "@{0 day ago}"
~~~





## 标签

列出现有标签

```
git tag
```

创建一个含附注类型的标签

```
git tag -a v0.5.0 -m 'version 0.5.0'
```

查看相应标签的版本信息，同时显示打标签时的提交对象

```
git show v0.5.0
```

`push` 单个 `tag`

~~~
git push origin [tagname]

eg:
git push origin v1.0  #将本地v1.0的tag推送到远端服务器
~~~

`push` 所有 `tag` , 如果不起作用，在 `Git` 控制台确认账号是否有权限推送 `Tag`

~~~
git push origin --tags
~~~

删除本地标签

~~~
git tag -d [tagName]
~~~

删除远程标签

~~~
git push origin :refs/tags/[tagName]
~~~







## 远程同步

下载远程仓库的所有变动

~~~
git fetch [remote]
~~~


显示所有远程仓库

~~~
git remote -v
~~~


拉取远程仓库的变化，并与本地分支合并

~~~
git pull [remote] [branch]
~~~


上传本地指定分支到远程仓库

~~~
git push [remote] [branch]
~~~

强行推送当前分支到远程仓库，即使有冲突 **谨慎使用**

~~~
git push [remote] --force
~~~

推送所有分支到远程仓库

~~~
git push [remote] --all
~~~





## Git 证书问题

临时生效，退出shell后失效

~~~
export GIT_SSL_NO_VERIFY=true 
~~~



永久生效

~~~
echo 'export GIT_SSL_NO_VERIFY=true' >> ~/.bashrc 
~~~



win10 出现证书问题 

~~~
git config --global http.sslVerify false
~~~





## 撤销

恢复暂存区的指定文件到工作区

~~~
git checkout [file]
~~~

恢复某个commit的指定文件到暂存区和工作区

~~~
git checkout [commit] [file]
~~~

恢复暂存区的所有文件到工作区

~~~
git checkout .
~~~

重置暂存区的指定文件，与上一次commit保持一致，但工作区不变

~~~
git reset [file]
~~~

重置暂存区与工作区，与上一次commit保持一致

~~~
git reset --hard
~~~

回滚到指定版本

~~~
git reset --hard [commitId]
~~~




## 重建版本库

~~~
rm -rf .git
git init
git add . 
git commit -a 'git init  first commit'
git remote add origin <github_repo_url>
git push -f -u origin master
~~~



## 添加远程仓库

~~~
cd existing_folder
git init
git remote add origin git@git.sg-ai.com:ywguo/Test.git
git add .
git commit -m "Initial commit"
git push -u origin master
~~~







## 如何创建公钥

1. 首先启动一个Git Bash窗口（非Windows用户直接打开终端）

2. 执行：

    >cd ~/.ssh

    如果返回“… No such file or directory”，说明没有生成过SSH Key，直接进入第4步。否则进入第3步备份!

3. 备份：

   >mkdir key_backup
   >mv id_isa* key_backup

4. 生成新的Key：（引号内的内容替换为你自己的邮箱）

   >ssh-keygen -t rsa -C "your_email@youremail.com"

  输出显示：

    >Generating public/private rsa key pair. Enter file in which to save the key 
    (/Users/your_user_directory/.ssh/id_rsa):<press enter>

  直接回车，不要修改默认路劲。

    >Enter passphrase (empty for no passphrase):<enter a passphrase>
    Enter same passphrase again:<enter passphrase again>

  设置一个密码短语，在每次远程操作之前会要求输入密码短语！闲麻烦可以直接回车，不设置。

5. 成功：

    >Your identification has been saved in /Users/your_user_directory/.ssh/id_rsa.
    >Your public key has been saved in /Users/your_user_directory/.ssh/id_rsa.pub.
    >The key fingerprint is:
    >... ...

6. 提交公钥：

   6.1 找到.ssh文件夹，用文本编辑器打开“id_rsa.pub”文件，复制内容到剪贴板。

   6.2 打开 https://github.com/settings/ssh ，点击 Add SSH Key 按钮，粘贴进去保存即可。




## git设置用户名密码

设置 git 用户名／邮箱

~~~
git config --global user.name [username]
git config --global user.email [email]
~~~



但是这个仅仅是设置用户名密码，如果你的Git 源每次操作需要你输入用户名/密码验证，你依然需要每次设置，那么该如何办呢？

 git 保存用户名密码

这里主要是配置一个 config 项

有两个方法，基本上原理都是一样，都是修改 .git/config 文件



1.使用如下命令，修改 config 文件即可保存

~~~
echo "[credential]" >> .git/config
echo "    helper = store" >> .git/config
~~~



2.直接修改 .git/config 文件

在 Linux/mac 下可以直接使用 vim 工具修改 config 文件

~~~
ubuntu@VM-7-212-ubuntu:~/kernel-code/kernel-netfilter-sample-code$ vim .git/config

##修改成如下

[core]
    repositoryformatversion = 0
    filemode = true
    bare = false
    logallrefupdates = true
[remote "origin"]
    url = https://github.com/Miss-you/kernel-netfilter-sample-code.git
    fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
    remote = origin
    merge = refs/heads/master
##主要是后面这两行，如果不想保存，则删除即可
[credential]
    helper = store

##保存
~~~

这样就可以保存用户名密码，不用每次都输入了！



`git config` 查看配置

使用 `git config --list` 查看已设配置

~~~shell
feiqianyousadeMacBook-Pro:xt_GTPU yousa$ git config --list
core.excludesfile=/Users/yousa/.gitignore_global
user.name=Miss-you
user.email=snowfly1993@gmail.com
core.repositoryformatversion=0
core.filemode=true
core.bare=false
core.logallrefupdates=true
core.ignorecase=true
core.precomposeunicode=true
remote.origin.url=https://github.com/Miss-you/xt_GTPU.git
remote.origin.fetch=+refs/heads/*:refs/remotes/origin/*
branch.master.remote=origin
branch.master.merge=refs/heads/master
~~~





## 操作规范

提交到仓库 `description` 需要有意义命名

~~~
git commit -m [description]
~~~


分支请及时合并清理

~~~
git merge [branch]
git push [remote] [branch]
git branch -d [branch-name]
git push origin --delete [branch-name]
~~~


请谨慎提交大文件,例如保存切割的图片文件夹

~~~
git commit -m ["description"]
~~~


推送`dev`分支操作，本地运行项目正常再推送代码

~~~
git push [remote] [branch]

1. 检查命令行是否出现 `error` 字样的内容，如果出现请及时修改
2. 浏览器上按 `F12` 或者 `右键 -> 检查(N)` 查看`console` 输出是否有明显的 `404 500` 错误，如果有请及时解决
~~~




- **友情提示:**  目前只是现在工作中用到的命令整理，如果没有您需要的，请自行 [google](www.google.com) 

- 文档持续更新中，欢迎大家拍砖 ...

  

