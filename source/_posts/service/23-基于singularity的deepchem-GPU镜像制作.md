---
title: 基于singularity的deepchem-GPU镜像制作
layout: page
date: 2017/6/21
updated: 2017/7/11
comments: false
tags: 
- singularity
categories: 
- docker
toc: true
---



[TOC]



### 准备前提

首先你的pc要安装了singularity,下载好了cuda,cudnn,anaconda.而在集群上这些都已经准备好了.



制作基础系统镜像

```
mkdir ~/deepchem-build && cd ~/deepchem-build
cp /atlas/backup/images/centos.def ./
singularity build --sandbox centos centos.def
```



准备下载好的应用.

```
cp /atlas/backup/software/{Anaconda3-5.1.0-Linux-x86_64.sh, cudnn-9.1-linux-x64-v7.1.tgz } ./
cp /etc/yum.repos.d/base.repo ./
# base.repo 中是在安装软件过程中所依赖的绝大部分安装包和cuda, 用本地源提高速度节省时间.
cat /etc/yum.repos.d/base.repo 
    [development]
    name=development
    baseurl=ftp://172.16.10.10/centos7.2
    gpgcheck=0
    gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7

    [epel]
    name=epel
    baseurl=ftp://172.16.10.10/epel
    gpgcheck=0

    [cuda]
    name=cuda
    baseurl=ftp://172.16.10.10/cuda/cuda
    gpgcheck=0
```



在镜像中安装应用.

```
singularity shell -w centos
    cp base.repo /etc/yum.repos.d/
    yum repolist 
    yum -y install vim wget perl git curl bzip2 
    export LANG=en_US.UTF-8 #设定系统字符编码

    # install Anaconda3
	bash Anaconda3-5.1.0-Linux-x86_64.sh -f -b -p -s /usr/local/anaconda3
    export PATH=/usr/local/anaconda3/bin:$PATH

    # install nvidia-driver cuda
    yum install cuda 
    tar xf cudnn-9.1-linux-x64-v7.1.tgz -C /usr/local/
    # test 
      nvidia-smi
      nvcc --version
    
    # if you can not get gpu information by nvidia-smi
        # reload the nvidia modules
            lsmod | grep nvidia
            systemctl stop gmond
            rmmod nvidia_drm
            rmmod nvidia_modeset
            rmmod nvidia_uvm
            rmmod nvidia
			nvidia-smi 
			
        #or cannot to reboot , can change the nvidia kernel module by hand 
            rm -rf /lib/modules/3.10.0-327.el7.x86_64/extra/nvidia*
            cp /var/lib/dkms/nvidia/387.26/3.10.0-327.el7.x86_64/x86_64/module/nvidia*  /lib/modules/3.10.0-327.el7.x86_64/extra/
            nvidia-smi

    # add environment value
    cat /environment
        export LANG=en_US.UTF-8
        export LD_LIBRARY_PATH=/usr/local/cuda/lib64:$LD_LIBRARY_PATH
        export PATH=/usr/local/anaconda3/bin:/usr/local/cuda/bin:$PATH

    # install tensorflow-gpu and keras and other modules can install by conda 
    cat ~/.condarc 
        channels:
          - https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/menpo/
          - https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/bioconda/
          - https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/msys2/
          - https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/conda-forge/
          - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/
          - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
          - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/mro/
          - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/pro/
          - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/r/
          - defaults
        show_channel_urls: true

    conda install tensorflow-gpu==1.6.0
    conda install keras-gpu
    conda install pytest pytest-html pytest-cov pydicom gevent numba autobahn pymongo redis rq txaio twisted

    conda install -c menpo opencv3 
    # install deepchem
    conda install -c deepchem -c rdkit -c conda-forge -c omnia deepchem-gpu=2.0.0

    # install openslide and autobahn_autoreconnect etc. can only install by pip 
    pip install openslide-python autobahn_autoreconnect
    wget http://ftp.gnu.org/pub/gnu/libiconv/libiconv-1.15.tar.gz && tar xf libiconv-1.15.tar.gz && cd libiconv-1.15
    ./configure && make -j 12 && make install 
    ln -sv /usr/local/lib/libiconv.so.2 /usr/lib/libiconv.so.2
    ldconfig

    cd .. && rm -rf libiconv-1.15
    
    conda clean -a
    yum clean all 
    exit
```



生成只读镜像

```
singularity build centos_gpu_tf6_n387_deepchem.simg centos
```

