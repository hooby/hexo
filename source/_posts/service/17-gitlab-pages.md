---
title: gitlab-pages配置说明
layout: page
date: 2016/7/15
updated: 2016/9/12
comments: false
tags: 
- hexo
categories: 
- gitlab
toc: true
---

<!-- toc -->

[TOC]

### pages 介绍

gitlab-pages是gitlab-ci的一个功能. 它上传静态页面文件到gitlab中, gitlab就使用这些静态页面作为网站展示出来.

gitlab-pages 功能:

```
Create websites for your GitLab projects, groups, or user account.
# 为你的gitlab项目搭建网站

Use any static website generator: Jekyll, Middleman, Hexo, Hugo, Pelican, and more.
# 可以使用各种网站静态生成器 

Connect your custom domain(s) and TLS certificates.
# 自定义域名 以及支持TLS证书安全保证

Host your static websites on GitLab.com for free, or on your own GitLab instance.
# 你的网站可以免费放到gitlab上
```

### pages 配置

搭建pages必须满足两个条件

1. 所有的静态内容必须放到`public` 目录下

2.  `.gitlab-ci.yml`  文件必须配置 `artifacts` 选项的`paths` 参数为`public` 

   [官网描述](https://docs.gitlab.com.cn/ce/ci/yaml/#pages) 

```
pages:
...
  script:
  - mv dist public
  artifacts:
    paths:
    - public
...
```

### pages 搭建示例

#### 准备条件:

1. gitlab服务
2. gitlab-runner

#### 集群搭建示例:

集群上已经搭建了gitlab和gitlab-runner,可以直接使用.

1. [公司博客](https://blog.sg-ai.com/doc/)  

`.gitlab-ci.yml:`

```
pages:
  script:
  - "singularity exec /scratch/containers/user.simg npm install "
  - "singularity exec /scratch/containers/user.simg hexo generate "
  cache:
    paths:
    - node_modules/
  artifacts:
    paths:
    - public
  only:
  - master
# cache选项 可以把node_modules缓存起来,下次执行script命令之前就会先加载cache目录,加快npm install

```

自动部署完成之后,打开项目`setting/pages`  下面的网址就是访问连接.

1. [Vue](https://git.sg-ai.com/ywguo/vue-cli-project-template) 

`vue-cli-project-template/config/index.js:` 

````
...
build: {
    // Template for index.html
    index: path.resolve(__dirname, '../dist/vue-cli-project-template/index.html'),

    // Paths
    assetsRoot: path.resolve(__dirname, '../dist/vue-cli-project-template'),
    assetsSubDirectory: 'static',
    assetsPublicPath: '/vue-cli-project-template/',
...
}
# 注意:
# assetsRoot: path.resolve 决定你生成静态页面的路径,这个路径要对应到pages配置的public中
````

`.gitlab-ci.yml:` 

```
pages:
  script:
  - "singularity exec /scratch/containers/backend.simg npm install"
  - "singularity exec /scratch/containers/backend.simg npm run build"
  - "cp -r dist/vue-cli-project-template public"
  - "cp public/index.html public/404.html"
  cache:
    paths:
    - node_modules/
  artifacts:
    paths:
    - public
  only:
  - master
```



#### gitlab官网搭建示例:

gitlab官网给我们免费提供了gitlab-runner, 所以也是可以直接使用.

1. [Vue](https://gitlab.com/hooby/vue-cli-project-template) 

这边的vue对build做了修改

`vue-cli-project-template/config/index.js:` 

```
  ...
  build: {
    // Template for index.html
    index: path.resolve(__dirname, '../public/index.html'),

    // Paths
    assetsRoot: path.resolve(__dirname, '../public'),
    assetsSubDirectory: 'static',
    assetsPublicPath: '/vue-cli-project-template/',
    ...
}
```

`.gitlab-ci.yml:` 

```
pages:
  image: node:8.11.1
  script:
    - "npm install"
    - "npm run build"
    # - "cp -r dist/vue-cli-project-template public"
    - "cp public/index.html public/404.html"
  cache:
    paths:
    - node_modules/
    policy: pull
  artifacts:
    paths:
    - public
  only:
  - master

```

