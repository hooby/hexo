---
title: shadowsocks server
layout: page
date: 2017/10/1
updated: 2018/7/15
comments: false
tags: 
- proxy
- shadowsocks
- bbr
categories: 
- vps
toc: true
---

<!-- toc -->

[TOC]

vultr的vps性价比越来越高了，带宽不限，5美刀的流量都有1024G/月 .废话不多说，直接开始说搭建。

首先连接上vultr的一个vps。

# 安装shadowsocks

## ubuntu 18

```
apt install shadowsocks
```



## 其它系统

安装依赖shadowsocks依赖包

```
apt install python-pip python-m2crypto -y 
pip install setuptools 
pip install wheel  
```

安装shadowsocks

```
pip install shadowsocks
```



## 修改配置文件

```
cat /etc/shadowsocks.json
{
    "server": "0.0.0.0",
    "server_port": 55555, #设置你的ss服务端口
    # "server_port": [55555, 44444], #开启多个端口
    "password": "your_password", #设置ss服务连接密码
    "timeout": 300,
    "method": "aes-256-cfb",
    "fast_open": false
}

```

## 开启ss服务

```
ssserver -c /etc/shadowsocks.json -d start
```

系统重启之后，自动开启

```
cat /etc/rc.local 

  ulimit -n 51200
  ssserver -c /etc/shadowsocks.json -d start
```





# 添加用户和端口

为什么要添加用户？

root用户密码太他喵的难敲了！一堆很长的随机字符串，各种配合shift的都有，登录一次贼费劲.

![vultr](34-vultr/vultr_information.png)

而且在console端不可以粘贴。所以换一个用户，帮助太大了。

![vultr](34-vultr/vultr_console.png)

操作如下

```
adduser superman
...

usermod -G sudo superman
#添加sudo权限
```



为什么要修改登录端口？ 

安全和性能考虑，我创建好的vps，不一会儿就有两个尝试登录我的vps。

![vultr](34-vultr/lastb.png)

登录默认使用22，如果不修改，每天都有大量的试图入侵的要尝试登录你的vps，你的vps要不断的验证身份，性能自然有所下降。万一还让他攻破了，那你就是裸奔了。修改登录端口之后，别人连端口都不知道，登录就无从说起了。操作如下：

```
cat /etc/ssh/sshd_config 
...
  #Port 22
  Port 60022 #自己定义一个端口 大于1024即可
 
 systemctl restart ssh
 #重启服务，登录服务生效，原来的22号端口就不能登录了哦
```



# 系统优化

vps启动自然有很多服务跟跟着启动，以及对文件数据网络等处理都有一个正常值的限制。那既然你的vps只是用来搭建shadowsocks server，那就可以针对这个做一下优化。

```
cat /etc/sysctl.d/local.conf

# max open files
  fs.file-max = 51200
  net.core.rmem_max = 67108864
  net.core.wmem_max = 67108864
  net.core.rmem_default = 65536
  net.core.wmem_default = 65536
  net.core.netdev_max_backlog = 4096
  net.core.somaxconn = 4096
  net.ipv4.tcp_syncookies = 1
  net.ipv4.tcp_tw_reuse = 1
  net.ipv4.tcp_tw_recycle = 0
  net.ipv4.tcp_fin_timeout = 30
  net.ipv4.tcp_keepalive_time = 1200
  net.ipv4.ip_local_port_range = 10000 65000
  net.ipv4.tcp_max_syn_backlog = 4096
  net.ipv4.tcp_max_tw_buckets = 5000
  net.ipv4.tcp_fastopen = 3
  # TCP receive buffer
  net.ipv4.tcp_rmem = 4096 87380 67108864
  # TCP write buffer
  net.ipv4.tcp_wmem = 4096 65536 67108864
  net.ipv4.tcp_mtu_probing = 1
  # net.ipv4.tcp_congestion_control = hybla #bbr 加速需要屏蔽掉这个
```

重新加载生效

```
sysctl --system
```



# bbr 加速

bbr加速可以是你的访问更快。就酱。

[bbr加速参考文档](https://teddysun.com/489.html)

```
wget --no-check-certificate https://github.com/teddysun/across/raw/master/bbr.sh && chmod +x bbr.sh && ./bbr.sh
```

安装完成后，重启生效。因为更换了较新的内核。



# serverSpeeder 加速

锐速（serverSpeeder ）的目的跟bbr一样，搞一个就可以了。不过bbr是升级内核，锐速是把内核改成固定的稳定版本。

[锐速参考文档](http://www.vpsdx.com/2812.html)

```
wget --no-check-certificate -O appex.sh https://raw.githubusercontent.com/0oVicero0/serverSpeeser_Install/master/appex.sh && chmod +x appex.sh && bash appex.sh install
```

使用方法:
启动命令 /appex/bin/lotServer.sh start
状态查询 /appex/bin/lotServer.sh status
停止加速 /appex/bin/lotServer.sh stop



好了，这就是从搭建ss server，到方便登录到保证安全，以及系统优化，到最后加速的全部过程。