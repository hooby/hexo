---
title: Java简要介绍
layout: page
date: 2015/11/25
updated: 2016/6/13
comments: false
tags: 
- Java
categories: 
- Java
---

<!-- toc -->

[TOC]

```
Java：
    Sun, Green Project, Oak,  James Gosling;
    1995：Java 1.0, Write once, Run Anywhere;
    1996：JDK（Java Development Kit），包含类库、开发工具(javac)、JVM（SUN Classic VM）
        JDK 1.0,  Applet, AWT
    1997：JDK 1.1
    1998: JDK  1.2
        Sun分拆Java技术为三个方向：
            J2SE：Standard Edition
            J2EE：Enterprise Edition
            J2ME：Mobile Edition
            
        代表性技术：EJB，java plugin, Swing, JIT(Just In Time，即时编译)
        
    2000：JDK 1.3
        HotSpot VM
    2002：JDK 1.4
    
    2006：Sun开源了Java技术，GPL，建立一个称为OpenJDK组织；
        Java 2 SE, Java 2 EE, Java 2 ME
        
    2011：JDK 1.7
    2014：JDK 1.8
    2016：JDK 1.9
    
Java代码的运行：
    *.java(source code) --> javac --> *.class(bytecode)
        jvm：class loader，加载程序的类文件，及程序的类文件依赖到的其它的类文件而后运行； 整个运行表现为一个jvm进程；
            threads；
            
java技术体系：
    Java编程语言
    Java Class文件格式
    Java API 
    Java VM 
        class loader
        执行引擎
        
    JVM运行时区域：
        方法区：线程共享； 用于存储被JVM加载的class信息、常量、静态变量、方法等；
        堆：是jvm所管理的内存中占用空间最大的一部分；也是GC管理的主要区域；存储对象；
        Java栈：线程私有，存储 线程自己的局部变量；
        PC寄存器：线程私有的内存空间，程序的指令指针；
        本地方法栈：  
    
安装JDK
    了解当前的java环境：
         ~]# java  -version
         
    OpenJDK： 
        java-VERSION-openjdk：
            The OpenJDK runtime environment.
        java-VERSION-openjdk-headless：
             The OpenJDK runtime environment without audio and video support.
        java-VERSION-openjdk-devel：
            The OpenJDK development tools.
            
        CentOS 7：
            VERSION：1.6.0, 1.7.0, 1.8.0
            java-1.8.0-openjdk-devel
        注意：多版本并存时，可使用 alternatives命令设定默认使用的版本；
        
    Oracle JDK：
        安装相应版本的rpm包；
            jdk-VERSION-OS-ARCH.rpm
            例如:jdk-1.8.0_25-linux-x64.rpm 
            
    注意：安装完成后，要配置JAVA_HOME环境变量，指向java的安装路径；
        OpenJDK：
            JAVA_HOME=/usr
        Oracle JDK:
            JAVA_HOME=/usr/java/jdk_VERSION
    
    
    
Java 2 EE：
    CGI: Common Gateway Interface
        
    Servlet：
        类库；web app；
        Servlet container, Servlet Engine
        
    JSP: Java Server Page
        <html>
            <title>TITLE</title>
            <body>
                <h1>...</h1>
                <%
                    ... java code...
                %>
            </body>
        </html>
        
        .jsp -->jasper--> .java --> javac --> .class --> jvm 
        
        注意：基于jasper将静态输出的数据转为java代码进行输出；
        
    JSP Container：
        JSP + Servlet Container
        
        Java Web Server：JWS
        ASF：JServ
            
            Tomcat 3.x
            Tomcat 4.x
                Catalina
                
            http://tomcat.apache.org/
        
        商业实现：
            WebSphere, WebLogic, Oc4j, Glassfish, Geronimo, JOnAS, JBoss, ...
        开源实现：
            Tomcat, Jetty, Resin, ...
            
        Tomcat： Java 2 EE技术体系的不完整实现； 
        
JVM常用的分析工具： 
    jps：用来查看运行的所有jvm进程；
    jinfo：查看进程的运行环境参数，主要是jvm命令行参数；
    jstat：对jvm应用程序的资源和性能进行实时监控；
    jstack：查看所有线程的运行状态；
    jmap：查看jvm占用物理内存的状态；
    jconsole：
    jvisualvm：

    官方文档：https://docs.oracle.com/javase/8/docs/technotes/tools/unix/
    
    jps：Java virutal machine Process Status tool，
        jps [-q] [-mlvV] [<hostid>]
            -q：静默模式；
            -v：显示传递给jvm的命令行参数；
            -m：输出传入main方法的参数；
            -l：输出main类或jar完全限定名称；
            -V：显示通过flag文件传递给jvm的参数；
            [<hostid>]：主机id，默认为localhost；
            
    jinfo：输出给定的java进程的所有配置信息；
        jinfo [option] <pid>
            -flags：to print VM flags
            -sysprops：to print Java system properties
            -flag <name>：to print the value of the named VM flag
            
    jstack：查看指定的java进程的线程栈的相关信息；
        jstack [-l] <pid>
        jstack -F [-m] [-l] <pid>
            -l：long listings，会显示额外的锁信息，因此，发生死锁时常用此选项；
            -m：混合模式，既输出java堆栈信息，也输出C/C++堆栈信息；
            -F：当使用“jstack -l PID"无响应，可以使用-F强制输出信息；
            
    jstat：输出指定的java进程的统计信息
        jstat -help|-options
        jstat -<option> [-t] [-h<lines>] <vmid> [<interval> [<count>]]
        
        # jstat -options
            -class：class loader
            -compiler：JIT
            -gc：gc
            -gccapacity：统计堆中各代的容量
            -gccause：
            -gcmetacapacity
            -gcnew：新生代
            -gcnewcapacity
            -gcold：老年代
            -gcoldcapacity
            -gcutil
            -printcompilation
            
        [<interval> [<count>]]
            interval：时间间隔，单位是毫秒；
            count：显示的次数；
            
        -gc：
            S0C  ：S0 总大小
            S1C  ：S1 总大小
            S0U  ：S0 已用大小
            S1U  ：S1 已用大小
            EC   ：Eden区总大小 
            EU   ：Eden区已用大小   
            OC   ：老年代总大小     
            OU   ：老年代已用大小   
            MC   ：Metaspace 总大小 
            MU   ：Metaspace 已用大小
            CCSC ：CompressedClassSpace总大小
            CCSU ：CompressedClassSpace已用大小
            YGC  ：Young GC 次数
            YGCT ：Young GC 消耗总时间
            FGC  ：FullGC 次数
            FGCT ：FullGC 消耗总时间
            GCT  ：GC总消耗时间

            参考文档：https://docs.oracle.com/javase/8/docs/technotes/tools/unix/jstat.html
            
    jmap：Memory Map, 用于查看堆内存的使用状态； 
    jhat：Java Heap Analysis Tool
        jmap [option] <pid>
        
        查看堆空间的详细信息：
            jmap -heap <pid>
            
        查看堆内存中的对象的数目：
            jmap -histo[:live] <pid>
                live：只统计活动对象；
                
        保存堆内存数据至文件中，而后使用jvisualvm或jhat进行查看：
            jmap -dump:<dump-options> <pid>
                dump-options:
                live         dump only live objects; if not specified, all objects in the heap are dumped.
                format=b     binary format
                file=<file>  dump heap to <file>    
```

