---
title: tomcat简要介绍
layout: page
date: 2015/11/25
updated: 2016/6/13
comments: false
tags: 
- tomcat
categories: 
- service
---

<!-- toc -->

[TOC]

Tomcat 服务器是一个免费的开放源代码的Web 应用服务器,Tomcat 实际上运行JSP 页面和Servlet.它还是一个Servlet和JSP容器.Tomcat： Java 2 EE技术体系的不完整实现

```
Tomcat：
    使用java语言编写：
    
    Tomcat的核心组件：server.xml
        <Server>
            <Service>
                <connector/>
                <connector/>
                ...
                <Engine>
                    <Host>
                        <Context/>
                        <Context/>
                        ...
                    </Host>
                    <Host>
                        ...
                    </Host>
                    ...
                </Engine>
            </Service>
        </Server>

        每一个组件都由一个Java“类”实现，这些组件大体可分为以下几个类型：
            顶级组件：Server
            服务类组件：Service
            连接器组件connector：http, https, ajp
            容器类：Engine, Host, Context
            被嵌套类：valve, logger, realm, loader, manager, ...
            集群类组件：listener, cluster, ...
            
    安装Tomcat:
        Base Repo：
            tomcat, tomcat-lib, tomcat-admin-webapps, tomcat-webapps, tomcat-docs-webapp
            
        Tomcat binary release：
            # tar xf apache-tomcat-VERSION.tar.gz  -C /usr/local/
            # cd /usr/local
            # ln -sv apache-tomcat-VERSION  tomcat
            
            
            /etc/profile.d/tomcat.sh 
                export CATALINA_BASE=/usr/local/tomcat
                export PATH=$CATALINA_BASE/bin:$PATH    
    
tomcat程序环境：
    
    tomcat的目录结构
        bin：脚本，及启动时用到的类；
        conf：配置文件目录；
        lib：库文件，Java类库，jar；
        logs：日志文件目录；
        temp：临时文件目录；
        webapps：webapp的默认目录；
        work：工作目录；
        
    rpm包安装的程序环境：
        配置文件目录：/etc/tomcat
            主配置文件：server.xml 
        webapps存放位置：/var/lib/tomcat/webapps/
            examples
            manager
            host-manager
            docs
        Unit File：tomcat.service
        环境配置文件：/etc/sysconfig/tomcat
        
    tomcat的配置文件：
        server.xml：主配置文件；
        web.xml：每个webapp只有“部署”后才能被访问，它的部署方式通常由web.xml进行定义，其存放位置为WEB-INF/目录中；此文件为所有的webapps提供默认配置；
        context.xml：每个web都可以专用的配置文件，它通常由专用的配置文件context.xml来定义，其存放位置为WEB-INF/目录中；此文件为所有的webapps提供默认配置；
        tomcat-users.xml：用户认证的账号和密码文件；
        catalina.policy：当使用-security选项启动tomcat时，用于为tomcat设置安全策略； 
        catalina.properties：Java属性的定义文件，用于设定类加载器路径，以及一些与JVM调优相关参数；
        logging.properties：日志系统相关的配置；               
        
    # catalina.sh --help
        debug             Start Catalina in a debugger
        debug -security   Debug Catalina with a security manager
        jpda start        Start Catalina under JPDA debugger
        run               Start Catalina in the current window
        run -security     Start in the current window with security manager
        start             Start Catalina in a separate window
        start  -security   Start in a separate window with security manager
        stop              Stop Catalina, waiting up to 5 seconds for the process to end
        stop n            Stop Catalina, waiting up to n seconds for the process to end
        stop -force       Stop Catalina, wait up to 5 seconds and then use kill -KILL if still running
        stop n -force     Stop Catalina, wait up to n seconds and then use kill -KILL if still running
        configtest        Run a basic syntax check on server.xml - check exit code for result
        version           What version of tomcat are you running?   
        
    JSP WebAPP的组织结构：
        /: webapps的根目录
            index.jsp：主页；
            WEB-INF/：当前webapp的私有资源路径；通常用于存储当前webapp的web.xml和context.xml配置文件；
            META-INF/：类似于WEB-INF/；
            classes/：类文件，当前webapp所提供的类；
            lib/：类文件，当前webapp所提供的类，被打包为jar格式；
            
    webapp归档格式：
        .war：webapp
        .jar：EJB的类打包文件；
        .rar：资源适配器类打包文件；
        .ear：企业级webapp；
        
部署(deploy)webapp的相关操作：
    deploy：将webapp的源文件放置于目标目录(网页程序文件存放目录)，配置tomcat服务器能够基于web.xml和context.xml文件中定义的路径来访问此webapp；将其特有的类和依赖的类通过class loader装载至JVM；
        部署有两种方式：
            自动部署：auto deploy
            手动部署:
                冷部署：把webapp复制到指定的位置，而后才启动tomcat；
                热部署：在不停止tomcat的前提下进行部署；
                    部署工具：manager、ant脚本、tcd(tomcat client deployer)等；                    
    undeploy：反部署，停止webapp，并从tomcat实例上卸载webapp；
    start：启动处于停止状态的webapp；
    stop：停止webapp，不再向用户提供服务；其类依然在jvm上；
    redeploy：重新部署；
    
手动提供一测试类应用，并冷部署：
    # mkidr  -pv  /usr/local/tomcat/webapps/test/{classes,lib,WEB-INF}
    创建文件/usr/local/tomcat/webapps/test/index.jsp 
        <%@ page language="java" %>
        <%@ page import="java.util.*" %>
        <html>
            <head>
                <title>Test Page</title>
            </head>
            <body>
                <% out.println("hello world");
                %>
            </body>
        </html>     
        
tomcat的两个管理应用:
    manager
    host-manager
    
tomcat的常用组件配置：
    
    Server：代表tomcat instance，即表现出的一个java进程；监听在8005端口，只接收“SHUTDOWN”。各server监听的端口不能相同，因此，在同一物理主机启动多个实例时，需要修改其监听端口为不同的端口； 
    
    Service：用于实现将一个或多个connector组件关联至一个engine组件；
    
    Connector组件：
        负责接收请求，常见的有三类http/https/ajp；
    
        进入tomcat的请求可分为两类：
            (1) standalone : 请求来自于客户端浏览器；
            (2) 由其它的web server反代：来自前端的反代服务器；
                nginx --> http connector --> tomcat 
                httpd(proxy_http_module) --> http connector --> tomcat
                httpd(proxy_ajp_module) --> ajp connector --> tomcat 
                
        属性：
            port="8080" 
            protocol="HTTP/1.1"
            connectionTimeout="20000"
            
            address：监听的IP地址；默认为本机所有可用地址；
            maxThreads：最大并发连接数，默认为150；
            enableLookups：是否启用DNS查询功能；
            acceptCount：等待队列的最大长度；
            secure：
            sslProtocol：
            
    Engine组件：Servlet实例，即servlet引擎，其内部可以一个或多个host组件来定义站点； 通常需要通过defaultHost来定义默认的虚拟主机；
    
        属性：
            name=
            defaultHost="localhost"
            jvmRoute=
            
    Host组件：位于engine内部用于接收请求并进行相应处理的主机或虚拟主机，示例：
         <Host name="localhost"  appBase="webapps"
            unpackWARs="true" autoDeploy="true">
        </Host>
        
        常用属性说明：
            (1) appBase：此Host的webapps的默认存放目录，指存放非归档的web应用程序的目录或归档的WAR文件目录路径；可以使用基于$CATALINA_BASE变量所定义的路径的相对路径；
            (2) autoDeploy：在Tomcat处于运行状态时，将某webapp放置于appBase所定义的目录中时，是否自动将其部署至tomcat；
            
            示例：
              <Host name="tc1.magedu.com" appBase="/appdata/webapps" unpackWARs="true" autoDeploy="true">
            </Host>
            
            # mkdir -pv /appdata/webapps
            # mkdir -pv /appdata/webapps/ROOT/{lib,classes,WEB-INF}
            提供一个测试页即可；
            
    Context组件:
        示例：
            <Context path="/PATH" docBase="/PATH/TO/SOMEDIR" reloadable=""/>
            
    Valve组件：
        <Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs"
            prefix="localhost_access_log" suffix=".txt"
            pattern="%h %l %u %t &quot;%r&quot; %s %b" />
            
        Valve存在多种类型：
            定义访问日志：org.apache.catalina.valves.AccessLogValve
            定义访问控制：org.apache.catalina.valves.RemoteAddrValve 
            
             <Valve className="org.apache.catalina.valves.RemoteAddrValve" deny="172\.16\.100\.67"/>
             

             
LNMT：Linux Nginx MySQL Tomcat 
    Client (http) --> nginx (reverse proxy)(http) --> tomcat  (http connector)
    
    location / {
        proxy_pass http://tc1.magedu.com:8080;
    }
    
    location ~* \.(jsp|do)$ {
        proxy_pass http://tc1.magedu.com:8080;
    }
    
LAMT：Linux Apache(httpd) MySQL Tomcat 
    httpd的代理模块：
        proxy_module
        proxy_http_module：适配http协议客户端；
        proxy_ajp_module：适配ajp协议客户端；
        
    Client (http) --> httpd (proxy_http_module)(http) --> tomcat  (http connector)
    Client (http) --> httpd (proxy_ajp_module)(ajp) --> tomcat  (ajp connector)
    Client (http) --> httpd (mod_jk)(ajp) --> tomcat  (ajp connector)
    
    proxy_http_module代理配置示例：
        <VirtualHost *:80>
            ServerName      tc1.magedu.com
            ProxyRequests Off
            ProxyVia        On
            ProxyPreserveHost On
            <Proxy *>
                Require all granted
            </Proxy>
            ProxyPass / http://tc1.magedu.com:8080/ 
            ProxyPassReverse / http://tc1.magedu.com:8080/ 
            <Location />
                Require all granted
            </Location>
        </VirtualHost>
        
    proxy_ajp_module代理配置示例：
        <VirtualHost *:80>
            ServerName      tc1.magedu.com
            ProxyRequests Off
            ProxyVia        On
            ProxyPreserveHost On
            <Proxy *>
                Require all granted
            </Proxy>
            ProxyPass / ajp://tc1.magedu.com:8009/ 
            ProxyPassReverse / ajp://tc1.magedu.com:8009/ 
            <Location />
                Require all granted
            </Location>
        </VirtualHost>
        
课外实践：client --> nginx --> httpd --> tomcat
proxy_http_module)(http) --> tomcat  (http connector)
    Client (http) --> httpd (proxy_ajp_module)(ajp) --> tomcat  (ajp connector)
    Client (http) --> httpd (mod_jk)(ajp) --> tomcat  (ajp connector)
    
    proxy_http_module代理配置示例：
        <VirtualHost *:80>
            ServerName      tc1.magedu.com
            ProxyRequests Off
            ProxyVia        On
            ProxyPreserveHost On
            <Proxy *>
                Require all granted
            </Proxy>
            ProxyPass / http://tc1.magedu.com:8080/ 
            ProxyPassReverse / http://tc1.magedu.com:8080/ 
            <Location />
                Require all granted
            </Location>
        </VirtualHost>
        
    proxy_ajp_module代理配置示例：
        <VirtualHost *:80>
            ServerName      tc1.magedu.com
            ProxyRequests Off
            ProxyVia        On
            ProxyPreserveHost On
            <Proxy *>
                Require all granted
            </Proxy>
            ProxyPass / ajp://tc1.magedu.com:8009/ 
            ProxyPassReverse / ajp://tc1.magedu.com:8009/ 
            <Location />
                Require all granted
            </Location>
        </VirtualHost>
        
课外实践：client --> nginx --> httpd --> tomcat
```

