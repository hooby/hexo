---
title: 运维自动化工具Ansible
layout: page
date: 2016/7/2
updated: 2017/11/13
comments: false
tags: 
- Ansible
categories: 
- Linux
toc: true
---



<!-- toc -->

[TOC]



# 一、Ansible概述

Ansible是今年来越来越火的一款开源运维自动化工具，通过Ansible可以实现运维自动化，提高运维工程师的工作效率，减少人为失误。Ansible通过本身集成的非常丰富的模块可以实现各种管理任务，其自带模块超过上千个。更为重要的是，它操作非常简单，即使小白也可以轻松上手，但它提供的功能又非常丰富，在运维领域，几乎可以做任何事。

## 1、Ansible特点

> Ansible自2012年发布以来，很快在全球流行，其特点如下：
>
> - Ansible基于Python开发，运维工程师对其二次开发相对比较容易；
> - Ansible丰富的内置模块，几乎可以满足一切要求；
> - 管理模式非常简单，一条命令可以影响上千台主机；
> - 无客户端模式，底层通过SSH通信；
> - Ansible发布后，也陆续被AWS、Google Cloud Platform、Microsoft Azure、Cisco、HP、VMware、Twitter等大公司接纳并投入使用；

# 二、Ansible的角色

- 使用者：如何使用Ansible实现自动化运维？
- Ansible工具集：Ansible可以实现的功能？
- 作用对象：Ansible可以影响哪些主机？

## 1、使用者

如下图所示：Ansible使用者可以采用多种方式和Ansible交互，图中展示了四种方式：

- CMDB：CMDB存储和管理者企业IT架构中的各项配置信息，是构建ITIL项目的核心工具，运维人员可以组合CMDB和Ansible，通过CMDB直接下发指令调用Ansible工具集完成操作者所希望达到的目标；
- PUBLIC/PRIVATE方式：Ansible除了丰富的内置模块外，同时还提供丰富的API语言接口，如PHP、Python、PERL等多种流行语言，基于PUBLIC/PRIVATE，Ansible以API调用的方式运行；
- Ad-Hoc命令集：Users直接通过Ad-Hoc命令集调用Ansible工具集来完成任务；
- Playbooks：Users预先编写好Ansible Playbooks，通过执行
  Playbooks中预先编排好的任务集，按序执行任务；
  ![简单聊一聊Ansible自动化运维](13-ansible/1dd392c497a0154708e000017ef378b0.png)

## 2、Ansible工具集

Ansible工具集包含Inventory、Modules、Plugins和API。其中：

- Inventory：用来管理设备列表，可以通过分组实现，对组的调用直接影响组内的所有主机；

- Modules：是各种执行模块，几乎所有的管理任务都是通过模块执行的；

- Plugins：提供了各种附加功能；

- API：为编程人员提供一个接口，可以基于此做Ansible的二次开发；

  **具体表现如下：**

> - Ansible Playbooks：任务脚本，编排定义Ansible任务及的配置文件，由Ansible按序依次执行，通常是JSON格式的YML文件；
> - Inventory：Ansible管理主机清单；
> - Modules：Ansible执行命令功能模块，多数为内置的核心模块，也可自定义；
> - Plugins：模块功能的补充，如连接类型插件、循环插件、变量插件、过滤插件等，该功能不太常用；
> - API：供第三方程序调用的应用程序编程接口；
> - Ansible：该部分图中表现得不太明显，组合Inventory、API、Modules、Plugins可以理解为是Ansible命令工具，其为核心执行工具；

## 3、作用对象

Ansible的作用对象不仅仅是Linux和非Linux操作系统的主机，也可以作用于各类PUBLIC/PRIVATE、商业和非商业设备的网络设施。

使用者使用Ansible或Ansible-Playbooks时，在服务器终端输入Ansible的Ad-Hoc命令集或Playbooks后，Ansible会遵循预选安排的规则将Playbooks逐步拆解为Play，再将Play组织成Ansible可以识别的任务，随后调用任务涉及的所有模块和插件，根据Inventory中定义的主机列表通过SSH将任务集以临时文件或命令的形式传输到远程客户端执行并返回执行结果，如果是临时文件则执行完毕后自动删除。

# 三、Ansible的配置

## 1、Ansible安装

Ansible的安装部署非常简单，以RPM安装为例，其依赖软件只有Python和SSH，且系统默认均已安装。Ansible的管理端只能是Linux，如Redhat、Debian、Centos。

### 1）通过YUM安装Ansible

可以自行从互联网上直接下载Ansible所需软件包

```
[root@centos01 ~]# cd /mnt/ansiblerepo/ansiblerepo/repodata/
[root@centos01 ansiblerepo]# vim /etc/yum.repos.d/local.repo
[local]
name=centos
baseurl=file:///mnt/ansiblerepo/ansiblerepo  <!--修改yum路径-->
enabled=1
gpgcheck=0
[root@centos01 ~]# yum -y install ansible
                <!--安装Ansible自动化运维工具-->
```

### 2）验证安装结果

```
[root@centos01 ~]# ansible --version
    <!--如果命令可以正常执行，则表示Ansible工具安装成功-->
ansible 2.3.1.0
  config file = /etc/ansible/ansible.cfg
  configured module search path = Default w/o overrides
  python version = 2.7.5 (default, Nov  6 2016, 00:28:07) [GCC 4.8.5 20150623 (Red Hat 4.8.5-11)]
```

### 3）创建SSH免交互登录

Ansible通过SSH对设备进行管理，而SSH包含两种认证方式：一种是通过密码认证，另一种是通过密钥对验证。前者必须和系统交互，而后者是免交互登录。如果希望通过Ansible自动管理设备，应该配置为免交互登录被管理设备。

```
[root@centos01 ~]# ssh-keygen -t rsa  <!--生成密钥对-->
Generating public/private rsa key pair.
Enter file in which to save the key (/root/.ssh/id_rsa):<!--密钥对存放路径-->
Created directory '/root/.ssh'.
Enter passphrase (empty for no passphrase):    
       <!--输入私钥保护密码，直接按Enter键表示无密码-->
Enter same passphrase again:    <!--再次输入-->
Your identification has been saved in /root/.ssh/id_rsa.
Your public key has been saved in /root/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:cJz6NRTrvMDxX+Jpce6LRnWI3vVEl/zvARL7D10q9WY root@centos01
The key's randomart image is:
+---[RSA 2048]----+
|          .   . .|
|       . . +   oo|
|      . = o o. oo|
|       = * o..+ *|
|      . S *.=+=*+|
|       . o =+XooE|
|        . ..=.++.|
|           ..o ..|
|           .. o. |
+----[SHA256]-----+
[root@centos01 ~]# ssh-copy-id -i .ssh/id_rsa.pub  root@192.168.100.20   <!--复制公钥到远端192.168.100.20-->
[root@centos01 ~]# ssh-copy-id -i .ssh/id_rsa.pub  root@192.168.100.30    <!--复制公钥到远端192.168.100.30-->
```

**至此，已经完成Ansible的部署，接下来就可以通过Ansible对设备进行管理了。**

## 2、Ansible配置

**Inventory是Ansible管理主机信息的配置文件，相当于系统Hosts文件的功能，默认存放在/etc/ansible/hosts。在hosts文件中，通过分组来组织设备，Ansible通过Inventory来定义主机和分组，通过在ansible命令中使用选项-i或--inventory-file来指定Inventory。**

```
[root@centos01 ~]# ansible -i /etc/ansible/hosts web -m ping
```

**如果使用默认的Inventory文件（/etc/ansible/hosts），也可以不指定Inventory文件，例如：**

```
[root@centos01 ~]# ansible web -m ping
```

Ansible通过设备列表以分组的方式添加到/etc/ansible/hosts文件来实现对设备的管理，所以在正式管理之前，首先要编写好hosts文件。hosts文件中，以[ ]包含的部分代表组名，设备列表支持主机名和IP地址。默认情况下，通过访问22端口（SSH）来管理设备。若目标主机使用了非默认的SSH端口，还可以在主机名称之后使用冒号加端口标明，以行为单位分隔配置。另外，hosts文件还支持通配符。

```
[root@centos01 ~]# vim /etc/ansible/hosts
............   <!--此处省略部分内容-->
[web]
192.168.100.20
192.168.100.30
[test]
www.benet.com:222                         <!--通过222端口管理设备-->
[mail]
yj1.kgc.cn
yj[2:5].kgc.cn
<!--[2:5]表示2~5之间的所有数字，即表示yj2.kgc.cn、yj3.kgc.cn……的所有主机-->
```

**可以将一个主机同时归置在不同的组中。**

配置完成之后，可以针对hosts定义的组进行远程操作，也可以针对组中的某一个或多个主机操作。例如：

### 1）只对web组中192.168.1.2主机操作，通过--limit参数限定主机的变更。

```
[root@centos01 ~]# ansible web -m command -a "systemctl status httpd" --limit "192.168.100.20"
192.168.100.20 | SUCCESS | rc=0 >>
<!--看到SUCCESS就知道成功了，所以以下内容-->
<!--如果测试httpd服务，被测试主机必然已经安装并启动了httpd服务-->
```

### 2）只对192.168.100.20主机操作。通过IP限定主机的变更。

```
[root@centos01 ~]# ansible 192.168.100.20 -m command -a "systemctl status httpd"
192.168.100.20 | SUCCESS | rc=0 >>
```

### 3）只对192.168.100.0网段主机操作，这就需要使用到通配符来限定主机的变更了。

```
[root@centos01 ~]# ansible 192.168.1.* -m command -a "systemctl status httpd"
192.168.100.20 | SUCCESS | rc=0 >>
.......  <!--此处省略部分内容-->
192.168.100.30 | SUCCESS | rc=0 >>
.......    <!--此处省略部分内容-->
<!--实验环境，效果一样，这里就不多说了-->
```

## 3、Ansible命令

Ansible的维护命令大多数是以ansible开头，在终端输入ansible后连续按两次Tab键，会补全所有跟ansible相关的命令。

```
[root@centos01 ~]# ansible  <!--连续按Tab键-->
ansible               ansible-console-2     ansible-galaxy        ansible-playbook-2.7  ansible-vault-2
ansible-2             ansible-console-2.7   ansible-galaxy-2      ansible-pull          ansible-vault-2.7
ansible-2.7           ansible-doc           ansible-galaxy-2.7    ansible-pull-2        
ansible-connection    ansible-doc-2         ansible-playbook      ansible-pull-2.7      
ansible-console       ansible-doc-2.7       ansible-playbook-2    ansible-vault 
```

### 1）ansible

ansible是生产环境中使用非常频繁的命令之一，主要在以下场景使用：

> - 非固化需求；
> - 临时一次性操作；
> - 二次开发接口调用；

非固化需求是指临时性的维护，如查看web服务器组磁盘使用情况、复制一个文件到其他机器等。类似这些没有规律的、临时需要做的任务，我们成为非固化需求，临时一次性操作，语法如下：

```
Ansible  <host-pattern> [options]
```

**可用选项如下：**

> - -v（--verbose）：输出详细的执行过程信息，可以得到执行过程所有信息；
> - -i PATH（--inventory=PATH）：指定inventory信息，默认为/etc/ansible/hosts；
> - -f NUM（--forks=NUM）：并发线程数，默认为5个线程；
> - --private-key=PRIVATE_KEY_FILE：指定密钥文件；
> - -m NAME，--module-name=NAME：指定执行使用的模块；
> - -M DIRECTORY（--module-path=DIRECTORY） ：指定模块存放路径，默认为/usr/share/ansible；
> - -a ARGUMENTS（--args=ARGUMENTS）：指定模块参数；
> - -u USERNAME（--user=USERNAME）：指定远程主机以USERNAME运行命令；
> - -l subset（--limit=SUBSET）：限制运行主机；

#### ①检查所有主机是否存活，执行命令如下：

```
[root@centos01 ~]# ansible all -f 5 -m ping                         
<!--调用ping模块，all表示/etc/ansible/hosts文件中的所有主机，不用创建all分组（默认存在）-->
192.168.100.20 | SUCCESS => {               <!--表示执行成功-->
    "changed": false,                        <!--没有对主机做出更改-->
    "ping": "pong"                  <!--表示执行ping命令的返回结果-->
}
192.168.100.30 | SUCCESS => {
    "changed": false, 
    "ping": "pong"
}
```

#### ②列出web组所有的主机列表，执行命令如下：

```
[root@centos01 ~]# ansible web --list      <!-- --list：表示列出主机列表信息-->
  hosts (2):
    192.168.100.20
    192.168.100.30
```

#### ③批量显示web组中的磁盘使用空间，执行命令如下：

```
[root@centos01 ~]# ansible web -m command -a "df -hT"
192.168.100.30 | SUCCESS | rc=0 >>
文件系统            类型      容量  已用  可用 已用% 挂载点
/dev/mapper/cl-root xfs        17G  4.4G   13G   26% /
devtmpfs            devtmpfs  897M     0  897M    0% /dev
tmpfs               tmpfs     912M   84K  912M    1% /dev/shm
tmpfs               tmpfs     912M     0  912M    0% /sys/fs/cgroup
/dev/sda1           xfs      1014M  173M  842M   18% /boot
tmpfs               tmpfs     183M   16K  183M    1% /run/user/42
tmpfs               tmpfs     183M     0  183M    0% /run/user/0

192.168.100.20 | SUCCESS | rc=0 >>
文件系统            类型      容量  已用  可用 已用% 挂载点
/dev/mapper/cl-root xfs        17G  4.3G   13G   26% /
devtmpfs            devtmpfs  897M     0  897M    0% /dev
tmpfs               tmpfs     912M   84K  912M    1% /dev/shm
tmpfs               tmpfs     912M     0  912M    0% /sys/fs/cgroup
/dev/sda1           xfs      1014M  173M  842M   18% /boot
tmpfs               tmpfs     183M   16K  183M    1% /run/user/42
tmpfs               tmpfs     183M     0  183M    0% /run/user/0
/dev/sr0            iso9660   4.1G  4.1G     0  100% /mnt
```

**web关键字需要提前在/etc/ansible/hosts文件中定义组。**

> Ansible的返回结果非常友好，一般会用三种颜色来表示执行结果：
>
> - 红色：表示执行过程出现异常；
> - 橘黄颜色：表示命令执行后目标有状态变化；
> - 绿色：表示执行成功且没有目标机器做修改；

### 2）Ansible-doc

Ansible-doc用来查询ansible模块文档的说明，类似于man命令，针对每个模块都有详细的用法说明及应用案例介绍，语法如下：

```
ansible-doc [options] [module……]
```

**列出支持的模块：**

```
[root@centos01 ~]#ansible-doc -l
```

**查询ping模块的说明信息：**

```
[root@centos01 ~]# ansible-doc ping
> PING    (/usr/lib/python2.7/site-packages/ansible/modules/system/ping.py)

  A trivial test module, this module always returns `pong' on successful contact. It
  does not make sense in playbooks, but it is useful from `/usr/bin/ansible' to verify
  the ability to login and that a usable python is configured. This is NOT ICMP ping,
  this is just a trivial test module.

EXAMPLES:
# Test we can logon to 'webservers' and execute python with json lib.
ansible webservers -m ping

MAINTAINERS: Ansible Core Team, Michael DeHaan

METADATA:
        Status: ['stableinterface']
        Supported_by: core
```

### 3）Ansible-playbook

Ansible-playbook是日常应用中使用频率最高的命令，类似于Linux中的sh或source命令，用来执行系列任务。其工作机制：通过读取预先编写好的playbook文件实现集中处理任务。Ansible-playbook命令后跟yml格式的playbook文件，playbook文件存放了要执行的任务代码，命令使用方式如下：

```
Ansible-playbook playbook.yml
<!--playbook.yml文件要提前编写好，建议使用绝对路径-->
```

### 4）Ansible-console

Ansible-console是Ansible为用户提供的一款交互式工具，类似于Windows的cmd或者是Linux中shell。用户可以在ansible-console虚拟出来的终端上像shell一样使用Ansible内置的各种命令，这为习惯于使用shell交互式方式的用户提供了良好的使用体验。在终端输入ansible-console命令后，显示如下：

```
[root@centos01 ~]# ansible-console
Welcome to the ansible console.
Type help or ? to list commands.
      <!--输入help或？获取帮助-->
root@all (2)[f:5]$ cd web    <!--使用cd命令切换主机或分组-->
root@web (2)[f:5]$ list                  <!--列出当前的设备-->
192.168.100.20
192.168.100.30
<!--支持Tab键补全，快捷键Ctrl+D或Ctrl+C即可退出当前的虚拟终端-->
```

## 4、Ansible模块

### 1）command模块

command模块在远程主机执行命令，不支持管道、重定向等shell的特性。常用的参数如下：

> - chdir：在远程主机上运行命令前要提前进入的目录；
> - creates：在命令运行时创建一个文件，如果文件已存在，则不会执行创建任务；
> - removes：在命令运行时移除一个文件，如果文件不存在，则不会执行移除任务；
> - executeable：指明运行命令的shell程序；

在所有主机上运行“ls ./”命令，运行前切换到/home目录下。操作如下：

```
[root@centos01 ~]# ansible web -m command -a "chdir=/ ls ./"
```

### 2）shell模块

shell模块在远程主机执行命令，相当于调用远程主机的Shell进程，然后在该Shell下打开一个子Shell运行命令。和command模块的区别是它支持Shell特性：如管道、重定向等。

**示例如下：**

```
[root@centos01 ~]# ansible web -m shell -a "echo hello world "        <!--输出到屏幕-->
192.168.100.20 | SUCCESS | rc=0 >>
hello world

192.168.100.30 | SUCCESS | rc=0 >>
hello world
[root@centos01 ~]# ansible web -m shell -a "echo hello world > /1.txt"   <!--输出到1.txt文件中-->
192.168.100.20 | SUCCESS | rc=0 >>

192.168.100.30 | SUCCESS | rc=0 >>
```

### 3）copy模块

copy模块用于复制指定主机文件到远程主机的指定位置。常见的参数如下：

> - dest：指出复制文件的目标目录位置，使用绝对路径。如果源是目录，则目标也要是目录，如果目标文件已存在，会覆盖原有内容；
> - src：指出源文件的路径，可以使用相对路径和绝对路径，支持直接指定目录。如果源是目录，则目标也要是目录；
> - mode：指出复制时，目标文件的权限，可选；
> - owner：指出复制时，目标文件的属主，可选；
> - group：指出复制时目标文件的属组，可选；
> - content：指出复制到目标主机上的内容，不能和src一起使用，相当于复制content指明的数据到目标文件中；

**示例如下：**

```
[root@centos01 ~]# ansible web -m copy -a "src=/etc/hosts 
dest=/root/a1.hosts mode=777 owner=root group=root"
<!--/将本机的hosts文件复制到web组中的所有主机上存放在家目录下的a1.hosts目录，
权限是777，属主是root，属组是root-->
```

### 4）hostname模块

hostname模块用于管理远程主机上的主机名。常用的参数如下：

> - name：指明主机名；

**示例如下：**

```
[root@centos01 ~]# ansible 192.168.100.20 -m hostname -a "name=test"
<!--将192.168.100.20的主机名改为test，
但是192.168.100.20需要敲一下bash才生效-->
```

### 5）yum模块

yum模块基于yum机制，对远程主机管理程序包。常用的参数如下：

> - name：程序包名称，可以带上版本号。若不指明版本，则默认为最新版本；
> - state=present|latest|absent：指明对程序包执行的操作:present表明安装程序包，latest表示安装最新版本的程序包，absent表示卸载程序包；
> - disablerepo：在用yum安装时，临时禁用某个仓库的ID；
> - enablerepo：在用yum安装时，临时启用某个仓库的ID；
> - conf_file：yum运行时的配置文件，而不是使用默认的配置文件；
> - disable_gpg_check=yes|no：是否启用完整性校验功能；

**示例如下：**

```
[root@centos01 ~]# ansible web -m shell -a "/usr/bin/rm -rf 
/etc/yum.repos.d/CentOS-*"   
          <!--批量化删除web组主机的yum源-->
[root@centos01 ~]# ansible web -m shell -a "/usr/bin/mount 
/dev/cdrom /mnt"   <!--批量化挂载光盘-->
 [WARNING]: Consider using mount module rather than running mount

192.168.100.20 | SUCCESS | rc=0 >>
mount: /dev/sr0 写保护，将以只读方式挂载

192.168.100.30 | SUCCESS | rc=0 >>
mount: /dev/sr0 写保护，将以只读方式挂载
[root@centos01 ~]# ansible web -m yum -a "name=httpd 
state=present"  <!--批量化安装httpd程序-->
[root@centos01 ~]# ansible web -m shell -a "rpm -qa | grep httpd"
    <!--批量化查看安装的httpd程序包-->
 [WARNING]: Consider using yum, dnf or zypper module rather than running rpm

192.168.100.20 | SUCCESS | rc=0 >>
httpd-2.4.6-67.el7.centos.x86_64
httpd-tools-2.4.6-67.el7.centos.x86_64

192.168.100.30 | SUCCESS | rc=0 >>
httpd-2.4.6-67.el7.centos.x86_64
httpd-tools-2.4.6-67.el7.centos.x86_64
[root@centos01 ~]# ansible web -m shell -a "systemctl start httpd"       <!--批量启动服务-->
[root@centos01 ~]# ansible web -m shell -a "netstat -anptu | grep httpd"     <!--批量化监听httpd服务是否启动成功-->
192.168.100.20 | SUCCESS | rc=0 >>
tcp6       0      0 :::80                   :::*                    LISTEN      2072/httpd          

192.168.100.30 | SUCCESS | rc=0 >>
tcp6       0      0 :::80                   :::*                    LISTEN      3098/httpd          
```

**管理端只是发送yum指令到被管理端，被管理端要存在可用的yum仓库才可以成功安装。**

### 6）service模块

service模块为用来管理远程主机上的服务的模块。常见的参数如下：

> - name:被管理的服务名称；
> - state=started|stopped|restarted：动作包含启动，关闭或重启；
> - enable=yes|no:表示是否设置该服务开机自启动；
> - runlevel:如果设定了enabled开机自启动，则要定义在哪些运行目标下自动启动；

**示例如下：**

```
[root@centos01 ~]# ansible web -m service -a "name=httpd 
enabled=yes state=restarted"
<!--设置httpd服务重新启动和开机自动启动-->
```

### 7）user模块

user模块主要用于管理远程主机上的用户账号。常见的参数如下：

> - name:必选参数，账号名称；
> - state=present|absent:创建账号或者删除账号，present表示创建，absent表示删除；
> - system=yes|no:是否为系统账户；
> - uid:用户UID；
> - group:用户的基本组；
> - groups:用户的附加组；
> - shell:默认使用的shell；
> - home:用户的家目录；
> - mve_home=yes|no：如果设置的家目录已经存在，是否将已存在的家目录进行移动；
> - pssword:用户的密码，建议使用加密后的字符串；
> - comment：用户的注释信息；
> - remore=yes|no：当state=absent时，是否要删除用户的家目录；

**创建用户示例如下：**

```
[root@centos01 ~]# ansible web -m user -a "name=user01 
system=yes uid=502 group=root groups=root shell=/etc/nologin 
home=/home/user01 password=pwd@123"
<!--在web组的所有主机上新建一个系统用户，UID为502，
属组是root，名字是user01，密码是pwd@123-->
```

# 四、playbook配置文件

## 1、执行配置文件

playbook配置文件使用YAML语法，具有简洁明了、结构清晰等特点。playbook配置文件类似于shell脚本，是一个YAML格式的文件，用于保存针对特定需求的任务列表。上面介绍的ansible命令虽然可以完成各种任务，但是当配置一些复杂任务时，逐条输入就显得效率非常低下。更有效的方案是在playbook配置文件中放置所有的任务代码，利用ansible-playbook命令执行该文件，可以实现自动化运维。YAML文件的扩展名通常为.yaml或.yml。

**YAML语法与其他高级语言类似，其结构通过缩进来展示，通过“-”来代表项；“：”用来分隔键和值；整个文件以“---”开头并以“...”结尾，如下所示：**

```
[root@centos01 ~]# grep -v ^# /etc/ansible/hosts | grep -v ^$              <!--查看hosts中的分组信息-->
[web1]
192.168.100.20
[web2]
192.168.100.30
[root@centos01 ~]# vim /etc/ansible/a.yml  
                   <!--创建a.yml文件，写入以下内容-->
---
- hosts: web1                   <!--针对web1组中的操作-->
  remote_user: root                    <!--远端执行用户身份为root-->
  tasks:                <!--任务列表-->
        - name: adduser                               <!--任务名称-->
          user: name=user1 state=present <!--执行user模块，创建用户-->
          tags:                <!--创建tag标签-->
          - aaa                 <!--tag标签为aaa-->
        - name: addgroup           <!--任务名称-->
          group: name=root system=yes <!--执行group模块，创建组-->
          tags:               <!--创建tag标签-->
          - bbb               <!--tag标签为bbb-->
- hosts: web2               <!--针对web2组中的操作-->
  remote_user: root        <!--远端执行用户身份为root-->
  tasks:                     <!--任务列表-->
        - name: copy file to web            <!--任务名称-->
          copy: src=/etc/passwd dest=/home        <!--执行copy模块，复制文件-->
          tags:                        <!--创建tag标签-->
          - ccc                     <!--tag标签为ccc-->
...
```

**所有的“-”和“：”后面均有空格，而且注意缩进和对齐，如下图所示：**
![简单聊一聊Ansible自动化运维](13-ansible/ab82ae178ee6db696ea3803445f1b28f.png)

**playbook的核心元素包含：**

> - hosts：任务的目标主机，多个主机用冒号分隔，一般调用/etc/ansible/hosts中的分组信息；
> - remote_user：远程主机上，运行此任务的默认身份为root；
> - tasks：任务，即定义的具体任务，由模块定义的操作列表；
> - handlers：触发器，类似tasks，只是在特定的条件下才会触发的任务。某任务的状态在运行后为changed时，可通过“notify”通知给相应的handlers进行触发执行；
> - roles：角色，将hosts剥离出去，由tasks、handlers等所组成的一种特定的结构集合；

playbook文件定义的任务需要通过ansible-playbook命令进行调用并执行。ansible-playbook命令用法如下：

```
ansible-playbook [option] /PATH/TO/PLAYBOOK.yaml
```

**其中，[option]部分的功能包括：**

> - --syntax-check：检测yaml文件的语法；
> - -C（--check）：预测试，不会改变目标主机的任何设置；
> - --list-hosts：列出yaml文件影响的主机列表；
> - --list-tasks：列出yaml文件的任务列表；
> - --list-tags：列出yaml文件中的标签；
> - -t TAGS（--tags=TAGS）：表示只执行指定标签的任务；
> - --skip-tags=SKIP_TAGS：表示除了指定标签的任务，执行其他任务；
>
> --start-at-task=START_AT：从指定的任务开始往下运行；

**执行playbook的示例如下：**

```
[root@centos01 ~]# ansible-playbook --syntax-check /etc/ansible/a.yml    <!--语法检测-->

playbook: /etc/ansible/a.yml     <!--表示没有报错-->
[root@centos01 ~]# ansible-playbook -C /etc/ansible/a.yml
         <!--对a.yml进行预测试-->
    .................<!--省略部分内容-->
192.168.100.20       : ok=3    changed=1    unreachable=0    failed=0   
192.168.100.30       : ok=2    changed=1    unreachable=0    failed=0   
<!--返回结果表示没有错误，全部可以执行成功。-->
[root@centos01 ~]# ansible-playbook --list-hosts /etc/ansible/a.yml   
<!--列出a.yml文件中的主机-->
[root@centos01 ~]# ansible-playbook --list-tasks /etc/ansible/a.yml       
<!--列出任务-->
[root@centos01 ~]# ansible-playbook --list-tags /etc/ansible/a.yml           <!--列出标签-->
[root@centos01 ~]# ansible-playbook /etc/ansible/a.yml                <!--执行任务-->
[root@centos01 ~]# ssh 192.168.100.20 tail -1 /etc/passwd <!--确认执行结果-->
user1:x:1001:1001::/home/user1:/bin/bash
[root@centos01 ~]# ssh 192.168.100.30 ls -ld /home/passwd
-rw-r--r--. 1 root root 2342 7月  23 16:06 /home/passwd
<!--一般情况先执行“-C”命令进行预测试，没有问题后再执行.yml文件。-->
```

**通常情况下先执行ansible-playbook -C /PATH/TO/PLAYBOOK.yaml命令进行测试，测试没问题后再执行ansible-playbook /PATH/TO/PLAYBOOK.yml命令。**

## 2、触发器

需要触发才能执行的任务，当之前定义在tasks中的任务执行成功后，若希望在此基础上触发其他任务，这时就需要定义handlers。例如，当通过ansible的模块对目标主机的配置文件进行修改之后，如果任务执行成功，可以触发一个触发器，在触发器中定义目标主机的服务重启操作，以使配置文件生效。**handlers触发器具有以下特点：**

> - handlers是Ansible提供的条件机制之一。handlers和task很类似，但是它只在被task通知的时候才会触发执行。
> - handlers只会在所有任务执行完成后执行。而且即使被通知了很多次，它也只会执行一次。handlers按照定义的顺序依次执行。

**handlers触发器的使用示例如下：**

```
[root@centos01 ~]# ssh 192.168.100.20 netstat -anpt | grep 80                  <!--查询100.20主机监听的端口-->
tcp6       0      0 :::80         :::*          LISTEN      94858/httpd 
<!--可以看到是监听80端口，现在通过脚本改为8080端口，并使其生效。-->
[root@centos01 ~]# vim /etc/ansible/httpd.yml
            <!--编辑httpd.yml文件，写入以下内容-->

---
- hosts: web1
  remote_user: root
  tasks:
        - name: change port
          command: sed -i 's/Listen\ 80/Listen\ 8080/g' /etc/httpd/conf/httpd.conf
          notify:                             <!--配置触发条件-->
                - restart httpd server    <!--完成该任务后调用名为“restart httpd server”的触发器-->
  handlers:                                      <!--配置触发器-->
        - name: restart httpd server  <!--指定触发器名字，要和上面“notify”指定的触发器名字一样-->
          service: name=httpd state=restarted<!--触发任务为重启httpd服务-->
...
<!--编写完成后，保存退出即可-->
[root@centos01 ~]# ansible-playbook -C /etc/ansible/httpd.yml          <!--进行预测试-->
[root@centos01 ~]# ansible-playbook  /etc/ansible/httpd.yml               <!--执行脚本-->
[root@centos01 ~]# ssh 192.168.100.20 netstat -anpt | grep 8080        <!--远端主机已经运行8080端口-->
tcp6       0      0 :::8080        :::*         LISTEN      103594/httpd
```

## 3、角色

将多种不同的tasks的文件集中存储在某个目录下，则该目录就是角色。角色一般存放在/etc/ansible/roles/目录，可通过ansible的配置文件来调整默认的角色目录，/etc/ansible/roles/目录下有很多子目录，其中每一个子目录对应一个角色，每个角色也有自己的目录结构，如下图所示：
![简单聊一聊Ansible自动化运维](13-ansible/dae0678a271ef459e4b0a222bfd43161.png)

> /etc/ansible/roles/为角色集合，该目录下有自定义的各个子目录：
>
> - mariadb：mysql角色；
> - Apache：httpd角色；
> - Nginx：Nginx角色；
>
> **每个角色的定义，以特定的层级目录结构进行组织。以mariadb（mysql角色）为例：**
>
> - files：存放由copy或script等模块调用的文件；
> - templates：存放template模块查找所需要的模板文件的目录，如mysql配置文件模板；
> - tasks：任务存放的目录；
> - handlers：存放相关触发执行的目录；
> - vars：变量存放的目录；
> - meta：用于存放此角色元数据；
> - default：默认变量存放的目录，文件中定义了此角色使用的默认变量；
>
> 上述目录中，tasks、handlers、vars、meta、default至少应该包含一个main.yml文件，该目录下也可以有其他.yml文件，但是需要在main.yml文件中用include指令将其他.yml文件包含进来。

有了角色后，可以直接在yaml文件（playbook配置文件）中调用角色，示例如下：

```
- hosts: web
  remote_user: root
  roles:            
  - mysql        <!--调用角色名-->
  - httpd             <!--调用角色名-->
```

可以只调用一个角色，也可以调用多个角色，当定义了角色后，用ansible-playbook PALYBOOK文件执行即可。此时ansible会到角色集合的目录（/etc/ansible/roles）去找mysql和httpd目录，然后依次运行mysql和httpd目录下的所有代码。

**下面来个安装及配置mariadb数据库的实例**

**需求分析：**

> - 要求被管理主机上自动安装mariadb，安装完成后上传提前准备好的配置文件至远端主机，重启服务，然后新建testdb数据库，并允许test用户对其拥有所有权限。
> - 被管理主机配置yum仓库，自行配置，若被管理端可以连接互联网，那么直接将yum仓库指向互联网即可。

**开始在ansible服务器上实施：**

```
[root@centos01 /]# mkdir -pv /etc/ansible/roles/mariadb/{files,tasks,handlers}
mkdir: 已创建目录 "/etc/ansible/roles/mariadb"
mkdir: 已创建目录 "/etc/ansible/roles/mariadb/files"
mkdir: 已创建目录 "/etc/ansible/roles/mariadb/tasks"
mkdir: 已创建目录 "/etc/ansible/roles/mariadb/handlers"
[root@ansible /]# cd /etc/ansible/roles/mariadb/tasks/ <!--切换至指定目录-->
[root@centos01 tasks]# ls
[root@centos01 tasks]# vim main.yml  <!--编写main.yml文件-->
---
- name: install mariadb
  yum: name=mariadb-server state=present
- name: move config file
  shell: "[ -e /etc/my.cnf ] && mv /etc/my.cnf /etc/my.cnf.bak"
- name: provide a new config file
  copy: src=my.cnf dest=/etc/my.cnf
- name: reload mariadb
  shell: systemctl restart mariadb
- name: create database testdb
  shell: mysql -u root -e "create database testdb;grant all on testdb.* to 'test'@'192.168.100.%' identified by 'test123';flush privileges;"
  notify:
  - restart mariadb
...
<!--编写完毕，保存退出即可-->
[root@centos01 tasks]# cd ../handlers/ <!--切换至触发器目录-->
[root@centos01 handlers]# vim main.yml <!--编写main.yml文件，写入以下内容-->
---
- name: restart mariadb
  service: name=mariadb state=restarted
...
<!--编写完毕，保存退出即可-->
[root@centos01 handlers]# cd ../files <!--进入mariadb角色文件夹的files-->
[root@centos01 files]# pwd
/etc/ansible/roles/mariadb/files
[root@centos01 files]# ls  <!--准备好配置好的mysql数据库配置文件，需要分发到远程主机的-->
my.cnf
[root@centos01 files]# cd /etc/ansible/
[root@centos01 ansible]# vim mariadb.yml  <!--编写.yml文件-->
---
- hosts: web
  remote_user: root
  roles:
  - mariadb
...
<!--编写完毕，保存退出即可-->
[root@centos01 ansible]# ansible-playbook -C mariadb.yml          <!--进行预检测-->
                                  ........................          <!--省略部分内容-->
PLAY RECAP ***************************************************************************
192.168.100.20                : ok=3    changed=1    unreachable=0    failed=0 
<!--返回结果表示没问题-->
[root@centos01 ansible]# ansible-playbook mariadb.yml   <!--执行安装-->
```

待安装完成后，在远端主机上自行测试。

