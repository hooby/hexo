---
title: docker repository使用方法
layout: page
date: 2017/5/31
updated: 2017/11/13
comments: false
tags: 
- docker
categories: 
- docker
toc: true
---

<!-- toc -->

[TOC]

## 1.查看repository中的image

假设集群上docker 私有仓库做到了master节点，172.16.10.10:5000。

### 获取已有image

私有仓库目前已有images如下：

```
$curl http://172.16.10.10:5000/v2/_catalog|python2 -m json.tool
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   289  100   289    0     0   1125      0 --:--:-- --:--:-- --:--:--  1128
{
    "repositories": [
        "centos",
        "gitlab/gitlab-ce",
        "gitlab/gitlab-runner",
        "gitlab-runner",
        "keras",
        "mongo",
        "nvidia/cuda",
        "nvidia/digits",
        "paddledev/paddle",
        "paddlepaddle/book",
        "paddlepaddle/paddle",
        "redis",
        "registry",
        "superman/registry",
        "ubuntu",
        "yhu/centos",
        "yhu/gitlab-runner",
        "yhu/registry"
    ]
}
```

### 获取制定image的所有版本号

如果想要查看对应的images拥有哪些版本， 例如paddlepaddle/paddle  这个镜像的版本

```
$curl http://172.16.10.10:5000/v2/paddlepaddle/paddle/tags/list|python2 -m json.tool
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    84  100    84    0     0  20771      0 --:--:-- --:--:-- --:--:-- 28000
{
    "name": "paddlepaddle/paddle",
    "tags": [
        "latest",
        "0.10.0",
        "0.10.0-dev",
        "0.10.0-gpu"
    ]
}

```

只需要修改中间对应的名字即可

## 2.上传和下载image

下面简单介绍一下如何从私有仓库下载和上传镜像。

找到对应的镜像和版本之后，执行 

```
$docker pull 172.16.10.10:5000/paddlepaddle/paddle:0.10.0  #下载images
...
$docker images 
REPOSITORY                              TAG                 IMAGE ID            CREATED             SIZE
python                                  2.7                 26bddf7dbe1b        6 days ago          679MB
$docker tag python:2.7 172.16.10.10:5000/python:2.7
$docker images 
REPOSITORY                              TAG                 IMAGE ID            CREATED             SIZE
172.16.10.10:5000/python                2.7                 26bddf7dbe1b        6 days ago          679MB
python                                  2.7                 26bddf7dbe1b        6 days ago          679MB
$docker push 172.16.10.10:5000/python:2.7   #上传images
The push refers to a repository [172.16.10.10:5000/python]
c89798427b75: Pushed 
...
18f9b4e2e1bc: Pushed 
2.7: digest: sha256:d3ecae8689444b025ff6dfd0972d98e4c0f9e8b9ca5762f044447dfd654c1b53 size: 2011
[superman@node7 ~]$curl http://172.16.10.10:5000/v2/_catalog|python2 -m json.tool
{
    "repositories": [
...
        "python",
...
    ]
}
```