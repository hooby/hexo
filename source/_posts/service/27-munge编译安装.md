---
title: munge编译安装
layout: page
date: 2017/7/8
updated: 2017/7/13
comments: false
tags: 
- munge
categories: 
- munge
toc: true
---

<!-- toc -->

[TOC]

## master 节点
### 创建文档
```
sudo mkdir -p /atlas/configs/munge_v0.5.12/munge
sudo mkdir -p /atlas/gensoft/munge_v0.5.12/lib/munge
sudo mkdir -p /atlas/gensoft/munge_v0.5.12/sbin/nologin
```


### 创建 munge ID 
```
export MUNGEUSER=2001
sudo groupadd -g $MUNGEUSER munge
sudo useradd  -m -c "MUNGE Uid 'N' Gid Emporium" -d /atlas/gensoft/munge_v0.5.12/lib/munge -u $MUNGEUSER -g munge  -s /atlas/gensoft/munge_v0.5.12/sbin/nologin munge
```


### munge下载
```
到 https://dun.github.io/munge/ 下载 tar.gz 最新版本
文件放置到atlas/backup/munge_v0.5.12 中
解压
sudo tar zxvf dun-munge-munge-0.5.12-24-gd11b5fe.tar.gz
cd dun-munge-d11b5fe
```


### 安装
```
sudo ./configure --prefix=/atlas/gensoft/munge_v0.5.12 --sysconfdir=/atlas/configs/munge_v0.5.12 --localstatedir=/atlas/gensoft/munge_v0.5.12 
sudo make -j 4
sudo make install 
```


### 配置环境变量
```
vim /etc/profile.d/munge.sh
export PATH=/atlas/gensoft/munge_v0.5.12/usr/bin:$PATH
export PATH=/atlas/gensoft/munge_v0.5.12/usr/sbin:$PATH
. /etc/profile.d/munge.sh
```


### 检查加密方式
```
munge -C
munge -M
```


### 在mater 节点创建所有 node 需要的秘钥 
```
dd if=/dev/urandom bs=1 count=1024 > /atlas/configs/munge_v0.5.12/munge/munge.key
chown munge: /atlas/configs/munge_v0.5.12/munge/munge.key
chmod 400 /atlas/configs/munge_v0.5.12/munge/munge.key
```


### 将/atlas/configs/munge_v0.5.12 /munge/munge.key 拷贝到其他节点
```
login
scp /atlas/configs/munge_v0.5.12/munge/munge.key root@172.16.10.18:/atlas/configs/munge_v0.5.12/munge
node5
scp /atlas/configs/munge_v0.5.12/munge/munge.key root@172.16.10.15:/atlas/configs/munge_v0.5.12/munge
```
---

---

## 在所有节点上设置权限和所属
```
chown -R munge: /atlas/configs/munge_v0.5.12/munge/ /atlas/gensoft/munge_v0.5.12/log/munge/
chmod 0700 /atlas/configs/munge_v0.5.12/munge/ /atlas/gensoft/munge_v0.5.12/log/munge/
```


## 在所有节点上运行 munge
```
 /atlas/configs/munge_v0.5.12/rc.d/init.d/munge start
```
