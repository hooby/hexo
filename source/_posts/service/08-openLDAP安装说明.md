---
title: openLDAP安装说明
layout: page
date: 2016/1/5
updated: 2016/6/13
comments: false
tags: 
- openLDAP
categories: 
- service
toc: true
---

<!-- toc -->

[TOC]

# 安装 openLDAP 准备

- 完成各个相互通信的节点的域名解析（DNS）
- 完成 NFS 将master节点 home目录挂载到其他节点同一路径下  
- 服务器IP、域名、主机名：172.16.10.10 maste.example.com master
- 关闭防火墙

# 搭建 LDAP 服务器
- yum 下载安装
```
$ yum install openldap* migrationtools -y
```
- 设置 LDAP 管理密码
```
$ slappasswd
New password:
Re-enter new password:
{SSHA}b28RwTpbqZ5/kxro785tKExdK4uyOX7T  ///此密码自行妥善保存
```
- 进入配置目录
```
$ cd /etc/openldap/slapd.d/cn=config
```
- 编辑文件
```
$ vim olcDatabase={2}hdb.ldif

```
```
替换dc=my-domain,dc=com
olcSuffix: dc=sg-ai,dc=com
olcRootDN: cn=Manager,dc=sg-ai,dc=com
在文档底部加上：
olcRootPW: {SSHA}b28RwTpbqZ5/kxro785tKExdK4uyOX7T
olcTLSCertificateFile: /etc/pki/tls/certs/sg-ai.pem
olcTLSCertificateKeyFile: /etc/pki/tls/certs/sg-aikey.pem 
```

```
$ vim olcDatabase={1}monitor.ldif
```
>同样替换dc=my-domain,dc=com
>olcAccess: {0}to * by dn.base="gidNumber=0+uidNumber=0,cn=peercred,cn=external, cn=auth" read by dn.base="cn=Manager,dc=linuxhelp,dc=com" read by * none

- 测试配置文件是否成功
```
$ slaptest -u
```
 显示如下为成功

```
 5737a49a ldif_read_file: checksum error on "/etc/openldap/slapd.d/cn=config/olcDatabase={1}monitor.ldif"
 5737a49a ldif_read_file: checksum error on "/etc/openldap/slapd.d/cn=config/olcDatabase={2}hdb.ldif"
config file testing succeeded
```



- 启动 LDAP 服务器
```
$ systemctl start slapd
```
- 设置 LDAP 为开机启动
```
$ systemctl enable slapd
```
- 配置 LDAP 数据库 
```
cp /usr/share/openldap-servers/DB_CONFIG.example /var/lib/ldap/DB_CONFIG
chown -R ldap:ldap /var/lib/ldap/
```
- 加入 LDAP 方案
```
$ ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/cosine.ldif
SASL/EXTERNAL authentication started
$ ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/nis.ldif
$ ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/inetorgperson.ldif
```
- 生成证书文件
```
openssl req -new -x509 -nodes -out /etc/pki/tls/certs/sg-ai.pem -keyout /etc/pki/tls/certs/sg-aikey.pem -days 365
```
按照提示填写：

```
Generating a 2048 bit RSA private key
...........................................+++
.............................................................................................................+++
writing new private key to '/etc/pki/tls/certs/linuxhelpkey.pem'
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
Country Name (2 letter code) [XX]:
State or Province Name (full name) []:
Locality Name (eg, city) [Default City]:
Organization Name (eg, company) [Default Company Ltd]:sgkj
Organizational Unit Name (eg, section) []:sg
Common Name (eg, your name or your server's hostname) []:master.sg-ai.com
Email Address []: 

```


- 确认生成证书运行的命令
```
ls -l /etc/pki/tls/certs/*.pem
```
- 进入 /usr/share/migrationtools/ 目录修改 “migratinos_common.ph” 文件
```
$ cd /usr/share/migrationtools/
$ vim migrate_common.ph
```
> 修改第 71 行和 74 行为
> $DEFAULT_MAIL_DOMAIN = "example.com";
> $DEFAULT_BASE = "dc=example,dc=com";
> 修改第 90 行为：
> $EXTENDED_SCHEMA = 1;

- 创建 base.ldif 文件
```
vim /root/base.ldif
```

写入

```
dn: dc=sg-ai,dc=com
objectClass: top
objectClass: dcObject
objectclass: organization
o: sg-ai com
dc: sg-ai

dn: cn=Manager,dc=sg-ai,dc=com
objectClass: organizationalRole
cn: Manager
description: Directory Manager

dn: ou=People,dc=sg-ai,dc=com
objectClass: organizationalUnit
ou: People

dn: ou=Group,dc=sg-ai,dc=com
objectClass: organizationalUnit
ou: Group
```
- 创建用户
```
$ useradd test003
$ useradd test004
$ passwd test003
$ passwd test003
```
- 将新建用户的信息填写到自建的.ldif文件中
```
$ tail /etc/passwd >>  /root/passwd
$ vim /root/passwd
test003:x:1001:1001::/home/test003:/bin/bash
test004:x:1002:1002::/home/test004:/bin/bash

$ tail /etc/group >>/root/group
$ vim /root/group
test003:x:1001:
test004:x:1002:
```
- 将这些文件转换为 ldif 格式
```
$ ./migrate_passwd.pl /root/passwd /root/users.ldif
$ ./migrate_group.pl /root/group /root/groups.ldif
```
- 将这些文件导入 LDAP 数据库中
```
$ ldapadd -x -W -D "cn=Manager,dc=sg-ai,dc=com" -f /root/base.ldif
$ ldapadd -x -W -D "cn=Manager,dc=sg-ai,dc=com" -f /root/users.ldif
$ ldapadd -x -W -D "cn=Manager,dc=sg-ai,dc=com" -f /root/groups.ldif
```
- 检查导入数据库后的情况
```
$ ldapsearch -x cn=test003 -b dc=example,dc=com
$ ldapsearch -x -b 'dc=sg-ai,dc=com' '(objectclass=*)'
```

# 搭建 LDAP 客户机
- 安装
```
yum install openldap-clients nss-pam-ldapd authconfig -y
```
- 设置  LDAP 认证
```
$ authconfig-tui
```

```
User Information ???????Authentication
[ ] Cache Information ??[ ] Use MD5 Passwords
[*] Use LDAP ????????????[*] Use Shadow Passwords
[ ] Use NIS ???????????????[*] Use LDAP Authentication
[ ] Use IPAv2 ???????????[ ] Use Kerberos
[ ] Use Winbind ????????[ ] Use Fingerprint reader
[ ] Use Winbind Authentication
[*] Local authorization is sufficient

 
- 选中Use LDAP  和 Use LDAP Authentication即可
用Tab键切换到Next，回车。


[*] Use TLS
Server: ldap://master
Base DN: dc=sg-ai,dc=com

```
- Use TLS可以不用选中
- 测试运行正确与否
```
getent passwd test003
```
>正确显示为：
>`test003:x:1001:1001::/home/test003:/bin/bash`