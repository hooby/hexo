---
title: Gitlab CI
layout: page
date: 2016/7/22
updated: 2016/9/12
comments: false
tags: 
- gitlab
categories: 
- gitlab
toc: true
---

<!-- toc -->

[TOC]

# GitLab CI CD

**持续集成(Continuous Integration)** , 合并存储库中的更改之前触发管道来构建，测试和验证新代码。就是先测试和验证代码，然后再合并。

**持续交付(Continuous Delivery）** 将CI验证的代码**交付**给您的应用程序。就是将合并的代码更新部署到线上。

## CI/CD的优点：

### 持续集成

- **尽快发现错误：** 在开发人员重新思考的同时解决问题
- **减少集成问题：** 更小的问题更容易消化
- **避免复杂的问题：** 使团队更加自信地发展更快

### 持续交付

- **确保每个更改都是可发布的：** 在完成之前测试所有内容，包括部署
- **降低每次发布的风险：** 使发布简单
- **更加频繁地交付价值：** 可靠的部署意味着更多的发布
- **紧密的客户反馈循环：** 快速而频繁的客户对变更的反馈

参考：https://about.gitlab.com/product/continuous-integration/

## GitLab Runner

**GitLab Runner** 是一个处理构建的应用程序。它可以单独部署，并通过API与GitLab CI / CD一起使用。为了让你提交的代码自动执行构建，测试和部署等功能，你需要有一个可以执行构建测试和部署的服务，Gitlab Runner就是这么一个服务。

### install

1. 添加官方的安装源

```
# For Debian/Ubuntu/Mint
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash

# For RHEL/CentOS/Fedora
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash
```

2. 安装gitlab-runner

```
# For Debian/Ubuntu/Mint
sudo apt-get install gitlab-runner

# For RHEL/CentOS/Fedora
sudo yum install gitlab-runner
```

3. 修改配置，链接到Gitlab

```
Run the following command:
sudo gitlab-runner register

Enter your GitLab instance URL:

Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com )
https://gitlab.com

Enter the token you obtained to register the Runner:

Please enter the gitlab-ci token for this runner
xxx

Enter a description for the Runner, you can change this later in GitLab’s UI:

Please enter the gitlab-ci description for this runner
[hostname] my-runner

Enter the tags associated with the Runner, you can change this later in GitLab’s UI:

Please enter the gitlab-ci tags for this runner (comma separated):
my-tag,another-tag

Enter the Runner executor:

Please enter the executor: ssh, docker+machine, docker-ssh+machine, kubernetes, docker, parallels, virtualbox, docker-ssh, shell:
docker

If you chose Docker as your executor, you’ll be asked for the default image to be used for projects that do not define one in .gitlab-ci.yml:

Please enter the Docker image (eg. ruby:2.1):
alpine:latest
```



参考：

- https://docs.gitlab.com/runner/install/linux-repository.html
- https://docs.gitlab.com/runner/register/index.html

## .gitlab-ci.yml

当gitlab-runner注册到了gitlab中，然后在代码库中加入`.gitlab-ci.yml` 文件，代码库中的代码就会根据`.gitlab-ci.yml` 文件的内容规则来执行构建，测试和部署。

`.gitlab-ci.yml`遵循yaml语法格式，配置规则参考[这里](https://docs.gitlab.com/ee/ci/yaml/)。