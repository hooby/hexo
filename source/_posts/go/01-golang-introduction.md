---
title: Go语言简洁
layout: page
date: 2019-04-01
updated: 2021-04-01
slug:
tags: 
- Go
categories: 
- Go
toc: true
comment: true
---



<!-- toc -->

[TOC]



# Go语言简介

Go是一门编译型和静态型的编程语言。



## 1、Go语言卖点

1、做为一门静态语言，Go却和很多动态脚本语言一样得灵活

2、节省内存、程序启动快和代码执行速度快

3、内置并发编程支持

4、良好的代码可读性，Go的语法很简洁并且和其它流行语言相似。

5、良好的跨平台支持

6、一个稳定的Go核心设计和开发团队以及一个活跃的社区

7、Go拥有一个比较齐全的标准库



和C家族语言相比有以下优点：

- 程序编译时间短
- 像动态语言一样灵活
- 内置并发支持



## 2、Go语言特性

- 内置并发编程支持：
  - 使用协程（goroutine）做为基本的计算单元。轻松地创建协程。
  - 使用通道（channel）来实现协程间的同步和通信。
- 内置了映射（map）和切片（slice）类型。
- 支持多态（polymorphism）。
- 使用接口（interface）来实现裝盒（value boxing）和反射（reflection）。
- 支持指针。
- 支持函数闭包（closure）。
- 支持方法。
- 支持延迟函数调用（defer）。
- 支持类型内嵌（type embedding）。
- 支持类型推断（type deduction or type inference）。
- 内存安全。
- 自动垃圾回收。
- 良好的代码跨平台性。



## 3、开源Go项目

图片来源于极客时间



![GitHub_go_projects](01-golang-introduction/GitHub_go_projects.webp)

