---
title: Go工具链
layout: page
date: 2019-04-02
updated: 2021-04-01
slug:
tags: 
- Go
categories: 
- Go
toc: true
comment: true
---



<!-- toc -->

[TOC]



# Go

为了从任意目录运行Go，安装目录下的`bin`子目录路径必须配置在`PATH`环境变量中。



## 1、Go环境变量

- GOPATH: 此环境变量的默认值为当前用户的HOME目录下的名为`go`文件夹对应的目录路径。 GOPATH文件夹中的pkg子文件夹用来缓存被本地项目所依赖的Go模块（一个Go模块为若干Go库包的集合）的版本。src子文件夹用来存放源码。
- GOBIN: `GOBIN`环境变量用来指定`go install`子命令产生的Go应用程序二进制可执行文件应该存储在何处。 它的默认值为`GOPATH`文件夹中的`bin`子目录所对应的目录路径。



## 2、Go子命令



### go run

编译和运行main包中的go程序。比如有一个 example.go 的文件，只需要执行 `go run example.go` 即可。

如果程序的main包中有多个go源码文件，我们可以指定目录。例如：

```
$ ls
a.go b.go
$ go run ./
```



### go install

编译和安装main包中的go程序，并不会执行，而是将可执行文件放入GOBIN指定的目录中。

我们可以运行`go install example.com/program@latest`来安装一个第三方Go程序的最新版本（至`GOBIIN`目录）。

 在1.16版本之前，可以是用`go get -u example.com/program` 来安装。



### go build

编译main包中的go程序，并不会安装，也不会执行。



### go vet

`go vet`子命令可以用来检查可能的代码逻辑错误（即警告）。



### go fmt

`go fmt`子命令来用同一种代码风格格式化Go代码。



### go test

`go test`子命令来运行单元和基准测试用例。



### go doc

`go doc`子命令用来（在终端中）查看Go代码库包的文档。



### go mod init

`go mod init `命令可以用来在当前目录中生成一个`go.mod`文件。此`go.mod`文件将被用来记录当前项目需要的依赖模块和版本信息。



### go mod tidy

`go mod tidy`命令用来通过扫描当前项目中的所有代码来添加未被记录的依赖至`go.mod`文件或从`go.mod`文件中删除不再被使用的依赖。



### go mod vendor

将依赖包都保存到当前项目的的vendor中。这样做有一个好处，保持不同go项目对依赖库版本的准确。比如出现A项目依赖mod中C的1.1版本，而B项目依赖mod中C的2.1版本，使用vendor即可很好的解决该问题。这种方式很类似Python中的venv或者conda中的不同env



### go get

安装第三方go程序包

在1.16之后使用 `go install ` 



### go help aSubCommand

查看帮助



