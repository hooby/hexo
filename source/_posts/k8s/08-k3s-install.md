---
title: k3s 安装
layout: page
date: 2021-04-01
updated:
slug:
tags: 
- Kubernetes
categories: 
- Kubernetes
toc: true
comment: true
---



<!-- toc -->

[TOC]



# k3s 简介

K3s 是一个轻量级的 Kubernetes 发行版，它针对边缘计算、物联网等场景进行了高度优化。由于运行 K3s 所需的资源相对较少，所以 K3s 也适用于开发和测试场景。

部署起来非常简单，所有服务打包为单个二进制文件，同时启动程序自动处理了众多TLS认证的内容，存储也是用轻量级数据库或者内置etcd等。





# k3s安装



## 第一个master

```bash
# first master

curl -sfL http://rancher-mirror.cnrancher.com/k3s/k3s-install.sh | \
K3S_TOKEN='262c73d1ac2e7a8179c861a8a47640f8' \
INSTALL_K3S_EXEC="server --cluster-init" \
INSTALL_K3S_MIRROR=cn \
INSTALL_K3S_VERSION=v1.20.4+k3s1  sh  -s -

# check
systemctl status k3s.service
kubectl get nodes
```



## 第二个master

只需要在第一个master的基础上，指定第一个master的IP `K3S_URL="https://${first_master_ip}:6443"` ，同时去掉初始化选项 `--cluster-init`  即可。如下：

```bash
# other master
first_master_ip=172.26.186.176

curl -sfL http://rancher-mirror.cnrancher.com/k3s/k3s-install.sh | \
K3S_TOKEN='262c73d1ac2e7a8179c861a8a47640f8' \
K3S_URL="https://${first_master_ip}:6443" \
INSTALL_K3S_EXEC="server" \
INSTALL_K3S_MIRROR=cn \
INSTALL_K3S_VERSION=v1.20.4+k3s1  sh  -s -

# check
systemctl status k3s.service
kubectl get nodes
```



## node节点

计算节点再去掉 `INSTALL_K3S_EXEC` 参数，直接去加入集群即可

```
# slave
first_master_ip=172.26.186.176

curl -sfL http://rancher-mirror.cnrancher.com/k3s/k3s-install.sh | \
K3S_TOKEN='262c73d1ac2e7a8179c861a8a47640f8' \
K3S_URL="https://${first_master_ip}:6443" \
INSTALL_K3S_MIRROR=cn \
INSTALL_K3S_VERSION=v1.20.4+k3s1  sh  -s -

# check
systemctl status k3s-agent.service
kubectl get nodes
```



注意：

master节点的systemd service 为：`k3s.service`， 而node节点的systemd service为：`k3s-agent.service`

 





