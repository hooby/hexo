---
title: Kubernetes设计架构
layout: page
date: 2017/11/27
updated: 2018/8/1
comments: false
tags: 
- Kubernetes
categories: 
- Kubernetes
toc: true
---

<!-- toc -->

[TOC]





# Kubernetes设计架构

Kubernetes集群包含有节点代理kubelet和Master组件(APIs, scheduler, etc)，一切都基于分布式的存储系统。下面这张图是Kubernetes的架构图。

![k8s-architecture](01-k8s-architecture/k8s-architecture.png)



Kubernetes主要由以下几个核心组件组成：

- etcd保存了整个集群的状态；
- apiserver提供了资源操作的唯一入口，并提供认证、授权、访问控制、API注册和发现等机制；
- controller manager负责维护集群的状态，比如故障检测、自动扩展、滚动更新等；
- scheduler负责资源的调度，按照预定的调度策略将Pod调度到相应的机器上；
- kubelet负责维护容器的生命周期，同时也负责Volume（CVI）和网络（CNI）的管理；
- Container runtime负责镜像管理以及Pod和容器的真正运行（CRI）；
- kube-proxy负责为Service提供cluster内部的服务发现和负载均衡；

除了核心组件，还有一些推荐的Add-ons：

- kube-dns负责为整个集群提供DNS服务
- Ingress Controller为服务提供外网入口
- Heapster提供资源监控
- Dashboard提供GUI
- Federation提供跨可用区的集群
- Fluentd-elasticsearch提供集群日志采集、存储与查询





## Kubernetes节点





一个培训机构的kubernetes学习课程，用来参考做学习：

```
Chapter 1：课程介绍
    理解 Kubernetes 设计原则、原理
    了解 Kubernetes 的过去、现在和未来
    了解并学会使用 Kubernetes 最重要的资源 -- API
    学会如何创建和管理应用，并配置应用外部访问
    理解 Kubernetes 网络、存储
    掌握 Kubernetes 调度的原理和策略
    Kubernetes 一些新功能的概念
    了解 Kubernetes 的日志、监控方案
    具备基本的故障排查的运维能力

Chapter 2：Kubernetes 基本概念
    了解什么是 Kubernetes
    了解 Kubernetes 的主要特性
    理解为什么需要 Kubernetes
    了解 Kubernetes 的过去、现在和未来
    了解目前 Kubernetes 社区的情况和被采用情况
    了解 Kubernetes 的基本架构
    获得一些学习资料推荐

Chapter 3：Kubernetes 架构及原理
    理解 Kubernetes 设计原则
    深入理解 Kubernetes 集群中的组件及功能
    了解 Kubernetes 集群对网络的预置要求
    深入理解 Kubernetes 的工作原理
    深入理解 Kubernetes 中 Pod 的设计思想


Chapter 4：Kubernetes 安装和配置
    了解部署 Kubernetes 的多种方式
    可以单机部署 Kubernetes（学习演示使用）
    可以在宿主机部署一套 Kubernetes 集群（非生产使用）


Chapter 5：Kubernetes API 及集群访问
    了解 Kubernetes 的 API
    理解 Kubernetes 中 API 资源的结构定义
    了解 kubectl 工具的使用
    了解 Kubernetes 中 API 之外的其他资源


Chapter 6：ReplicaController，ReplicaSets 和 Deployments
    理解 RC
    理解 label 和 selector 的作用
    理解 RS
    理解 Deployments 并且可操作 Deployments
    理解 rolling update 和 rollback

Chapter 7：Volume、配置文件及密钥
    了解 Kubernetes 存储的管理，支持存储类型
    理解 Pod 使用 volume 的多种工作流程以及演化
    理解 pv 和 pvc 的原理
    理解 storage class 的原理
    理解 configmaps 的作用和使用方法
    理解 secrets 的作用和使用方法资源结构

Chapter 8：Service 及服务发现
    了解 Docker 网络和 Kubernetes 网络
    了解 Flannel 和 Calico 网络方案
    理解 Pod 在 Kubernetes 网络中的工作原理
    理解 Kubernetes 中的 Service
    理解 Service 在 Kubernetes 网络中的工作原理
    理解 Kubernetes 中的服务发现
    掌握 Kubernetes 中外部访问的几种方式

Chapter 9：Ingress 及负载均衡
    理解 Ingress 和 Ingress controller 的工作原理
    掌握如何创建 Ingress 规则
    掌握如何部署 Ingress controller
    
Chapter 10：DaemonSets，StatefulSets，Jobs，HPA，RBAC
    了解 DaemonSet 资源和功能
    了解 StatefulSet 资源和功能
    了解 Jobs 资源和功能
    了解 HPA 资源和功能
    了解 RBAC 资源和功能

Chapter 11：Kubernetes 调度
    理解 Pod 调度的相关概念
    深度理解 Kubernetes 调度策略和算法
    深度理解调度时的 Node 亲和性
    深度理解调度时的 Pod 亲和性和反亲和性
    深度理解污点和容忍对调度的影响
    深度理解强制调度 Pod 的方法

Chapter 12：日志、监控、Troubleshooting
    理解 Kubernetes 集群的日志方案
    理解 Kubernetes 集群的监控方案
    了解相关开源项目：Heapster，Fluentd，Prometheus 等
    掌握常用的集群，Pod，Service 等故障排查和运维手段

Chapter 13：自定义资源 CRD
    理解和掌握 Kubernetes 中如何自定义 API 资源
    可以通过 kubectl 管理 API 资源
    了解用于自定义资源的 Controller 及相关使用示例
    了解 TPR 和 CRD

Chapter 14：Kubernetes Federation
    了解 Kubernetes 中 Federation 的作用和原理
    了解 Federation 的创建过程
    了解 Federation 支持的 API 资源
    了解集群间平衡 Pod 副本的方法

Chapter 15：应用编排 Helm，Chart
    了解 Kubernetes 中如何进行应用编排
    了解 Helm 的作用和工作原理
    了解 Tiller 的作用和工作原理
    了解 Charts 的作用和工作原理

Chapter 16：Kubernetes 安全
    了解 Kubernetes 中 API 访问过程
    了解 Kubernetes 中的 Authentication
    了解 Kubernetes 中的 Authorization
    了解 ABAC 和 RBAC 两种授权方式
    了解 Kubernetes 中的 Admission
    了解 Pod 和容器的操作权限安全策略
    了解 Network Policy 的作用和资源配置方法

```

