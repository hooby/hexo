---
title: Linux安全策略之tcp_wrapper
layout: page
date: 2015/11/1
updated: 2016/6/13
comments: false
tags: 
- Linux
categories: 
- Linux
---

<!-- toc -->

[TOC]

```
tcp_wrapper：

    库文件：libwrap.so，tcp包装器；
    
    判断一个服务程序是否能够由tcp_wrapper进行访问控制的方法：
        (1) 动态链接至libwrap.so库；
            ldd  /PATH/TO/PROGRAM
                libwrap.so
        (2) 静态编译libwrap.so库文件至程序中：
            strings /PATH/TO/PGRGRAM 
                hosts_access
    
    配置文件：/etc/hosts.allow, /etc/hosts.deny
        
         See 'man 5 hosts_options' and 'man 5 hosts_access' for information on rule syntax. 
    
        配置文件语法：
            daemon_list : client_list[ : option : option ...]
            
            daemon_list：程序文件名称列表
                (1) 单个应用程序文件名；
                (2) 程序文件名列表，以逗号分隔；
                (3) ALL：所有受tcp_wrapper控制的应用程序文件；
                
            client_list：
                (1) 单个IP地址或主机名；
                (2) 网络地址：n.n.n.n/m.m.m.m，n.n.n.；
                (3) 内建的ACL:
                    ALL：所有客户端主机；
                    LOCAL：Matches any host whose name does not contain a dot character.
                    UNKNOWN
                    KNOWN
                    PARANOID
                    
                OPERATORS：
                    EXCEPT
                        list1 EXCEPT list2 EXCEPT list3
                        
                        sshd: 172.16. EXCEPT 172.16.100. EXCEPT 172.16.100.68
        
            [ : option : option ...]
            
                deny：拒绝，主要用于hosts.allow文件中定义“拒绝”规则；
                allow：允许，主要用于hosts.deny文件中定义”允许“规则；
                
                spawn：生成，发起，触发执行用户指定的任意命令，此处通常用于记录日志；
                
                    vsftpd: 172.16. : spawn /bin/echo $(date) login attempt from %c to %s >> /var/log/tcp_wrapper.log 
                
    练习：仅开放本机的sshd服务给172.16.0.0/16网络中除了172.16.0.0/24网络中的主机之外的所有主机，但允许172.16.0.200访问； 每次的用户访问都要记录于日志文件中；
```

