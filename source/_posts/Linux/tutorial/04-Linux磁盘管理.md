---
title: Linux磁盘管理
layout: page
date: 2015/7/20
updated: 2016/6/13
comments: false
tags: 
- Linux
categories: 
- Linux
---

<!-- toc -->

[TOC]



```
在系统中，常见的硬盘接口有分两类：并行的和串行的
    并行：
        IDE:    133MB/s
        SCSI:   640MB/s
    串行：
        SATA:   6Gbps
        SAS:    6Gbps
        USB:    480MB/s
存储设备的设备文件命名方式大致为：/dev/DEV_FILE
    IDE:    /dev/hd#  #-->0,1,2,3
    SCSI,SATA,SAS,USB:  /dev/sdx  x表示a-z    eg：/dev/sda,/dev/sdb
    光盘中显示的是 /dev/sr0,/dev/sr1...
同一设备上的不同分区：1,2,3... eg：/dev/sda1,/dev/sda2...
```

想要使用一块没有用过的磁盘，需要做一下几部：

    1、设备识别，即首先要讲磁盘接入设备中，在系统中能够看到设备（/dev/DEV_FILE）
    2、设备分区，全新的磁盘是没有分区的
    3、创建文件系统，主要明确在设备上存储的数据是什么格式的.(ext3,ext4,NTFS...)
    4、标记文件系统
    5、在/etc/fstab文件中创建条目
    6、挂在新的文件系统  


磁盘分区：

```
磁盘分区的理由:
  1、优化i/o性能
  2、实现磁盘空间配额限制
  3、提高修复速度
  4、隔离系统和程序
  5、安装多个OS
  6、采用不同文件系统
两种分区方式：MBR,GPT
MBR: master boot record ，使用32位表示山区数，分区不超过2T，按柱面分区
在0磁道0扇区的512bytes中，前446bytes：BootLoader，64bytes：分区表（其中16byte表示一个分区，因此主分区+扩展分区<=4）最后2byte：55AA
GPT:GUID(global unique identifiers) partition table 支持128个分区，使用64位，支持8Z（512byte/block） 64Z（4096byte/block）
使用128位UUID（universally unique identifier ）表示磁盘和分区，GPT分区表自动备份在头和尾两份，并有CRC校验位
```



管理分区：

```
列出块设备：lsblk
创建分区：fdisk（MBR），gdisk（GPT），parted 高级分区操作（创建，复制，调整大小等）
partprobe 重新设置内存中的内核分区表版本
fdisk gdisk命令类似，下面主要介绍fdisk的使用
fdisk [options] <disk>      change partition table
fdisk [options] -l <disk>   list partition table(s)
交互式fdisk命令其实更适合使用，分区之后可查看，可修改，只有在保存的时候才会真正的自该磁盘的分区。
子命令：
    p 分区列表
    t 更改分区类型
    n 创建分区 
    d 删除分区
    w 保存并退出
    q 不保存退出
    m 查看帮助，打印菜单选择项
在修改分区完成之后，可查看内核是否已经识别新的分区   cat /proc/partations
如果内核没有识别到新的分区，可通过命令是内核重新读取硬盘分区表
在 centos 6 上 partx -a -n M:N /dev/DEVICE  或者 kpartx -a /dev/DEVICE 
删除分区：partx -d -n M:N /dev/DEVICE
在centos 5，7上面直接使用partprobe 就可以了  partprobe [/dev/DEVICE]
```

文件系统:

    查看当前系统支持的文件系统 cat /proc/filesystems
    创建文件系统
        mkfs.fs_type /dev/DEVICE
            fs_type: ext# xfs btrfs vfat...
        mkfs -t fs_type /dev/DEVICE
    创建ext文件系统
    mke2fs:ext系列文件系统专用管理工具
        -t ext#
        -b {1024|2048|4096}
        -L 'lable'
        -j: 加日志记录 相当于-t ext3
        -i #：为数据空间中每多少个字节创建一个inode;此大小不应该小于block的大小
        -N #：为数据空间创建多少个inode
        -I 一个inode记录大小 128--4096
        -m #：默认5% 位管理人员预留空间占总空间的百分比
    文件系统标签：
        blkid [options]...[device]块设备属性信息查看
        -U UUID:根据指定的UUID来查找对应的设备
        -L LABLE:根据指定的lable来查找
        e2lable:管理ext系列文件系统的lable   eg e2labl device [lable]
        findfs:查找分区
            findfs [options] LABLE=<lable>
            findfs [options] UUID=<uuid>
    tune2fs:重新设定ext系列文件系统可调整参数的值
        -l：查看指定文件系统超级块信息：super block
        -L 'LABLE' 修改卷标
        -m #：默认5% 位管理人员预留空间占总空间的百分比
        -j 将ext2升级为ext3
        -U UUID修改
    dumpe2fs -h  查看你超级块信息，不显示分组信息
文件系统检测和修复

    常发生于死机或者非正常关机之后
    挂在为文件系统标记为 No clean
    注意：不要在挂载状态下修复
    修复命令工具：
    fsck：file system check
    fsck.fs_type
    fsck -t fs_type
        -a :自动修复错误
        -f :交互式修复错误
    e2fsck：ext系列文件专用的检测修复工具
    -y：自动回答yes
    -f：强制修复
挂载mount

    挂载mount：将额外文件系统与根文件系统某现存的目录建立起关联关系，从而使得此目录作为其他文件访问入口的行为.
    卸载unmount：接触挂载关系，进程正在使用中的设备无法被卸载
    挂载点目录一般为空，如果有文件，则挂载后原文件在挂载完成后倍临时隐藏
    通过查看/etc/mtab 文件显示当前已挂载的所有设备
    mount [-fnrsvw] [-t vfstype] device mount_point
        -v vfstype:指定要挂载的设备上的文件系统类型
        -r： readonly,只读挂载
        -w：read and write，读写挂载
        -n：不更新/etc/mtab
        -a:自动挂载所有支持自动挂载的设备（定义在/etc/fstab文件中,且挂载选项中有auto功能）
        -L 'LABLE'
        -U 'UUID'
        -B,--blind:绑定目录到另一个目录上
cat /proc/mounts 查看内核追踪到的已挂载的所有设备

    -o options：（挂载文件系统的选择），多个选择使用逗号分隔
        async：异步模式
        sync：同步模式，内存更改时同时写磁盘
        atime/noatime:包含目录和文件
        diratime/nodiratime:目录访问时间戳
        auto/noauto: 是否支持自动挂载，是否支持 -a选择
        exec/noexec：是否支持在文件系统上运行应用程序
        dev/nodev: 是否支持在此文件系统上使用设备文件
        suid、nosuid:是否支持suid和sgid权限
        remount 重新挂载
        ro：只读   rw：读写
        user、nouser：是否允许普通用户挂载次设备，默认管理员才能挂载
        acl：启用次文件系统上的acl功能
        default：相当于rw，nosuid，dev，exec，auto，nouser，async
findmnt mount_point     查看挂载情况
查看正在访问指定文件系统的进程：

    lsof mount_point    
    fuser -v mount_point
终止所有正在访问指定的文件系统的进程：
    fuser -km mount_point
卸载 `umount device` 或者 `umount mount_point`

文件挂载配置文件 /etc/fstab

```
1、要挂载的设备或伪文件系统：设备文件、lable（LABLE=""）、UUID（UUID=""）、伪文件系统名称（proc、sysfs）
2、挂载点：
3、文件系统类型
4、挂载选项:defaults,acl ...
5、转储频率:0,不做备份。1、每天转储。2、每隔一天转储
6、自检次序：0、不自检。1、首先自检，一般只有rootfs才用1
```



    创建ISO文件
    cp /dev/cdrom /root/centos7.iso
    mkisofs -r -o /root/etc.iso /etc 
    刻录光盘
    wodim -v -eject centos.iso 
    
    常见工具命令
    free [option]   -m :以M为单位   -g :以GB为单位
    df [option]...[file]...
        -H:以1000为单位
        -T：文件系统类型
        -h：human readable
        -i：inodes instead of block 
        -p：以posix 兼容的格式输出
    
    du [option]...dir 
    -h :human readable
    -s :summary
    
    dd if=/path/from/src of=/path/to/dest
    bs=# :block size ,复制单元大小
    count=# ：复制多少个bs
    of=file 写到所命名的文件
    if=file  从file文件读取
    bs=size     指定块大小（即使ibs也是obs）
    ibs=size    一次读取size个byte
    obs=size    一次写size个byte
    cbs=size    一次转化size个byte
    skip=blocks     从开头忽略blocks个ibs大小的快
    seek=blocks     从开头忽略blocks个obs大小的快
    count=n         只拷贝n个记录
    conv=conversion[,conversion...]     用指定的参数转换文件，转换参数如下：
        ascii  转换ebcdic为ascii
        ebcdic 转换ascii为ebcdic
        block 转换一行数据为度为cbs的记录，不足部分用空格填充
        unblock 替代cbs长度的每一行尾的空格为新行
        lcase 吧大写字符转换为小写字符
        ucase 吧小写字符转换为大写字符
        nocreat 不创建输出文件
        noerror 出错时不停止
        notrunc 不截短输出文件
        sync 把每个输出块填充到ibs个字节，不足部分用空字符补齐
    
    备份MBR： dd if=/dev/sda of=/tmp/mbr.bak bs=512 count=1