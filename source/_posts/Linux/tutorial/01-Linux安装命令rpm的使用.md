---
title: Linux安装命令rpm的使用
layout: page
date: 2015/7/13
updated: 2016/6/13
comments: false
tags: 
- Linux
categories: 
- Linux
---

<!-- toc -->

[TOC]

rpm命令，安装（-i，--install）、卸载、升级（-U，-F）、查询（-q，--query）、校验（-V，--verify）、数据库维护

# 1.安装：

    rpm {-i|--install} [install-options] PACKAGE_FILE…
    v：可视化
    h：以#显示进度
    rpm -ivh package_file
    
    [install-options]
    --test：测试安装
    --nodeps：忽略依赖关系
    --replacepkgs |replacefiles
    --nosignalture：不检查来源合法性
    --noscripts：不执行程序包脚本
        --nopre,--nopost,--nopreun,--nopostun 安装前后，卸载前后

# 2.查询：

    rpm {-q|--query} [select-options][query-options]
    [select-options]
    -a：所有包
    -f:查看指定文件有那个程序安装包生成
    -p rpmfile：正对尚未安装的程序包做查询操作
    --whatprovides capability：查看指定的capability由哪个包所提供

# 3.卸载：

`rpm {-e|--erase} [--allmatches][--nodeps] [--noscripts][--notriggers] [--test] PACKAGE_NAME ...`

# 4.包校验：

    rpm {-V|--verify} [select-options][verify-options]
    S file Size differs
    M Mode differs (includes permissions and file type)
    5 digest (formerly MD5 sum) differs
    D Device major/minor number mismatch
    L readLink(2) path mismatch
    U User ownership differs
    G Group ownership differs
    T mTimediffers
    P capabilities differ
# 5.导入所需要公钥：

    rpm -K|checksig  rpmfile检查包的完整性和签名
    rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
    CentOS 7发行版光盘提供：RPM-GPG-KEY-CentOS-7
    rpm -qagpg-pubkey*

# 6.常用

```
rpm -ivh package_file
rpm -ql  app_name
rpm -qf  file
rpm --import  /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
```





