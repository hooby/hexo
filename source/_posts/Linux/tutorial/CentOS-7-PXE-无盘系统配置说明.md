---
title: CentOS 7 PXE 无盘系统配置说明
layout: page
date: 2015/10/13
updated: 2017/11/13
comments: false
tags: 
- Linux
categories: 
- Linux
---



PXE无盘工作站系统是指由一台或多台“系统服务器”和多台“PXE客户端(无盘工作站)”通过 交换机 相连组成的局域网系统。

![xitong.png](CentOS-7-PXE-无盘系统配置说明/xitong.png)

 系统服务器：通过DHCP+TFTP+NFS服务向无盘工作站提供系统支持

　　· DHCP服务：向PXE客户端分发IP地址、子网掩码、网关等，并指定启动引导文件所在服务器（TFTP服务器）的地址和PXE启动文件（pxelinux.0）

　　· TFTP服务：向PXE客户端传输PXE启动文件、PXE引导配置文件、linux内核vmlinuz，以及系统启动文件initrd.img

　　· NFS服务：向PXE客户端发布工作站的系统（整个根目录“/”的克隆）；为了避免磁盘IO资源的冲突，建议将克隆的系统部署在存储服务器上

 PXE客户端：PXE客户端无需硬盘，只需要一块支持PXE启动的网卡,将“网卡启动”设置为首选

| 操作系统       |      |
| ---------- | ---- |
| CentOS 7.2 |      |

| 软件环境                              |                              |
| --------------------------------- | ---------------------------- |
| tftp-server-5.2-12.el7.x86_64     | 向无盘工作站传输系统启动文件等              |
| nfs-utils-1.3.0-0.21.el7.x86_64   | 共享发布工作站系统                    |
| syslinux-4.05-12.el7.x86_64       | 提供引导程序"pxelinux.0"           |
| dhcp-4.2.5-42.el7.centos.x86_64   | 提供DHCP 服务；指定TFTP 地址及PXE 启动文件 |
| dracut-033-359.el7.x86_64         | 用来制作启动initrd 镜像              |
| dracut-network-033-359.el7.x86_64 | 依赖包，否则将导致PXE无法启动             |

### 首先，克隆好工作站的系统模板

配置之前关掉防火墙和selinux

```
systemctl stop firewalld  #我还是比较习惯用iptables 所以关掉了 firewalld
systemctl disable firewalld 
iptables -F #清空防火墙策略，如果之前有布置防火墙策略的需要查看下是否真要清空
iptables -vnL #查看filter防火墙策略

setenforce 0   #临时关闭selinux，重启后失效
vim /etc/sysconfig/selinux
SELINUX=disabled #关闭selinux，重启后生效


```

\1. 创建工作站系统模板的存放目录（/nodiskos/workstation）和启动引导文件存放目录（/nodiskos/tftpboot）

```
mkdir /nodiskos                               # 系统模板+启动文件存放目录
mkdir /nodiskos/tftpboot                      # 工作站系统启动文件存放目录
mkdir /nodiskos/workstation                   # 工作站系统模板存放目录
```

\2. 使用rsync 工具将整个"/"目录拷贝到/nodiskos/workstation 目录下，去除不需要的文件目录

```
rsync -av --exclude='/proc' --exclude='/sys' --exclude='/tmp' --exclude='/var/tmp' --exclude='/etc/mtab' --exclude='/nodiskos' /* /nodiskos/workstation

```

 注意：在此处如果你有临时用的U盘或者移动硬盘之类的，先umount掉，否则会吧里面的内容一并拷贝到/nodiskos/workstation。很耗费时间，而且也没有用。

\3. 重新创建临时数据等目录，还原系统模板的目录结构，同时清除掉本机的主机名和IP配置

```
cd /nodiskos/workstation
mkdir proc sys tmp var/tmp
rm -f /nodiskos/workstation/etc/hostname
rm -f /nodiskos/workstation/etc/sysconfig/network-scripts/ifcfg-enp*

```

4.调整系统模板的设备挂载配置文件/nodiskos/workstation/etc/fstab：删除所有的本地存储设备挂载信息（如：/和/boot）；添加系统模板的挂载信息。IP（192.168.199.162）是你的服务端

```
192.168.199.162:/nodiskos/workstation　　/　　nfs　　　　defaults　　　　　　0 0

```

5.将整个工作站系统模板打包备份到系统服务器的/opt 目录下，以作备用

```
cd /nodiskos
tar -cvf /opt/workstation.tar workstation
```

### 准备好工作站启动引导需要的文件

\1. 安装syslinux 和dracut 软件包

```
yum install syslinux dracut dracut-network
```

\2. 拷贝PXE启动文件（由syslinux 程序提供）

```
cp /usr/share/syslinux/pxelinux.0 /nodiskos/tftpboot
```

\3. 拷贝用linux内核文件vmlinuz

```
cp /boot/vmlinuz-3.10.0-327.el7.x86_64 /nodiskos/tftpboot
```

\4. 创建用于系统启动 镜像文件initrd.img

```
dracut initrd-3.10.0-327.el7.x86_64.img 3.10.0-327.el7.x86_64  
# 通用的做法是dracut initrd-`uname -r`.img `uname -r`
# 我在CentOS 7 上使用这种方法没有成功，还花费了我大量时间去排查错误，所以我使用的是镜像系统/image/pxeboot/initrd.img 改下名字就 可以了
chmod 644 initrd-3.10.0-327.el7.x86_64.img
mv initrd-3.10.0-327.el7.x86_64.img /nodiskos/tftpboot
```

    5. 在/nodiskos/tftpboot/**pxelinux.cfg**/目录下创建默认的PXE引导配置文件"**default**"

```
# prompt 0 表示工作站立即启动，1 表示工作站等待选择
# kernel 指定内核文件
# append 后面的加下划线的是一行内容，不能换行！！！
# append 附加参数值解释说明：
# initrd= 指定用于引导的initrd 镜像文件
# root= 指定工作站系统的nfs 路径
# selinux= 设置selinux 开关，0 表示关闭，1 表示开启，默认为1
# rw 设置工作站系统为可读写
# nomodeset 这个参数是配合后面的vga=参数一起使用，设置分辨率
# vga= 这个参数值是设置分辨率，0x 表示十六进制，0314 表示800*600 16 位色
# 文件内容如下：

default auto
label auto
prompt 0
kernel  vmlinuz-3.10.0-327.el7.x86_64
append initrd=initrd-3.10.0-327.el7.x86_64.img root=nfs:192.168.199.162:/nodiskos/workstation selinux=0 ip=dhcp rw nomodeset vga=0x0314
# 这里rw 需要斟酌考虑，如果你是给多个客户端使用，又怎会让他随便写数据呢（export文档中的也是如此）
```

 

\6. 最后，/disklessboot/tftpboot 目录下应该有下列几个文件/目录：

|                                  |                  |           |
| -------------------------------- | ---------------- | --------- |
| pxelinux.0                       | PXE 引导文件         | （由第2 步创建） |
| pxelinux.cfg/default             | 默认的引导配置文件        | （由第5 步创建） |
| initrd-3.10.0-327.el7.x86_64.img | 用于引导的initrd 镜像文件 | （由第4 步创建） |
| vmlinuz-3.10.0-327.el7.x86_64    | 用于引导的内核文件        | （由第3 步创建） |

### 配置DHCP服务

\1. 安装DHCP 服务软件包

```
yum install dhcp
```

\2. 编辑配置文件/etc/dhcp/dhcpd.conf

```
# dhcpd.conf 部分参数说明：
# default-lease-time      # 指定确认租赁时长，单位为秒，-1 表示无限制
# max-lease-time          # 指定最大租赁时长
# authritative            # 拒绝不正确的IP 地址的要求
# subnet netmask {}       # 设置dhcp 区域
# range                   # 提供动态分配IP 的范围；若所有工作站都是绑定的固定IP，可删除此配置
# option routers          # 设置网关/路由器地址，多个地址用逗号隔开；若不想让客户端上网，可删除此配置
# domain-name-servers     # 设置DNS，若不想让客户端上网，可删除此配置；多个地址用逗号隔开
# next-server             # 告知工作站TFTP 服务器的地址，TFTP 服务提供启动引导
# filename                # 告知工作站PXE 引导程序名
# host XXX {}             # 此处是根据工作站的MAC 地址绑定固定的IP 地址，前提是知道MAC 地址
# hardware ethernet       # 工作站的MAC 地址，一定要小写
# fixed-address           # 绑定固定的IP 地址，和range 的不会有冲突，优先以它为主
# 配置内容如下：

ddns-update-style none;
ignore client-updates;
default-lease-time -1;
max-lease-time -1;
authritative;

subnet 192.168.199.0 netmask 255.255.255.0 {
range 192.168.199.100 192.168.199.200;
option routers 192.168.199.254;
option domain-name-servers 114.114.114.114,8.8.8.8;
next-server 192.168.199.162;
filename "pxelinux.0";

#下面是给指定mac地址分配指定IP的
host node1 {
    hardware ethernet 12:34:56:78:ab:cd;
    fixed-address 172.16.10.11;
    }

}
```

\3. 重启DHCP服务，且将DHCP服务设置成开机自启

```
systemctl start dhcpd
systemctl enable dhcpd
systemctl status dhcpd
```

### 配置TFTP服务

\1. 安装TFTP 服务软件包

```
yum install tftp-server
```

\2.  编辑配置/etc/xinetd.d/tftp，只需更改如下2 处：

```
# and to start the installation process for some operating systems.
service tftp
{
　　...... ........
　　server_args = -s /nodiskos/tftpboot             
# 改成启动文件的存放目录（如果不修改此项，上面所有配在/nodiskos/tftpboot目录中的都要配在/var/lib/tftpboot目录下）
　　Disable = no                                    
# 将yes 改成no，以激活此服务
　　...... ........
}
```

\3. TFTP 服务是通过xinetd 工具管理的，因此需要通过xinetd 启动、停止、重启等

```
systemctl start xinetd
systemctl status xinetd
systemctl enable xinetd 
```

• 配置NFS服务

\1. 安装NFS 服务软件包

```
yum install nfs-utils rpcbind
```

\2. 编辑配置文件/etc/exports，添加如下内容：

```
# 这一行是配置默认的工作站系统目录
/nodiskos/workstation　　192.168.199.0/24(rw,async,no_root_squash)
```

\3. 重启NFS服务，且将NFS服务设置成开机自启

```
   systemctl start rpcbind.socket 
   systemctl start rpcbind.service 
   systemctl status rpcbind.service 
   systemctl status rpcbind.socket 
   systemctl start nfs-server
   systemctl status nfs-server 
```

\4. 至此，所有配置都已完成了。最后检查下DHCP、TFTP、NFS这3个服务是否都已启动，是否都已设置成开机自启，将所有PXE客户端的启动项设置成首选网卡启动，然后就启动PXE客户端了。

参考资料  <http://www.bkjia.com/Linuxjc/1121652.html>

补充：关于主机名

```
chmod +x /nodiskos/workstation/etc/rc.local
vim /nodiskos/workstation/etc/rc.local

eth0_name=enp129s0f0
ib_name=ib0
node1_mac="0c:c4:7a:85:3b:04"
node2_mac="0c:c4:7a:86:77:d6"
node3_mac="0c:c4:7a:82:c6:18"
node4_mac="0c:c4:7a:e0:75:72"
node5_mac=
node6_mac=
node7_mac="0c:c4:7a:82:c5:d8"
address=`ip a |grep -A 1 $eth0_name |grep link/ether|sed 's/[ ][ ]*/ /' |cut -d" " -f3`
case $address in

$node1_mac)
ip addr add 172.16.20.11/24 dev $ib_name
hostname node1
;;
$node2_mac)
ip addr add 172.16.20.12/24 dev $ib_name
hostname node2
;;
$node3_mac)
ip addr add 172.16.20.13/24 dev $ib_name
hostname node3
;;
$node4_mac)
ip addr add 172.16.20.14/24 dev $ib_name
hostname node4
;;
$node5_mac)
ip addr add 172.16.20.15/24 dev $ib_name
hostname node5
;;
$node6_mac)
ip addr add 172.16.20.16/24 dev $ib_name
hostname node6
;;
$node7_mac)
ip addr add 172.16.20.17/24 dev $ib_name
hostname node7
;;

*)

;;
esac
```