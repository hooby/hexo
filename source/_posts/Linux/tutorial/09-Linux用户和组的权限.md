---
title: Linux用户和组的权限
layout: page
date: 2015/8/2
updated: 2016/6/13
comments: false
tags: 
- Linux
categories: 
- Linux
---

<!-- toc -->

[TOC]

# 1、文件的权限分类

    文件的权限对象分三类：属主（u）、属组（g）、其他（o），每个对象都有rwx，读写执行三类权限。
    对于文件
        r：可查看文件内容
        w：可修改其类容
        x：可把此文件提请内核启动为一个进程
    对于目录
        r：可使用ls查看此目录中文件列表
        w：可在此目录中创建和删除文件
        x：可使用ls查看目录中文件列表，可以cd进入此目录
        X：只给目录x权限，不给文件x权限
    在访问文件之前，必须要先能够进入文件的父目录
    前提：进程有属主和属组；文件有属主和属组
    (1) 任何一个可执行程序文件能不能启动为进程：取决发起者对程序文件是否拥有执行权限
    (2) 启动为进程之后，其进程的属主为发起者；进程的属组为发起者所属的组
    (3) 进程访问文件时的权限，取决于进程的发起者
        (a) 进程的发起者，同文件的属主：则应用文件属主权限
        (b) 进程的发起者，属于文件属组；则应用文件属组权限
        (c) 应用文件“其它”权限
    root 没有了 x 执行权限不能执行，但是可以给自己加权限
    root用户没有读和写权限也可以直接修改查看。

# 2、chmod 用法

    a、chmod [option]... 八进制  file
        eg：chmod -R 755 /dir
    b、chmod [option]... mode ... file 
        eg：chmod u=rwx,g=rx,u= file/dir
        或者 chmod a=rx file/dir 或者 chmod u+w file/dir
    c、chmod [option] ... --reference = rfile file 
        参考rfile权限 复制到 file
        
    chmod -R a=rwX dir
    目录下的文件如果有x权限 则都有x权限
    如果没有x权限 则 所有的文件都没有x权限 
    但是所有的目录都有x权限

# 3、新建文件和目录的默认权限

    新建FILE权限:666-umask如果所得结果某位存在执行（奇数）权限，则将其权限+1
    新建DIR权限: 777-umask,非特权用户umask是002,root的umask是022
    umask: 查看  umask#: 设定  #umask -S --> u=rwx,g=rx,o=rx #umask -p --> umask 0022

# 4、Linux文件系统上的特殊权限，SUID, SGID, Sticky

    chmod 4755 file
    chmod u+s /usr/bin/passwd
    -rwsr-xr-x. 1 root root 30768 Feb 22  2012 /usr/bin/passwd
    当用户使用passwd【具有suid权限的二进制程序文件】命令，用户权限临时切换成【passwd文件所有者】权限
    启动为进程之后，其进程的属主为原程序文件的属主
    /bin   文件夹下面有众多 红色背景的程序均是这样。
    
    chmod 2755 
    /usr/bin/passwd chmod g+s /usr/bin/passwd
    -rwxr-sr-x. 1 root root 30768 Feb 22  2012 /usr/bin/passwd
    当用户使用passwd【具有suid权限的二进制程序文件】命令，用户权限临时切换成【passwd文件所属组】权限    --黄色背景
    启动为进程之后，其进程的属主为原程序文件的属组
    
    chmod 1777 /dir
    [root@localhost conf]#ll -d /tmp 
    drwxrwxrwt. 18 root root 4096 Oct 16 02:52 /tmp
    可以在目录中删除【自己的】文件，而不能删除别人的文件
    
    chmod 2755 dir
    chmod g+s /dir
    -rwxr-sr-x. 1 root root 30768 Feb 22  2012 /dir 
    当该目录【所属组用户】在该目录下创建文件的时候，创建的文件所属组均为目录的所属组
    
    小坑一个： 用0777，并不能把sst等权限去掉
    没有执行权限则该位置显示为【S、T】

# 5、设定文件特定属性（attr--attribute）

    chattr +i 不能删除，改名，更改 +a 只能追加（>>）
    lsattr 显示文件特定属性。

# 6、ACL

    ACL：Access Control List，实现灵活的权限管理，除了u，g，o可以对更多的用户设置权限
    ACL生效顺序：所有者（属主，属组），自定义用户，自定义组，其他人
    
    setfacl [-bkndRLP] { -m|-M|-x|-X ... } file ...
        -b,--remove-all：删除所有扩展的acl规则，基本的acl规则(所有者，群组，其他）将被保留。 
        -k,--remove-default：删除缺省的acl规则。如果没有缺省规则，将不提示。 
        -n，--no-mask：不要重新计算有效权限。setfacl默认会重新计算ACL mask，除非mask被明确的制定。 
        --mask：重新计算有效权限，即使ACL mask被明确指定。 
        -d，--default：设定默认的acl规则。 
        --restore=file：从文件恢复备份的acl规则（这些文件可由getfacl-R产生）。通过这种机制可以恢复整个目录树的acl规则。
            此参数不能和除--test以外的任何参数一同执行。 
        --test：测试模式，不会改变任何文件的acl规则，操作后的acl规格将被列出。 
        -R，--recursive：递归的对所有文件及目录进行操作。 
        -L，--logical：跟踪符号链接，默认情况下只跟踪符号链接文件，跳过符号链接目录。 
        -P，--physical：跳过所有符号链接，包括符号链接文件。 
        --version：输出setfacl的版本号并退出。 --help：输出帮助信息。 
        --：标识命令行参数结束，其后的所有参数都将被认为是文件名 
        -：如果文件名是-，则setfacl将从标准输入读取文件名。
        
    Usage: setfacl [-bkndRLP] { -m|-M|-x|-X ... } file ...
      -m, --modify=acl        modify the current ACL(s) of file(s)
      -M, --modify-file=file  read ACL entries to modify from file
      -x, --remove=acl        remove entries from the ACL(s) of file(s)
      -X, --remove-file=file  read ACL entries to remove from file
    
    例子：
        mount -o acl /directory
        getfacl file|directory      --获取文件或者目录的acl
        setfacl -m u:wang:rwx file|directory    --增加文件或者目录的用户acl
        setfacl -Rm g:sales:rwX directory       --目录及其子目录以及文件增加用户acl，新增加文件或者目录不会自带acl
        setfacl -M file.acl file|directory      --参照file.acl 给目录或者文件增加acl
        setfacl -m g:salesgroup:rw file| directory  --增加文件或者目录的用户组acl
        setfacl -m d:u:wang:rx directory        --增加该目录的acl，新增文件或者目录的时候【自带acl】，目录下原有的文件或者目录acl不变
        setfacl -x u:wang file|directory        --删除文件或者目录中，对应用户的acl ，mask还在
        setfacl -X file.acl directory           --参照file.acl 删除目录中的acl ，mask还在
        setfacl -Rb directory                   --清空目录下的所有acl，包括mask
        setfacl-k dir                           --删除默认ACL权限
    
    【注意】：
        默认拷贝不会拷贝文件或者目录的acl，用cp来复制文件的时候我们现在可以加上-p选项。对于不能拷贝的ACL属性将给出警告。
        mv命令将会默认地移动文件的ACL属性，同样如果操作不允许的情况下会给出警告。
        如果你的文件系统不支持ACL的话，你也许需要重新mount你的file system： mount -o remount, acl [mount point] 
        如果用chmod命令改变Linux file permission的时候相应的ACL值也会改变，反之改变ACL的值，相应的file permission也会改变。

