---
title: Linux终端类型
layout: page
date: 2015/8/3
updated: 2016/6/13
comments: false
tags: 
- Linux
categories: 
- Linux
---

<!-- toc -->

[TOC]

# 1.终端的概念

所谓终端就是在一个连接主机的端子上面接入鼠标、键盘、显示器等所组成的一个组合。终端用于用户与主机之间的交互。早期的时候，在大型主机上，想使用主机的用户很多，但是主机却只有一个，也不能实现人手一台。于是为了让一台主机给多个用户使用，便产生了多个终端和多个用户，这样每个用户只要有一个终端变可以与主机交互了。

# 2.终端的类型

终端的类型有四类：物理终端，虚拟终端，图形终端和伪终端。

## 物理终端：

将鼠标、键盘、显示器直接连接到主机的接口上。也称为物理控制台（console）--/dev/console。

## 虚拟终端：

附加在物理终端之上，用软件的方式实现虚拟终端。centos中有6个虚拟中断，通过Alt+Ctrl+（F1-F6）进行切换。

## 图形终端：

图形终端也是附加在物理终端之上，用软件的方式实现。但提供桌面环境，切换方式 Ctrl+Alt+F7.

## 伪终端：

在图形界面下打开的的命令行界面，或者基于ssh协议或Telnet协议等远程打开的命令行界面。例如用xshell或者secureCRT远程连接的命令行界面就是属于伪终端。



在这里我们要注意的是，实际操作中当我们的系统启动还没有完全运行起来的时候，在我们的设备上运行的终端是物理终端，在服务完全启动之后映射的就是虚拟终端或者图形终端啦。