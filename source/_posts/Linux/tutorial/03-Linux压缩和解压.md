---
title: Linux压缩和解压
layout: page
date: 2015/7/18
updated: 2016/6/13
comments: false
tags: 
- Linux
categories: 
- Linux
---

<!-- toc -->

[TOC]

在Linux系统中压缩、解压以及归档的工具主要有一下几种：

```
file-roller ： 这个工具适合在桌面系统下进行解压操作。
compress/uncompress  ：这是一个比较老的解压工具，压缩后会添加.Z为后缀
gzip/gunzip : .gz结尾
bzip2/bunzip2 : .bz2
xz/unxz : .xz 
zip/unzip .zip
tar 
cpio

主要命令参数介绍
compress [-dvc] [file...]
    -d: 解压，相当于uncompress
    -c: 解压结果标准输出，不删除源文件
    -v: 显示详情
zcat file.Z -->不解压的情况下查看文件
zcat file.Z >file  -->解压保留源文件
    
gzip [-dc#] file...
    -d:解压缩 =gunzip
    -c:标准输出
    -#：1-9,指定压缩比，默认为6
zcat file.gz -->不解压的情况下查看文件
zcat file.gz >file  -->解压保留源文件  
    
bzip2 [-kd#] file...
    -k:keep,即保留源文件
    -d:解压缩=bunzip2
    -#：指定压缩比，默认6
bzcat ：不解压下查看文件内容
    
xz [-kd#] file...
    -k:keep,即保留源文件
    -d:解压缩=unxz
    -#：指定压缩比，默认6
xzcat ：不解压下查看文件内容

zip 
    -r : 递归压缩，文件及目录
    zip –r /testdir/sysconfig.zip /etc/sysconfig/  ...
    -d :删除压缩文件中指定的文件
    -m :添加
    zip -d myfile.zip old.txt
    zip -m myfile.zip new.txt
unzip -p message.zip > message

    tar []          tape archive  磁带 归档
        -c create
        -f  file.tar
        -v  可视化
        -x  解包
        -r  add追加
        -t  预览文件名
        -C 展开到指定目录
        -z 归档后压缩成 .tar.gz 
        -j .tar.bz2
        -J .tar.xz
        -T 指定输入文件
        -X 指定要排除文件
        常用组合
        tar -cvf archive.tar file ...  归档
        tar -tvf archive.tar        查看归档文件名列表
        tar -xvf archive.tar        解包

cpio:  copy input output,可解压以.cpio 或者.tar结尾的文件
cpio [option] > file or devicename 
cpio [option] < file.cpio or devicename 
    -o :打包生成file.cpio
    -i ：从cpio中解包
    -t :预览文件名
    -v :显示过程
    -d ：解包生成目录
    常用组合
    find ./etc |cpio -ov > etc.cpio
    cpio -tv < etc.cpio
    cpio -iv < etc.cpio
    cpio -idv < etc.cpio

```

