---
title: Linux用户和组
layout: page
date: 2015/7/30
updated: 2016/6/13
comments: false
tags: 
- Linux
categories: 
- Linux
---

<!-- toc -->

[TOC]

# 1、用户

个人理解的是，给使用者一个在系统中使用的身份，即用户。
用户分两种：管理员和普通用户。
而每一个用户都有一些属性，每一个属性都是用冒号分割开来。配置文件存储在【/etc/passwd】中。
例如，`sarash:x:507:508::/home/sarash:/bin/nologin`
他们分别是，用户名，密码，ID号，主组ID号，注释，家目录，默认登录shell
用户名是用来登录的账号，密码位置并没有存储真正的密码在这里，只是一个占位符。如果的确想要看到密码存储结果，可以使用命令：pwunconv。
其中 pwconv, pwunconv, grpconv, grpunconv - convert to and from shadow passwords and groups 就是用户密码隐藏 显示 组密码隐藏显示的命令
 增加用户的常用命令是：
` useradd [options] login`

    -u UID: [UID_MIN, UID_MAX]定义在/etc/login.defs
    -o 配合-u 选项，不检查UID的唯一性
    -g GID：指明用户所属基本组，可为组名，也可以GID
    -c "COMMENT"：用户的注释信息
    -d HOME_DIR:以指定的路径(不存在)为家目录
    -s SHELL: 指明用户的默认shell程序  在/etc/shells文件中有系统可用shell
    -G GROUP1[,GROUP2,...]：为用户指明附加组，组必须事先存在
    -N 不创建私用组做主组，使用users组做主组
    -r: 创建系统用户CentOS 6: ID<500，CentOS 7: ID<1000
` userdel [options] LOGIN`
       -f, --force 删除用户的主目录和邮箱，即使用户登录中，或者邮箱公用
       -r           删除用户家目录及邮箱
    【注意】：在新建服务型用户的时候（例如MySQL），由于这些用户不需要登录（/bin/nologin），也不需要家目录，所以定义成系统用户最好。可以防止恶意的登录。
    一般来说， 1~99 会保留给系统预设的帐号，另外 100~499 则保留给一些服务来使用。centos7 就加倍。
    
    #cat 【/etc/default/useradd】  用户配置信息
    # useradd defaults file     --新增用户时的默认信息
    GROUP=100                   --user组的组ID
    HOME=/home                  --默认家目录路径
    INACTIVE=-1                 --宽限期？
    EXPIRE=                     --口令有效期？
    SHELL=/bin/bash             --默认shell
    SKEL=/etc/skel              --默认配置信息文件
    CREATE_MAIL_SPOOL=yes       --是否创建邮箱

# 2、密码

    #cat /etc/shadow
    root:$6$f3B1B1NC$zybHBGqE/SWMtRVk8iqVz.WP.JC/kmBDajcPc4JFcwsPZS/RvsJqJDTZMGtH7CSs5guC40DqXWegF7zRY/x7M1:17089:0:99999:7:::
    该配置文件中以":"分割，分别代表：用户名，密码，上一次修改时间，两次修改密码相隔最短时天数，两次修改密码最长天数，提前多少天提示用户修改密码，口令过期几天后禁用，用户过期日期--从1970年算起，最后是保留字段
    对于密码位置的解释：
    $6$:这个6 表是加密方式。1：MD5 5：SHA-256 6：SHA-512（常见）更改加密算法 authconfig--passalgo=sha256 --update
    $f3B1B1NC$：这就是salt，盐。加密的时候，根据用户的密码，加上这串字符，组成了独一无二的密码显示格式。
    $第三串字符$：加密后的字符。用户登录时 ，加密格式+salt+输入密码=密码字符串。 单向，不能逆算。得出的字符串相同才能登录。
    1、如果密码为空，则该用户在登录时，无须输入密码即可登录，这在做一些测试的时候可以用到。
    2、如果密码为！+密码，表示该用户已经被锁定了。
    3、!! 如果是两个叹号，表示该用户还没有设置密码，或者说用户的密码已经过期。
    4、*，在服务用户那边有些是星号【，。。。好像是不需要密码，网上暂时没找到解释，节约时间先留下】
    
    对于/etc/login.defs 这个文件配置信息如下
    MAIL_DIR    /var/spool/mail  --邮件目录
    
    PASS_MAX_DAYS   99999  --密码最长使用天数 99999 相当于无限大
    PASS_MIN_DAYS   0      --密码最小禁止修改的时间，即在这个时间内禁止密码再次修改
    PASS_MIN_LEN    5     --密码最小长度
    PASS_WARN_AGE   7     --密码过期前多少天开始给予提醒
    
    UID_MIN           500 --用户新建最小ID号
    UID_MAX         60000 --用户新建最大ID号
    GID_MIN           500 --用户组新建最小ID号
    GID_MAX         60000 --用户组新建最大ID号
    
    CREATE_HOME yes       --默认是否创建家目录
    UMASK           077   在家目录新建文件的权限umusk值
    USERGROUPS_ENAB yes   --删除用户时 如果用户组没有用户是否删除用户组
    ENCRYPT_METHOD SHA512 --加密格式

用法：`usermod [选项] 登录（用户名）`

```
选项：
  -c, --comment 注释            GECOS 字段的新值
  -d, --home HOME_DIR           用户的新主目录
  -e, --expiredate EXPIRE_DATE  设定帐户过期的日期为 EXPIRE_DATE
  -f, --inactive INACTIVE       过期 INACTIVE 天数后，设定密码为失效状态
  -g, --gid GROUP               强制使用 GROUP 为新主组
  -G, --groups GROUPS           新的附加组列表 GROUPS
  -a, --append GROUP            将用户追加至上边 -G 中提到的附加组中，
                                并不从其它组中删除此用户
  -l, --login LOGIN             新的登录名称
  -L, --lock                    锁定用户帐号
  -m, --move-home               将家目录内容移至新位置 (仅于 -d 一起使用)
  -o, --non-unique              允许使用重复的(非唯一的) UID
  -p, --password PASSWORD       将加密过的密码 (PASSWORD) 设为新密码
  -R, --root CHROOT_DIR         chroot 到的目录
  -s, --shell SHELL             该用户帐号的新登录 shell
  -u, --uid UID                 用户帐号的新 UID
  -U, --unlock                  解锁用户帐号
  -Z, --selinux-user  SEUSER    用户账户的新 SELinux 用户映射
```



# 3、用户组

    具有某种共同特征的用户集合起来就是用户组（Group）。用户组（Group）配置文件主要有 /etc/group和/etc/gshadow
    cat/etc/group
    root:x:0:gentoo
    users:x:100:
    /etc/group 的内容包括用户组（Group）、用户组口令、GID及该用户组所包含的用户（User），每个用户组一条记录；格式如下：
    group_name:passwd:GID:user_list   用户组的GID如同UID，系统组在500或者1000以下。

# 4、用户组密码

    对于大型服务器，针对很多用户和组，定制一些关系结构比较复杂的权限模型，设置用户组密码是极有必要的。比如我们不想让一些非用户组成员永久拥有用户组的权限和特性，这时我们可以通过密码验证的方式来让某些用户临时拥有一些用户组特性，这时就要用到用户组密码；
    cat /etc/gshadow
    格式如下：
    groupname:password:admin,admin,...:member,member,...
    密码位置如果为！或者为空表示无须密码，或者尚未设置密码。
