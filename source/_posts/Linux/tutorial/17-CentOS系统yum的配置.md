---
title: CentOS系统yum的配置
layout: page
date: 2015/8/25
updated: 2015/10/13
comments: false
tags: 
- Linux
categories: 
- Linux
---

<!-- toc -->

[TOC]

yum仓库使用起来特别方便，然而使用之前当然是要配置的啦。下面就介绍一下怎么从0 配置一个yum仓库。
首先要创建yum仓库，当然不能使只给一台服务器用，那要给多个服务器用的话，就需要网络服务。yum仓库支持的网络服务有两种，FTP和HTTP。用yum主要用到的是数据传输，因此FTP更适合创建yum仓库，下面就以FTP为例，说明一下yum仓库的配置。
第一步是要先在自己的服务器上提供FTP服务，而这个时候还没有yum仓库，所以只能使用rpm的方式安装vsftp，把FTP服务搭起来。这个包必须要提前有，或者从光盘中获取。
获取安装 FTP服务 并启用

```
mkdir /mnt/cdrom 
mount /dev/cdrom  /mnt/cdrom
rpm -ivh /mnt/cdrom Packages/vsftpd-3.0.2-10.el7.x86_64.rpm 
```

注意，在CentOS 6 和CentOS 7 上开启FTP服务的命令式不一样的。
iptables -F（在CentOS 7 上 也可以使用systemctl stop firewalld 来关闭防火墙）
CentOS-6 开启FTP服务
chkconfig vsftpd on  ===  立即打开FTP，但重启后失效
service vsftpd start  ===  开启FTP服务，但不会立即生效
CentOS-7 开启FTP服务
systemctl  enable  vsftpd   === 立即打开FTP，但重启后失效
systemctl start vsftpd  === 开启FTP服务，但不会立即生效

到此为止FTP服务就开启啦，在一个局域网内的电脑只需要在浏览器里面输入 ftp://ip 就可以访问这个服务器啦
但是里面说明内容的没有，顶多只有一个lost+found 和pub 的文件夹，这个目录就是本机的/var/ftp/ 共享文件要放到这个文件里面。
需要注意一点，共享的文件所有FTP客户端都可以访问，所以我们这里如果创建yum仓库的话是可以给众多使用yum的系统提供yum仓库的，只不过不同系统例如CentOS 5 、CentOS 6.7 CentOS6.8  CentOS 7 等不同版本的系统使用的程序安装包不同，所以只需要在这里分类创建文件夹，供各类系统使用即可。
实例：

```
http://server/centos/releasever/basearch/
http://server/centos/7/x86_64
http://server/centos/6/i384
```

这样客户端只需要识别自己的系统版本$releasever和系统架构$basearch，就可以轻松访问FTP指定的文件夹，不需要手动修改选择进入 6 还是 7 文件夹。
分类创建好文件夹，并把rpm拷贝到指定目录下之后，执行：createrepo <directory> 这个directory 目录要指定到rpm包所在目录 或者上一级，执行完后该目录下会有一个名叫repodata的目录，该目录中记录了这个yum仓库所拥有的程序包。同时，在客户端配置yum的时候，输入yum源路径是要指定到directory这一级目录的。
如果你的电脑没有安装 createrepo-0.9.9-23.el7.noarch.rpm 包的话是不能执行 createrepo 的 ，需要安装。
而如果你想把光盘或者系统盘ISO文件中的rpm全部共享出去，只需要把光盘挂载到/var/ftp/pub/..  目录下面就可以了，也不需要执行 createrepo 这个程序，光盘里面已经有了repodata这个文件夹。

```
eg：mount -o loop /root/CentOS-6.8-x86_64-bin-DVD1.iso  /var/ftp/pub/6.8/ 或者 mount /dev/cdrom  /mnt/cdrom
```



