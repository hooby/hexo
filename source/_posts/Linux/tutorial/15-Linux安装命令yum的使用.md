---
title: Linux安装命令yum的使用
layout: page
date: 2015/8/23
updated: 2015/10/13
comments: false
tags: 
- Linux
categories: 
- Linux
---

<!-- toc -->

[TOC]

YUM: yellowdog update modifier ，rpm的前端程序，用来解决软件包相关依赖性，可以在多个库之间定位软件包。
yum repository：yum repo，存储了众多RPM包，以及包相关的元数据文件，放置于特定目录repodata下。
yum 访问的文件服务器主要有三种，ftp，http，file。

yum客户端配置文件：
    【/etc/yum.conf】:为所有仓库提供公共配置,man /etc/yum.conf 有英文详解
        [main]
        cachedir=/var/cache/yum
            //yum 缓存的目录，yum 在此存储下载的rpm 包和数据库，默认设置为/var/cache/yum
        keepcache=0
        　　//安装完成后是否保留软件包，0为不保留（默认为0），1为保留
        debuglevel=2
        　　//Debug 信息输出等级，范围为0-10，缺省为2
        logfile=/var/log/yum.log
        　　//yum 日志文件位置。用户可以到/var/log/yum.log 文件去查询过去所做的更新。
        pkgpolicy=newest
            //包的策略。一共有两个选项，newest 和last，这个作用是如果你设置了多个repository，而同一软件在不同的repository 中同时存在，yum 应该安装哪一个，如果是newest，则yum 会安装最新的那个版本。如果是last，则yum 会将服务器id 以字母表排序，并选择最后的那个服务器上的软件安装。一般都是选newest。
        distroverpkg=centos-release
        　　//指定一个软件包，yum 会根据这个包判断你的发行版本，默认是 centos-release，也可以是安装的任何针对自己发行版的rpm 包。
        tolerant=1
            //有1和0两个选项，表示yum 是否容忍命令行发生与软件包有关的错误，比如你要安装1,2,3三个包，而其中3此前已经安装了，如果你设为1,则yum 不会出现错误信息。默认是0。
        exactarch=1
        　　//有1和0两个选项，设置为1，则yum 只会安装和系统架构匹配的软件包，例如，yum 不会将i686的软件包安装在适合i386的系统中。默认为1。
        retries=6
        　　//网络连接发生错误后的重试次数，如果设为0，则会无限重试。默认值为6.
        obsoletes=1
        　　//这是一个update 的参数，具体请参阅yum(8)，简单的说就是相当于upgrade，允许更新陈旧的RPM包。
        plugins=1
            //是否启用插件，默认1为允许，0表示不允许。我们一般会用yum-fastestmirror这个插件。官方yum源的速度实在让人不敢恭维，而非官方的yum源又五花八门，让人难以取舍。幸运的是，yum-fastestmirror插件弥补了这一缺陷：自动选择最快的yum源。安装之后，生成配置文件/etc/yum/pluginconf.d/fastestmirror.conf。配置文件中的hostfilepath字段，用于定义yum源的配置文件（通常是/var/cache/yum/x86/7/timedhosts.txt），然后我们就可以将所知道的yum源统统写入这个txt文件
        bugtracker_url=http://bugs.centos.org/set_project.php?project_id=16&ref=http://bugs.centos.org/bug_report_page.php?category=yum
    
        metadata_expire=1h
        installonly_limit = 5
    
    【/etc/yum.repo.d/*.repo】:为仓库的指向提供配置 主要配置 [repositoryID] baseurl  gpgcheck  三个即可使用仓库
        [repositoryID]  //仓库ID
        name=Some name for this repository//仓库名称
        baseurl=url://path/to/repository/ //仓库路径，路径必须给到 repodata文件夹所在的目录
                URL可以有一下三种，file，ftp，http
        enabled={1|0}       //使能
        gpgcheck={1|0}      //是否检查来源合法性
        gpgkey=URL          //gpg证书来源路径
        enablegroups={1|0
        failovermethod={roundrobin|priority}
        默认为：roundrobin，意为随机挑选；
        cost= 默认为1000
        
        yum的repo配置文件中可用的变量：
            $releasever: 当前OS的发行版的主版本号
            $arch: 平台，i386,i486,i586,x86_64等
            $basearch：基础平台；i386
            $YUM0-$YUM9:自定义变量
            实例:
            baseurl=http://server/centos/$releasever/$basearch/


yum命令的用法：yum [options] [command] [package ...]
        yum-config-manager --add-repo= http://172.16.0.1/cobbler/ks_mirror/CentOS-X-x86_64/ 快速非交互式创建yum仓库
        yum-config-manager --disable “仓库名" 禁用仓库
        yum-config-manager --enable “仓库名” 启用仓库
        yum repolist [all|enabled|disabled] 显示仓库 所有的|可用的|禁用的
        yum list [all | glob_exp1] [glob_exp2] [...] 根据需求筛选需要的RPM包
        yum list {available|installed|updates} [glob_exp1]
        yum install package1 [package2] [...]
        yum reinstall package1 [package2] [...] 重新安装
        yum remove | erase package1 [package2] [...] 卸载
        yum provides | whatprovidesfeature1 [feature2] [...]查看指定的特性(可以是某文件)是由哪个程序包所提供：
        yum clean [ packages | metadata | expire-cache | rpmdb| plugins | all ]清理本地缓存：
        yum search string1 [string2] [...] 以指定的关键字搜索程序包名及summary信息
        yum history
        yum history info 6
        yum history undo 6
        yum history redo 6
        包组管理的相关命令：
            yum groupinstall group1 [group2] [...]
            yum groupupdate group1 [group2] [...]
            yum grouplist [hidden] [groupwildcard] [...]
            yum groupremove group1 [group2] [...]
            yum groupinfo group1 [...]
        -y: 自动回答为“yes” 可实现非交互式
创建yum仓库：
    createrepo[options] <directory>     


常用命令:

​	yum  install package.rpm

​	yum remove package

​	yum repolist 

​	yum clean all 

​	yum list |grep exrep

​	yum search  exgrep

