---
title: Linux用户资源配额
layout: page
date: 2015/8/19
updated: 2015/1/22
comments: false
tags: 
- Linux
categories: 
- Linux
---

<!-- toc -->

[TOC]

用户资源系统配额
在系统中，允许系统用户使用存储空间，但是不能限制的让用户随意使用存储空间，对用户存储空间的设定就是用户资源系统配额。
下面以/home 为例，通常如果系统中有一定的用户，会把用户的家目录作为一个单独的分区，从而实现更好的管理。下面具体讲解如何把用户家目录从根系统分区分离出来进行磁盘配额。

# 1.备份数据

拿出一个未使用的分区，把用户家目录的数据拷贝到新的分区

```
fdisk /dev/sdb  --> n（新建分区）-->  p(主分区)--> +10G--> w (保存)
mkfs.ext4  -L  home_dir  /dev/sdb1   格式化新建的分区，卷标名称：home_dir
(如果新建分区没有跟新到内存，partx -a /dev/sdb -->centos6  partprobe -->centos7|5)
mkdir /mnt/home
mount /mnt/home  /dev/sdb1  将分区临时挂载到一个目录，备份home目录中数据
cp -a /home/*  /mnt/home
```

这样就将home家目录中的历史数据备份到新的分区里面了，但要想将之后的数据也自动存储到新建分区的中必须重新挂载，如果直接将新的分区挂载到/home 目录下，存在以下问题。第一、挂载过程中存在用户正在使用家目录的情况，挂载到/home后原来/家目录中的数据时没有用的，而且会自动隐藏掉。所以，安全起见，必须实现通知所有用户，即将要迁移家目录，选一个时间停掉服务，在init 1 模式下实现重新挂载

# 2.单用户模式运行

```
rm -rf /home/*
vim /etc/fstab  -->     /dev/sdb1  /home    ext4 defaults.usrquota,grpquota 0 0
mount -a  将分区重新挂载到家目录下
```

# 3、init 3 重新回到正常模式

```
cd /home  -->   quotacheck -cug  /home  -->可以看到 aquota.group  aquota.user两个数据库
quotaon -p /home   quotaon /home  查看和打开系统配额功能
（如果系统配额没有打开，有可能是selinux功能没有关闭 ）
getenforce 查看selinux   setenforce=0 关闭selinux功能

repquota -a  可以查看所有用户的磁盘配额情况，显示如下
                        Block limits                File limits
User            used    soft    hard  grace    used  soft  hard  grace
----------------------------------------------------------------------
root      --       4       0       0              1     0     0       
huyu      --      56   10240   40960             19     0     0       
superman  --      48       0       0             12     0     0       
supergirl --      48       0       0             12     0     0
user/group ：用户或者组名
userd：用户或者组 已经使用的存储空间，以byte为单位
soft：存储空间使用过多警告提示
hard：空间使用极限，提示如下
sdb1: warning, user block quota exceeded.
sdb1: write failed, user block limit reached.
grace： 宽限期。在这个有效时间内，存储空间数据可以使用修改
后面的分别表示 inode节点的限制，内容和空间相同，只是数值表示限制节点个数

repquota  -u /dir|/dev/sdb1   repquota -g /dir|/dev/sdb1 这两个可以看分区用户和组的配额

修改用户/组 de 存储空间配额
edquota -u/g  huyu  --> 进入修改
edquota  -p huyu  user2   配额复制，user2配额 如同 huyu
setquota username soft hard soft hard  直接命令配置，没有限制，则用0 代替
```

# 4、去掉磁盘配额

```
1、 quotaoff  /dir|/dev/sdb1
2、 rm -rf  aquota.user|group
3、 vim /etc/fstab
```

