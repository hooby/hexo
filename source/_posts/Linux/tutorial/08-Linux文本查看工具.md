---
title: Linux文本查看工具
layout: page
date: 2015/8/1
updated: 2015/6/13
comments: false
tags: 
- Linux
categories: 
- Linux
---

<!-- toc -->

[TOC]

在Linux系统中，一个核心理念就是：一切皆文件。正确的对系统中文本的编辑配置与管理是Linux系统得以稳定运行的一个重要大前提。
因此Linux系统中的文件与管理特别重要，下面简单说一下在对文件处理过程中经常用到的一些工具。
文本查看命令：`cat,tac,rev,more,less`

    cat [option]...[file]...
        -E:显示行结束符$
        -n:显示行号
        -A:显示所有控制符eg：tab键就显示为 ^I
        -b:非空行编号，即空行不显示行号，略过
        -s:压缩连续的空行成一行
        
    tac [option]...[file]...
        -b, --before    在行前而非行尾添加分隔标志【默认换行】
        -r, --regex     将分隔标志视作正则表达式来解析
        -s, --separator=字符串 使用指定字符串代替换行作为分隔标志 -s="#"
    
    rev [options] [file ...]
    reversing the order of characters in every line：将每一行的显示翻转过来
    注意这里tac和rev不同的地方是，tac是将整个文档以行为单位翻转，而rev是以每一个字符为单位，在一行的范围内翻转。ABC-->CBA  而tac 是 ABC 【第一行】 123【第二行】 -->123【第一行】ABC【第二行】
    
    more [options] file [...]  翻到底自动退出
    less [options] file [...]  翻到底不会自动退出（man帮助查看就是用的less用法）
    
    head [OPTION]... [FILE]...
        -c # :指定获取前#个字节
        -n # 或 -# : 指定获取行数
        tail [OPTION]... [FILE]...
        -c # :指定获取前#个字节
        -n # 或 -# : 指定获取行数
        -f ：跟踪显示文件新追加的内容，常用语日志监控
        
    cut [OPTION]... [FILE]...
        -d : 指明分隔符，默认tab  。eg：-d" ",以空格为分割
        -f FILEDS:      #: 第#个字段    #,#[,#]：离散的多个字段，例如1,3,6
                        #-#：连续的多个字段, 例如1-6      混合使用：1-3,7
        -c按字符切割   cut -c 1-10 anaconda-ks.cfg
        --output-delimiter=STRING指定输出分隔符
        
    paste 合并两个文件同行号的列到一行
        paste [OPTION]... [FILE]...
        -d 分隔符:指定分隔符，默认用TAB
        -s : 所有行合成一行显示
        paste f1 f2
        paste -s f1 f2  先合并成一行，在追加。n个文件就显示n行。
    
    wc [OPTION]... [FILE]...
         -c, --bytes    显示  字节数     
         -m, --chars    显示  字符数        
         -l, --lines    显示  行号
         
    sort [OPTION]... [FILE]...   
        -r执行反方向（由上至下）整理
        -n执行按数字大小整理 默认是按字符排序，即1,11,12...19,2,21...
        -f选项忽略（fold）字符串中的字符大小写
        -u选项（独特，unique）删除输出中的重复行    
    
    uniq[OPTION]... [FILE]...
        -c: 显示每行重复出现的次数
        -d: 仅显示重复过的行
        -u: 仅显示不曾重复的行
        【连续且完全相同方为重复】
         
    diff foo.conf-brokenfoo.conf-works   
    比较两个文件的不同