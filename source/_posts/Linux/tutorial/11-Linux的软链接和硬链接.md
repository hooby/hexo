---
title: Linux的inode软链接和硬链接
layout: page
date: 2015/8/5
updated: 2016/11/13
comments: false
tags: 
- Linux
categories: 
- Linux
---

<!-- toc -->

[TOC]

Linux系统中的链接有两种，软连接和硬链接。要充分了解他们最好先了解下inode.

# inode

简单来说就是系统文件记录方式用的是inode,二文件名称只是inode的一个别名,就好像机房有100台主机,每台主机有自己的编号(inode),而有两个主机用来做管理,所以取名叫master1,master2(文件名).

为什么要这么做呢?文件系统是数据在硬盘上的一种排列组织方式，linux每个分区都可以格式化为不同的文件系统，所以可以说每个分区都是一个文件系统，都有自己的目录层次结构。而在目录之下，linux已经将所有的资源处理成为文件，不论是目录还是命令，之后linux会将这些分属不同分区的、单独的文件系统按一定的方式形成一个系统的总的目录层次结构，实现对不同分区的数据进行访问。但是通过目录层次访问磁盘上的文件，只是表面现象。要知道数据在磁盘上以块(block)存储(一个block由8个扇区组成，每个扇区0.5K，扇区是硬盘存储数据的最小单位)数据的大小不一，造成了块的数量也不尽相同，增删改查都会影响数据块的排列方式，那么如何在众多的可能不连续的数据块中，如何精准的找到数据对应的那些块，必须依附在一张索引列表之上，这就是inode。当然，inode中不仅仅存储着指针，还存储着对应资源的权限、属主、属组和大小.

inode也是存储在block上的一段数据，所以其也是占用磁盘空间的，每个分区都有自己的inode上限，所以有时候如果显示磁盘空间已满，inode上限也需要作为一个原因，可使用`df -i`查看

```
[yhu@login ~]$df -i 
Filesystem              Inodes    IUsed      IFree IUse% Mounted on
/dev/sda2            419430400  1008127  418422273    1% /
/dev/sda1              2097152      374    2096778    1% /boot
[yhu@login ~]$df -ih 
Filesystem          Inodes IUsed IFree IUse% Mounted on
/dev/sda2             400M  985K  400M    1% /
/dev/sda1             2.0M   374  2.0M    1% /boot
[yhu@login ~]$df -h 
Filesystem           Size  Used Avail Use% Mounted on
/dev/sda2            400G  144G  257G  36% /
/dev/sda1            2.0G  169M  1.9G   9% /boot
```

 查看文件的inode，使用`ls -i`

```
[yhu@login test_images]$ls -i 
30373532983 Tumor_084_Mask.tif  30373532982 tumor_084.tif
```

注意:

​	同一个分区每一个inode都是唯一的,两个不同分区的文件inode可以相同.

## inode的应用

​    在实际生活中，我们删除数据的时候，只不过是删除掉了指向文件的inode路径，文件依然存在于磁盘之上，这时我们是可以通过一些手段恢复数据的，其本质也不过只是添加了指向文件数据的inode。如果未及时恢复，在原来的磁盘空间上重新写入了数据后，那么数据就很难恢复了。

# 硬链接

一个分区中文件inode唯一,但是可以共享,就是取多个别名,文件刚创建的时候就有一个别名了,创建硬链接就是再创建一个别名,inode一样.

命令` ln SRC_FILE DES_FILE  `

```
[yhu@login test]$ls
inceptionV3.h5  inode  output  p.yaml
[yhu@login test]$ln -v inode inode_test
‘inode_test’ => ‘inode’
[yhu@login test]$ls -i 
  175021784 inceptionV3.h5     20576826 inode     20576826 inode_test  22456345464 output     17028267 p.yaml
```

这里发现创建的硬链接和原有文件使用的是相同的inode.

```
[yhu@login ~]$ll
drwxrwx---   3 yhu  mitosis_detection       4096 Dec  4 11:52 auto
drwxrwx---   2 yhu  mitosis_detection        137 Dec  4 13:31 bin
-rw-rw-r--   1 yhu  mitosis_detection         44 Jan 22 11:40 cc
#在第二列中 的 3 2 1 这个就是描述
```



注意:

​	硬链接不可以跨分区创建.

​	删除原来的文件,通过新的硬链接也同样可以访问到文件内容.

# 软链接

在硬链接之外，还存在一种情况，那就是虽然创建了a文件，但是通过b路径也可以访问到a的内容，尽管a和b的inode不同，但是此时b借助a的路径访问到了a的内容，b就叫做a的软链接。实际上b文件存的是a的路径,找到了a,再通过a访问内容.

命令`ln -s SRC_FILE DES_FILE`

```
[yhu@login test]$ln -sv inode test
‘test’ -> ‘inode’
[yhu@login test]$ls
inceptionV3.h5  inode  inode_test  output  p.yaml  test
[yhu@login test]$ll
lrwxrwxrwx 1 yhu mitosis_detection         5 Mar 18 15:32 test -> inode
```

下面看一个有意思的现象

```
[yhu@login ~]$pwd
/atlas/home/yhu
[yhu@login ~]$ln -sv cc /tmp/new
‘/tmp/new’ -> ‘cc’
[yhu@login ~]$ll /tmp/new
lrwxrwxrwx 1 yhu mitosis_detection 2 Mar 18 15:34 /tmp/new -> cc
[yhu@login ~]$cat /tmp/new   
cat: /tmp/new: No such file or directory  
```

在查看new的时候为什么会报错呢? 因为new这个软连接指向的是new这么目录下的cc,而/tmp/new这个目录下没有cc,因此报错.正确的做法如下:

```
[yhu@login ~]$cd /tmp/            crossbar-starter/ 
[yhu@login tmp]$ln -sv ../atlas/home/yhu/cc new2
‘new2’ -> ‘../atlas/home/yhu/cc’
[yhu@login tmp]$ll new2
lrwxrwxrwx 1 yhu mitosis_detection 20 Mar 18 15:35 new2 -> ../atlas/home/yhu/cc
[yhu@login ~]$cat /tmp/new2
ok
```



注意:

​	软连接可以跨分区创建

​	删除了源文件,软连接就会失效

​	在实际生产生活中，尽量使用相对路径的软链接，来增强代码的可移植性