---
title: dd命令制作U盘启动
layout: page
date: 2015/9/19
updated: 2016/7/13
comments: false
mathjax: true
tags: 
- Linux
categories: 
- Linux
---

<!-- toc -->

[TOC]



```
1、插入U盘，df -h查看U盘文件系统挂载情况，然后使用umount /dev/sdb*卸载U盘文件系统；

2、执行命令：sudo mkfs.vfat -I /dev/sdb格式化U盘为FAT格式；

3、dd if=*/*.iso   of=/dev/sdb  bs=4M  (数据块大小，每个数据块只能存一个文件的数据)

4、执行sync，同步缓存中的数据至U盘；
```

