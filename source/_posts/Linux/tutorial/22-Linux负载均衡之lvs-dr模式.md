---
title: Linux负载均衡之lvs-dr模式
layout: page
date: 2015/10/13
updated: 2016/6/13
comments: false
tags: 
- Linux
categories: 
- Linux
---

<!-- toc -->

[TOC]

```
lvs-dr：
    dr模型中，各主机上均需要配置VIP，解决地址冲突的方式有三种：
        (1) 在前端网关做静态绑定；
        (2) 在各RS使用arptables；
        (3) 在各RS修改内核参数，来限制arp响应和通告的级别；
            限制响应级别：arp_ignore
                0：默认值，表示可使用本地任意接口上配置的任意地址进行响应；
                1: 仅在请求的目标IP配置在本地主机的接收到请求报文接口上时，才给予响应；
            限制通告级别：arp_announce
                0：默认值，把本机上的所有接口的所有信息向每个接口上的网络进行通告；
                1：尽量避免向非直接连接网络进行通告；
                2：必须避免向非本网络通告；
                
            
    
    
    RS的预配置脚本：
        #!/bin/bash
        #
        vip=10.1.0.5
        mask='255.255.255.255'

        case $1 in
        start)
            echo 1 > /proc/sys/net/ipv4/conf/all/arp_ignore
            echo 1 > /proc/sys/net/ipv4/conf/lo/arp_ignore
            echo 2 > /proc/sys/net/ipv4/conf/all/arp_announce
            echo 2 > /proc/sys/net/ipv4/conf/lo/arp_announce

            ifconfig lo:0 $vip netmask $mask broadcast $vip up
            route add -host $vip dev lo:0//当发往本机的请求报文目标地址为 vip 的时候，本机的响应报文从lo:o应答
            ;;
        stop)
            ifconfig lo:0 down

            echo 0 > /proc/sys/net/ipv4/conf/all/arp_ignore
            echo 0 > /proc/sys/net/ipv4/conf/lo/arp_ignore
            echo 0 > /proc/sys/net/ipv4/conf/all/arp_announce
            echo 0 > /proc/sys/net/ipv4/conf/lo/arp_announce

            ;;
        *) 
            echo "Usage $(basename $0) start|stop"
            exit 1
            ;;
        esac                    
    
    VS
    ifconfig eno16777736:0 172.16.0.50 netmask 255.255.255.255 broadcast 172.16.0.50 up
    vip=172.16.0.50
    
    VS的配置脚本：
        #!/bin/bash
        #
        vip='10.1.0.5'
        iface='eno16777736:0'
        mask='255.255.255.255'
        port='80'
        rs1='10.1.0.7'
        rs2='10.1.0.8'
        scheduler='wrr'
        type='-g'

        case $1 in
        start)
            ifconfig $iface $vip netmask $mask broadcast $vip up
            iptables -F
            
            ipvsadm -A -t ${vip}:${port} -s $scheduler
            ipvsadm -a -t ${vip}:${port} -r ${rs1} $type -w 1
            ipvsadm -a -t ${vip}:${port} -r ${rs2} $type -w 1
            ;;
        stop)
            ipvsadm -C
            ifconfig $iface down
            ;;
        *)
            echo "Usage $(basename $0) start|stop"
            exit 1
            ;;
        esac                

    课外扩展作业：vip与dip/rip不在同一网段的实验环境设计及配置实现； 
    
    博客作业：lvs的详细应用
        讲清楚类型、调度方法；并且给出nat和dr类型的设计拓扑及具体实现；
                
```

