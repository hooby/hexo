---
title: Linux系统grep的使用
layout: page
date: 2015/7/28
updated: 2016/6/13
comments: false
tags: 
- Linux
categories: 
- Linux
---

<!-- toc -->

[TOC]

# grep:

Global search REgular expression and Print out the line . 

文本过滤工具，根据用户指定的模式对目标文本进行匹配检查；打印匹配到的行。过滤即不改变原来文本数据。这对于在输出较多信息或者文本内容较多的信息中寻找指定信息很有帮助.

比如想查看文件ifcfg-eth0中配置的网络是多少,你只需要显示有IPADDR这一行:

```
#grep IPADDR ifcfg-eth0 
IPADDR=172.16.10.18
```

这样,就会显示ifcfg-eth0 文件中包含有IPADDR的行.

当然grep还有很多参数可以帮助我们更精确的找到想要的内容,下面列举几个最常用的参数

```
grep [options] PATTERN [file...]
    --color=auto： 对匹配到的文本着色显示
    -v：显示不被pattern匹配到的行
    -i：忽略大小写  #上面的就可以写成 grep -i ipaddr ifcfg-eth0 
    -n：显示匹配的行号
    -c：统计匹配的行数
    -o：仅显示匹配到的字符串
    -q：静默模式，不输出任何模式
    -A #：after，后#行一并显示
    -B #：before，前#行一并显示
    -C #：context，前后#行一并显示
    -e:实现多个选择间的逻辑或关系--> grep -e ‘cat’ -e ‘dog’ file
grep的PATTERN 支持正则表达式，支持正则表达式的还有 vim，less，nginx等
```

grep还可以配合管道命令使用:

```
[root@node7 ~]#lspci | grep -i network 			#lspci的结果直接通过grep过滤显示
81:00.0 Ethernet controller: Intel Corporation I350 Gigabit Network Connection (rev 01)
81:00.1 Ethernet controller: Intel Corporation I350 Gigabit Network Connection (rev 01)
```



# 正则表达式

regexp，由一类特殊字符及文本字符所编写的模式，其中有些字符不表示字符字面意义，而表示控制或通配的功能.比如:

```
grep "ip.ddr" ifcfg-eth0 
#这里的这个点"." 代表匹配任意单个字符,那么匹配到的内容可能是"ipaddr" "ipbddr" "ipcddr" ...  只要i字母后面接的任何单个字母再加上ddr 都能够被命中.这就是通配符的力量
```

正则表达式有基本正则表达式（BRE）和扩展正则表达式（ERE）两种，元字符分类则有四种：字符匹配、匹配次数、位置锚定和分组

## 字符匹配



```
字符匹配：
  . 表示匹配任意单个字符
  [] 表示匹配指定范围内的任意单个字符
  [^] 表示匹配指定范围外的任意单个字符
  [:alnum:] 字母和数字
  [:alpha:] 英文字母
  [:lower:]   [:upper:]  大、小写字母
  [:blank:] （空格和制表符tab）
  [:space:] 垂直和水平的空白字符（比[:blank:]包含的范围广）
  [:cntrl:] 不可打印的控制字符（退格、删除、警铃。。。）
  [:digit:] [:xdigit:] 十进制数字，十六进制数字
  [:graph:] 可打印的非空白字符
  [:print:] 可打印的字符
  [:punct:] 标点符号
```

eg:

```
grep "[:blank:]"  ifcfg-eth0 
#显示文件中有空格或者制表符的行
```



## 匹配次数



```
匹配次数：
  *   匹配前面的字符任意次，包括0次。在贪婪模式下：即尽可能长的匹配
  .*  任意长度的任意字符
  \?  匹配器前面的字符 0或1次
  \+  匹配其前面的字符至少一次
  \{n\}   匹配前面的字符n次
  \{m,n\}匹配其前面的字符至少m次，至多n次
```

eg:

```
grep "[:digit:]\{3\} "  ifcfg-eth0 
#匹配文件中三个数字的行
```



## 位置锚定



```
位置锚定：
  ^   行首锚定
  $   行尾锚定
  ^pattern    用于模式匹配整行    ^$ 空行  ^[[:space:]]*$  空白行
  \<,\b   词尾锚定
  \>,\b   词首锚定
  \<pattern\> 匹配整个单词
```

eg:

```
grep "^$"  ifcfg-eth0 
#匹配文件中的空行
```



## 分组



```
分组：
  \(\)    将一个或多个字符捆绑在一起当做一个整体进行处理，eg：\(root\)\+
  分组括号中的的模式匹配到的内容会被正则表达式引擎记录与内部的变量中，这些变量的命名		方式为：\1,\2,\3,...
  \1  表示从坐骑第一个左括号以及与之匹配的右括号之间的模式所匹配到的字符
  示例：\(string1\+\(string2\)*\)
  \1 ：string1\+\(string2\)*
  \2 ：string2
  后向引用：引用前面的分组括号中的模式所匹配字符，而非模式本省
```

## 扩展正则表达式

扩展正则表达式想要呈现的效果和正则表达式一样,只是只是匹配规则不一样.匹配规则主要是减少了正则中转移用的反斜杠"\"

```
egrep及扩展正则表达式
    egrep = grep -E 
    egrep [options] pattern [file...]
    
扩展正则表达式的元字符：
    字符匹配：
        . 
        [] 
        [^] 
    次数匹配：
        *   
        ?   
        +   
        {m}
        {m,n\}
    位置锚定：
        ^   
        $   
        \<,\>,\b    
    分组：
        ()
    后向引用：\1,\2...
        或者：
        a|b : a 或b
        C|cat :C 或者cat
        (C|c)at: Cat或者cat

```

