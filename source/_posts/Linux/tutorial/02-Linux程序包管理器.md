---
title: Linux程序包管理器
layout: page
date: 2015/7/15
updated: 2015/9/13
comments: false
tags: 
- Linux
categories: 
- Linux
---

<!-- toc -->

[TOC]

源代码命名方式，name-version.tar.gz|bz2|xz,    -->version:major.minor.release
要将一个源代码程序安装到Linux系统上，一般有两个方法。

    1.找到源代码，手动编译安装。解压，./configure  --> make --> make install
    2.使用软件包管理器（程序包管理器）
    3.直接解压做好的二进制包后使用

# 1.程序包管理器：

将编译好的应用程序文件打包成一个或者几个，从而方便快捷的实现程序包的安装，卸载，查询，升级和校验等管理操作。

    Linux系统不同版本有不同的程序包管理器。目前主要分两类
    1、debian：deb文件，dpkg包管理器
    2、RedHat：rpm文件，rpm包管理器，RPM：RedHat package manager（原名），现在已是 RPM package manager 递归缩写

# 2.RPM包命名方式：

    name-version-release.arch.rpm   -->version:major.minor.release  -->release:release.os
    eg:bash-4.2.46-19.el7.x86_64.rpm
    常见arch：x86，x86_64，PowerPC 或者noarch（）
# 3.RPM包的分类和拆包

    Application-VERSION-ARCH.rpm: 主包
    Application-devel-VERSION-ARCH.rpm开发子包
    Application-utils-VERSION-ARHC.rpm其它子包
    Application-libs-VERSION-ARHC.rpm其它子包
    包与包之间可能存在依赖关系，甚至循环依赖。解决方法：yum（RedHat，centos）
# 4.RPM包文件组成

    a、RPM包内的文件。 b、RPM包的元数据，如名称，版本，依赖性，描述等。 c、安装或者卸载时运行的脚本。
# 5.获取程序包的途径方法：

    1、系统发版的光盘或者官方服务器；
        CentOS镜像
        http:www.centos.org/download/
        http://mirror.aliyun.com（阿里云） 搜狐 163等
    2、项目官方站点
    3、第三方组织
        Fedora-EPEL：
        Extra Packages for Enterprise Linux
        Rpmforge:RHEL推荐，包很全
        搜索引擎：
        http://pkgs.org
        http://rpmfind.net
        http://rpm.pbone.net
        https://sourceforge.net/
    4、自己制作
        注意：非官方下载的程序包要检查其合法性：来源的合法以及程序包的完整性

# 6.库文件

    程序的运行需要依赖库文件 ldd /path/to/binary_file 可以查看二进制程序所依赖的库文件
    库文件的配置文件：/etc/ld.so.conf /etc/ld.so.conf.d/*.conf   即ld.so.conf 文件和 /etc/ld.so.conf.d/ 目录下所有以 .conf结尾的文件
