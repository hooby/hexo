---
title: Linux sed
layout: page
date: 2015/9/13
updated: 2016/6/13
comments: false
tags: 
- Linux
categories: 
- Linux
---

<!-- toc -->

[TOC]

# sed : Stream EDitor 



sed是一种流编辑器，它一次处理一行内容。处理时，把当前处理的行存储在临时缓冲区中，称为“模式空间”（pattern space），接着用sed命令处理缓冲区中的内容，处理完成后，把缓冲区的内容送往屏幕。接着处理下一行，这样不断重复，直到文件末尾。文件内容并没有改变，除非你使用重定向存储输出。Sed主要用来自动编辑一个或多个文件,简化对文件的反复操作,编写转换程序等

## 用法



```
sed  [option]... 'script' inputfile...
常用选项：
    -n：不输出模式空间内容到屏幕，即不自动打印,默认在输出前会打印源内容
    -e: 多点编辑
    -f：/PATH/SCRIPT_FILE: 从指定文件中读取编辑脚本
    -r: 支持使用扩展正则表达式
    -i: 原处编辑
script:
    '地址命令'= [地址定界] + 编辑命令 + [替换标记]
        地址定界：
            (1) 不给地址：对全文进行处理
            (2) 单地址：
                #: 指定的行
                /pattern/：被此处模式所能够匹配到的每一行
            (3) 地址范围：
                #,#
                #,+#
                /pat1/,/pat2/
                #,/pat1/
            (4) ~：步进
                1~2 奇数行
                2~2 偶数行
                
        编辑命令：
            d: 删除模式空间匹配的行
            p: 显示模式空间中的内容
            a [\]text：在指定行后面追加文本，支持使用\n实现多行追加
            i[\]text：在行前面插入文本
            c [\]text：替换行为单行或多行文本
            w /path/somefile: 保存模式匹配的行至指定文件
            r /path/somefile：读取指定文件的文本至模式空间中匹配到的行后
            =: 为模式空间中的行打印行号
            !:模式空间中匹配行取反处理
            s///：查找替换,支持使用其它分隔符，s@@@，s###
            
    	替换标记：
            g: 行内全局替换
            p: 显示替换成功的行
            w /PATH/TO/SOMEFILE：将替换成功的行保存至文件中
```

## 实例



例子来啦!没有例子呢,的确是很难懂的.那么下面就尽量给多一点例子,来详细分析上面的意思.

```
#cat base.repo
[development]
name=development
baseurl=ftp://172.16.10.10/centos7.2
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7

[epel]
name=epel
baseurl=ftp://172.16.10.10/epel
gpgcheck=0
```

首先以上面这个` base.repo`文件为例.

1.打印文件中的第二行 

```
#sed '2p' base.repo 
[development]
name=development
name=development    #这里是新打印的地方
baseurl=ftp://172.16.10.10/centos7.2
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7

[epel]
name=epel
baseurl=ftp://172.16.10.10/epel
gpgcheck=0
#分析:首先sed默认会打印源内容,其次sed是行编辑器,所以会一行一行的处理,于是就有了到第二行打印,输出的第三行就是新打印的地方.在地址命令中:'2'对应的是'地址定界','p'呢就是'编辑命令'
```

2.只显示文件中第二行

```
#sed -n '2p' base.repo
name=development
# -n 选项就不用打印源文件了
```

3.只显示文件中第1-5行

```
#sed -n '1,5p' base.repo 
[development]
name=development
baseurl=ftp://172.16.10.10/centos7.2
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
# '1,5' '地址界定'中的'地址范围'
```

5.只显示文件中包含`baseurl`的行

```
#sed -n '/baseurl/p' base.repo 
baseurl=ftp://172.16.10.10/centos7.2
baseurl=ftp://172.16.10.10/epel
#这里就用到了'地址定界'中的模式匹配
```

6.从第5行开始显示到包含`baseurl`的行

```
#sed -n '5,/baseurl/p' base.repo 
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7

[epel]
name=epel
baseurl=ftp://172.16.10.10/epel
#从指定行(5)开始显示,到第一次遇到baseurl停止
可以验证:sed -n '1,/baseurl/p' base.repo 看看效果
```

7.a显示空行行号,b显示空行并显示行号

```
#sed -n '/^$/=' base.repo 
6
#sed -n -e '/^$/p' -e '/^$/=' base.repo    

6
```

8.在包含baseurl的行 a行前增加superman  b行后增加superman  c替换掉baseurl为superman

```
-------------------------------------
#sed  '/baseurl/i\superman' base.repo
[development]
name=development
superman
baseurl=ftp://172.16.10.10/centos7.2
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7

[epel]
name=epel
superman
baseurl=ftp://172.16.10.10/epel
gpgcheck=0
-------------------------------------
#sed  '/baseurl/a\superman' base.repo 
[development]
name=development
baseurl=ftp://172.16.10.10/centos7.2
superman
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7

[epel]
name=epel
baseurl=ftp://172.16.10.10/epel
superman
gpgcheck=0
-------------------------------------
#sed  '/baseurl/c\superman' base.repo 
[development]
name=development
superman
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7

[epel]
name=epel
superman
gpgcheck=0
```

9.将文件中的`baseurl`全部替换成`BASEURL`

```
#sed  's/baseurl/BASEURL/g' base.repo
[development]
name=development
BASEURL=ftp://172.16.10.10/centos7.2
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7

[epel]
name=epel
BASEURL=ftp://172.16.10.10/epel
gpgcheck=0
#注意这里源文件并没有整的替换,只是把打印出来的替换掉了,如果真的要替换记得加一个 -i 参数
```

10.删除centos7系统/etc/grub2.cfg文件中所有以空白开头的行行首的空白字符

```
#sed 's/^[ \t]\+//g' grub2.cfg
#这里空白包括制表符和空白,删除的话就直接替换成空就可以了
```

11.删除/etc/fstab文件中所有以#开头，后面至少跟一个空白字符的行的行首的#和空白字符

```
sed 's/^# \+//g' fstab
```

其他练习:

```
在centos6系统/root/install.log每一行行首增加#号
在/etc/fstab文件中不以#开头的行的行首增加#号
处理/etc/fstab路径,使用sed命令取出其目录名和基名
利用sed 取出ifconfig命令中本机的IPv4地址
统计centos安装光盘中Package目录下的所有rpm文件的以.分隔倒数第二个字段的重复次数
统计/etc/init.d/functions文件中每个单词的出现次数，并排序（用grep和sed两种方法分别实现）
将文本文件的n和n+1行合并为一行，n为奇数行
```

