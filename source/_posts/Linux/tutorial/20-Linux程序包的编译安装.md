---
title: Linux程序包的编译安装
layout: page
date: 2015/9/20
updated: 2016/6/13
comments: false
tags: 
- Linux
categories: 
- Linux
---

<!-- toc -->

[TOC]

在有些源代码程序没有被编译成rpm的时候，或者其他人写了一个源代码程序，要把它安装在服务器上要怎么做呢？
那就需要对源代码进行编译安装了。
C代码编译安装三步骤：

    1、./configure：
        (1)通过选项传递参数，指定启用特性、安装路径等；执行时会参考用户的指定以及makefile.in文件生成makefile
        (2) 检查依赖到的外部环境，如依赖的软件包
    2、make：根据makefile文件，构建应用程序
    3、make install:复制文件到相应路径
开发工具：

    autoconf: 生成configure脚本
    automake：生成Makefile.in
    注意：安装前查看INSTALL，README
例子:

```
下面就以编译安装HTTP服务为例进行说明：
首先要 获取到HTTP源代码，此处通过FTP获取
yum install lftp  安装FTP--源代码获取工具
ftp://172.16.0.1/pub/Sources/sources/httpd/httpd-2.2.29.tar.bz2 --下载源代码
tar xvf httpd-2.2.29.tar.bz2  --解压源代码--> 得到http-2.2.29 目录
由于要用到autoconf和automake工具 因此可以安装 工具包
yum groupinstall "Development Tools"
安装工具安装好了之后进入http目录执行 ./configure命令，注意这个命令要指定该软件安装的程序所在目录，以及配置文件所在目录。所以执行./configure命令之前要先看一下目录下的INSTALL、README两个文件
http服务中--prefix=程序安装目录  --sysconfdir=配置文件目录 如果不加这两个选项，是有默认安装路径的。在man帮助中有，接下来执行：
./configure  --prefix=/usr/local/huyuhttp  --sysconfdir=/etc/huyuhttp/
make 和 make install
这样呢其实已经安装完了http服务，但是此时http还没有启动，帮助文档还不能方便查看，调用的库也不行，共享库有哪些也不知道，所以要进行接下来的配置。
1、在/etc/profile.d/目录下创建一个以.sh结尾的文件，修改path变量，加入http二进制程序路径，这样就可以不用加路径直接执行http程序了。
vim  /etc/profile.d/huyuhttp.sh  
PATH=$PATH:/usr/local/huyuhttp/bin
.   /etc/profile.d/huyuhttp.sh  刷新path变量值
2、添加man手册，可以方便查看帮助
vim /etc/man_db.conf   /usr/local/huyuhttp/man 
3、添加lib图文件目录
vim /etc/ld.so.conf.d/huyuhttp.conf     /usr/local/huyuhttp/lib
    ldconfig 刷新库文件缓存
4、添加头文件，这里只需要在include的目录中加入http的include目录链接即可
ln -s /usr/local/huyuhttp/include/ /usr/include/huyuhttp
5、关闭防火墙
    iptables -F
6、开启http
apachectl start
7、查看端口是否打开
netstat -ant 查看80
8、测试查看网页
links ip
curl ip
```

