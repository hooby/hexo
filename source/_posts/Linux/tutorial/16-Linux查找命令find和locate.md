---
title: Linux查找命令find和locate
layout: page
date: 2015/8/24
updated: 2016/6/13
comments: false
tags: 
- Linux
categories: 
- Linux
---

<!-- toc -->

[TOC]

有些时候我们是想要在系统中查找某个具体的文件，却不知道路径在哪里，只是知道其中的某些特性，比如大小或者名字什么的。这时候就要用到查找工具啦。
在文件系统上查找符合条件的文件命令有两个，locate和find，其中locate是非实时查找即数据库查找。而find是实时查找

locate：
    用法：locate [OPTION]... [PATTERN]...
    [option]
        -i  不区分大小写
        -n # 只列举前N个匹配项目
        -r 使用扩展正则表达式 查询 
        locate foo 搜索名称或路径中带有foo的文件
        locate -r '\.foo$' 使用扩展正则表达式来搜索以 ".foo" 结尾的文件
        
    查询系统上的文件索引数据库
        /var/lib/mlocate/mlocate.db 
    依赖于实现构建的索引，
        索引的构建是在系统较为空闲时自动进行的（周期性任务），也可管理员手动更新数据库（updatedb）
    索引构建过程需要遍历整个根文件系统，极消耗资源
    工作特点：
        查找速度快（只查找数据库中有的数据）
        模糊查找
        非实时查找（在索引构建之后创建的文件时不能找到的）
        搜索的是文件的全路径，不仅仅是文件名
        可能只搜索用户具备读取和执行权限的目录

find：
    find [-H] [-L] [-P] [-Olevel] [-D help|tree|search|stat|rates|opt|exec] [path...] [expression]
    即：find [option] [查找路径] [查找条件] [处理动作] 
    查找路径：指定具体目标路径，【默认为当前目录】
    查找条件：指定的查找标准，可以文件名、大小、类型、权限等。默认找出指定路径下的所有文件
    处理动作：对符合条件的文件做操作，默认输出至屏幕
    【查找条件】
        根据文件名和inode查找：
            -name “文件名称”： 支持使用glob （文件名通配符） *，？，[],[^]
            -iname “文件名称” ： 不区分字母大小写
            inum n  按inode号查找
            -samefile name 相同inode号的文件
            -links n 链接数为n的文件
            -regex “PATTERN”：以pattern匹配整个文件路径字符串，而不仅仅是文件名称
        根据属主属组查找：
            -user username：
            -group groupname：
            -uid userid：
            -gid groupid：
            -nouser：查找没有属主的文件
            -nogroup：查找没有属组的文件
        根据文件类型查找：
            -type type：  f,d,l,s,b,c,p 
        根据组合条件查找：
            -a  -o  -not=!
        根据文件大小来查找：
            -size [+|-]#UNIT   (k,M,G)
            #UNIT (#-1,#] --> 如6k 即（5k,6k] 5k到6k 但不包括5k
            -#UNIT:[0,#-1] -->如6k 即[0,5k]
            +#UNIT:(#,∞)  大于# 但不包括#
        根据时间戳查找：
            以 【天】为单位
                -atime[+|-]#  ：  [0,#) [#,#+1) [#+1,∞]
                -mtime[+|-]#  ：
                -ctime[+|-]#  ：
            以【分钟】为单位
                -amin[+|-]#  ：
                -mmin[+|-]#  ：
                -cmin[+|-]#  ：
        根据权限查找：
            -perm [/|-] mode
            mode : 精确匹配，不能多不能少
            /mode 或+mode ： 或关系 ugo中 只要有一个匹配即可
            -mode : ugo必须至少拥有或比mode更多权限
    【处理动作】
    -print：默认处理动作
    -ls：类似于对查找到的文件执行 ls -l
    -delete:删除查找到的文件
    -fls file：查找到的所有文件的长格式信息保存至指定文件中
    -ok commond {} \; 对查找到的每个文件执行有commond指定的命令，每个文件执行前都要交互式确认
    -exec commond {} \; 对查找到的每个文件执行有commond指定的命令
        {}：用于引用查找到的文件名称自身
    例：
        find -name “*.conf” -exec cp {} {}.orig \;
        •备份配置文件，添加.orig这个扩展名
        find /tmp -ctime +3 -user joe -ok rm {} \;
        •提示删除存在时间超过３天以上的joe的临时文件
        find ~ -perm -002 -exec chmod o-w {} \ ;
        •在你的主目录中寻找可被其它用户写入的文件
        find /data –type f -perm 644 -name “*.sh” –exec chmod 755 {} \;
        •在 /data 目录中 找出所有 以.sh 结尾没有执行全选的文件 增加执行权限
        find /home -type d -ls 
        找出所有/home 目录下的 目录 列出详细信息

```
find /var -u root -g mail 
find /var not \(-user root -o -user lp -o -user gdm \)
find /var -ctime -7 -not \( -user root -o -user postfix \)
find / -nouser -nogroup -atime -7 
find /etc -size +1M -type f 
find /etc -not \( -perm +222 \)
find /etc -not \( -perm -222 \)
find /etc/init.d -perm /113 
find /etc -path '/etc/sane.d' -a -prune -o -name "*.conf" -print
find /etc -path '/etc/front*' -prune -o -path '/etc/hu*' -prune -o -path '/etc/lib*' -prune -o -path '/etc/se*' -prune -o -path '/etc/init' -prune -o -name "*.conf" -print
```