---
title: PXE配置文档
layout: page
date: 2015/10/13
updated: 2016/6/13
comments: false
tags: 
- Linux
categories: 
- Linux
---

<!-- toc -->

[TOC]

目前所做为测试，即在虚拟机上的测试。先简单介绍一下整体环境，两台centos7.2系统，7-2-a（安装vsftp  tftp）和7-2-b（安装dhcp服务）

在7-2-a   ip:192.168.199.149 的操作：

```
systemctl stop firewalld 
setenforce 0
yum install -y vsftpd tftp-server syslinux   
cp -a /misc/cd/*  /var/ftp
cp /root/anaconda-ks.cfg  /var/ftp/ks.cfg
```

//这里ks.cfg需要根据自己需求简单修改配置，后面会说明用system-config-kickstart 如何配置ks.cfg

内容修改如下

```
#platform=x86, AMD64, or Intel EM64T
#version=DEVEL
# Install OS instead of upgrade
install
# Keyboard layouts
keyboard 'us'
# Root password
rootpw --iscrypted $1$gfl.Vz5t$mqzEhPMYDiANGwDB8gj/E/
# System timezone
timezone Asia/Shanghai
user --name=superman --password=$6$cq8y5jAZQCGd4muD$8G7zz.fpZFK3rI0AA7dyAxeD2B3mIjgu1HY/yeOm.PdsYHAYsD77K8uXTjbEPvJs9z7M6s0TECr6FtL2Bc5to/ --iscrypted --gecos="superman"   #为服务器添加一个用户
# Use network installation
url --url="ftp://192.168.199.149"   
# System language
lang en_US
# Firewall configuration
firewall --disabled
# System authorization information
auth  --useshadow  --passalgo=sha512
# Use text mode install
text
firstboot --enable
# SELinux configuration
selinux --disabled

# Network information
network  --bootproto=dhcp --device=eno16777736 --ipv6=auto --activate
network  --hostname=7-2-test

# Reboot after installation
reboot
# System bootloader configuration
bootloader --location=mbr --boot-drive=sda
# Partition clearing information
clearpart --all --initlabel
# Disk partitioning information
part / --fstype="xfs" --size=12040
part /boot --fstype="xfs" --size=10240
part /home --fstype="xfs" --size=10240
part /var --fstype="xfs" --size=20480
part /usr --fstype="xfs" --size=10240
part /tmp --fstype="xfs" --size=10240

%packages
@^minimal
@core
@ftp-server

%end
```

配置tftp-server

```
cp /usr/share/syslinux/pxelinux.0 /var/lib/tftpboot
mkdir /var/lib/tftpboot/pxelinux.cfg
cp /misc/cd/isolinux/isolinux.cfg  /var/lib/tftpboot/pxelinux.cfg/default
cp /misc/cd/images/pxeboot/{vmlinuz,initrd.img}  /var/lib/tftpboot/
cp /misc/cd/isolinux/{vesamenu.c32,boot.msg,splash.png} /var/lib/tftpboot/
vim /var/lib/tftpboot/pxelinux.cfg/default


```

default中 很多不需要修改，需要修改的部分如下

```
label linux
  menu label ^Install CentOS 7 minimal
  menu default   
  kernel vmlinuz
  append initrd=initrd.img inst.stage2=ftp://192.168.199.149/ inst.ks=ftp://192.168.199.149/ks.cfg

```

label可以有多个，但是只能有一个label是menu default

启动服务

```
systemctl start vsftpd
systemctl start tftp.socket 
systemctl start tftp.service  #如果是开启了守护进程，则需要启动xinetd
ss -tunlp   #通过端口简单查看服务是否启动

```

在7-2-b   ip:192.168.199.158 的操作：

```
yum install -y dhcp
vim /etc/dhcp/dhcpd.conf
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp*/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
option domain-name-servers 192.168.199.1;

default-lease-time 60000;
max-lease-time 720000;

log-facility local7;

subnet 192.168.199.0 netmask 255.255.255.0 {
  range 192.168.199.230 192.168.199.249;
  option routers 192.168.199.1;
  next-server 192.168.199.149;#这里的ip就是tftpd所在ip
filename "pxelinux.0";
}

systemctl start dhcpd 

```

kickstart 生成ks.cfg

```
yum install -y system-config-kickstart.noarch

system-config-kickstart  /var/ftp/ks.cfg    #这个代表在原有基础上进行修改
system-config-kickstart  #这个代表从头自己创建
运行之后按照如下配置即可
```

![kickstart1.png](PXE配置文档/kickstart1.png)

perform isntallation in text mode 代表使用 文本（非图形化）模式，默认为图形化模式  安装系统

![kickstart2.png](PXE配置文档/kickstart2.png)

获取安装包可以有多种方式这里使用ftp，ftp server 对应自己的ftp服务地址，directory对应在ftp下的哪个目录

![kickstart3.png](PXE配置文档/kickstart4.png)

磁盘分区可以根据需求修改，配置好了之后也可以在ks.cfg上进行再次修改

![kickstart5.png](PXE配置文档/kickstart5.png)

![kickstart7.png](PXE配置文档/kickstart8.png)

这里千万注意，为什么这里没有。这是centos 7 上的一个bug，在这里必须把yum原名称修改为development才会出现，（如果是有什么其他原因的，明白为什么的，麻烦告诉我，我只知道怎么改可以避免这个问题）

![development.png](PXE配置文档/development.png)

![kickstart9.png](PXE配置文档/kickstart9.png)

所有一切配置好了之后，就可以打开新机 看看能否自动安装系统了。

常见问题总结：

![tftp问题.png](PXE配置文档/tftp_problem.png)

![ftp问题.png](PXE配置文档/ftp_problem.png)

出现这个问题代表是tftp服务出错，获取到了ip（dhcp工作正常），第一个没有连接上tftp，第二个没有没有获取到pxelinux.0。可以从tftp入手差错。

![停留.png](PXE配置文档/pause.png)

这里出现一个问题，ks.cfg吧所需要的都配好了，但是走到这里就停下来了。下面显示we won't touch your disk until you click begin installation  但是begin installation却是灰色的，不能选中。

在图片中随便点开一个配置图片，不用修改任何东西，点出来就可以继续往下走了，但是这样就需要人工干预了，没有实现自动化安装。目前是由于什么原因导致的尚不知晓，后续如果有找到原因再来补充。

最后我在ks.cfg部分把运行模式改成了text，同时增加和删减了部分参数之后就可以了。

![](PXE配置文档/menu.png)

![ok.png](PXE配置文档/ok.png)