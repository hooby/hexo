---
title: ubuntu18 命令行设置wireless 连接WiFi热点
layout: page
date: 2019/4/25
updated: 2019/7/13
comments: false
tags:
  - wireless
categories:
  - Linux
---

<!-- toc -->

[TOC]

## nmcli



查看可用WiFi

```
$ nmcli dev wifi 
IN-USE  SSID           MODE   CHAN  RATE        SIGNAL  BARS  SECURITY  
        HiWiFi_5489B6  Infra  3     130 Mbit/s  70      ▂▄▆_  WPA1 WPA2 
        PING           Infra  11    195 Mbit/s  54      ▂▄__  WPA2      
        H3C            Infra  1     270 Mbit/s  44      ▂▄__  WPA1 WPA2 
```





## rfkill



```
# rfkill list 
0: bluedroid_pm: Bluetooth
	Soft blocked: yes
	Hard blocked: no
1: phy0: Wireless LAN
	Soft blocked: no
	Hard blocked: no
2: brcmfmac-wifi: Wireless LAN
	Soft blocked: no
	Hard blocked: no
	
# rfkill block 1
# rfkill block 2
# rfkill list
0: bluedroid_pm: Bluetooth
	Soft blocked: yes
	Hard blocked: no
1: phy0: Wireless LAN
	Soft blocked: yes
	Hard blocked: no
2: brcmfmac-wifi: Wireless LAN
	Soft blocked: yes
	Hard blocked: no

# rfkill unblock 1
# rfkill unblock 2





```





## iw



```
.扫描可用的WiFi

# 不加less可能会产生太多输出
iw dev wlan0 scan |less

# 或者
iwlist wlan0 scanning
```





## ifconfig



```
# ifconfig wlan0 
wlan0: flags=4098<BROADCAST,MULTICAST>  mtu 1500
        ether 00:04:4b:8c:55:d4  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 70  bytes 7620 (7.6 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        
# ifconfig wlan0 down
ip link set wlan0 down

# ifconfig wlan0 up
# ip link set wlan0 up

```





wpa

```
wpa_passphrase HiWiFi_5489B6 Alex.sg-ai.c0m > wpa_passphrase.conf
wpa_supplicant -i wlan0 -c ./wpa_passphrase.conf



wpa_passphrase HiWiFi_5489B6 Alex.sg-ai.c0m > wpa_passphrase.conf
# cat wpa_passphrase.conf 
network={
	ssid="HiWiFi_5489B6"
	#psk="Alex.sg-ai.c0m"
	psk=a4975b898770daca9cdb38b5167b216561e83cc00460edb3ef2ce5a9ee1f77ce
}

wpa_supplicant -Dnl80211 -iwlan0 -cwpa_passphrase.conf -B
dhclient wlan0
killall wpa_supplicant
```







stop networkd-dispatcher

```
$ systemctl stop networkd-dispatcher
$ systemctl disable networkd-dispatcher
$ systemctl mask networkd-dispatcher
$ apt purge nplan netplan.io

apt install nplan
systemctl unmask networkd-dispatcher
systemctl start network-dispatcher
systemctl enable network-dispatcher
```

