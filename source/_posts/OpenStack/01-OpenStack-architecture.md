---
title: OpenStack架构简介
layout: page
date: 2017/11/27
updated: 2018/8/1
comments: false
tags: 
- OpenStack
categories: 
- OpenStack
toc: true
---

<!-- toc -->

[TOC]



# 1.云计算

```
 云计算是一种按使用量付费的模式，这种模式提供可用的、便捷的、按需的网络访问， 进入可配置的计算资源共享池（资源包括网络，服务器，存储，应用软件，服务），这些资源能够被快速提供，只需投入很少的管理工作，或与服务供应商进行很少的交互。


```

# 2.IT系统架构的发展到目前为止大致可以分为3个阶段：

## 物理机架构

这一阶段，应用部署和运行在物理机上。 
比如企业要上一个ERP系统，如果规模不大，可以找3台物理机，分别部署Web服务器、应用服务器和数据库服务器。 
如果规模大一点，各种服务器可以采用集群架构，但每个集群成员也还是直接部署在物理机上。 
我见过的客户早期都是这种架构，一套应用一套服务器，通常系统的资源使用率都很低，达到20%的都是好的。

虚拟化架构 
摩尔定律决定了物理服务器的计算能力越来越强，虚拟化技术的发展大大提高了物理服务器的资源使用率。 
这个阶段，物理机上运行若干虚拟机，应用系统直接部署到虚拟机上。 
虚拟化的好处还体现在减少了需要管理的物理机数量，同时节省了维护成本。

## 云计算架构

虚拟化提高了单台物理机的资源使用率，随着虚拟化技术的应用，IT环境中有越来越多的虚拟机，这时新的需求产生了： 
如何对IT环境中的虚拟机进行统一和高效的管理。 
有需求就有供给，云计算登上了历史舞台。

云平台是一个面向服务的架构，按照提供服务的不同分为 IaaS、PaaS 和 SaaS。 
请看下图

![img](01-OpenStack-architecture/1349539-20180707163310096-320236091.png)

IaaS（Infrastructure as a Service）提供的服务是虚拟机。 
IaaS 负责管理虚机的生命周期，包括创建、修改、备份、启停、销毁等。 
使用者从云平台得到的是一个已经安装好镜像（操作系统+其他预装软件）的虚拟机。 
使用者需要关心虚机的类型（OS）和配置（CPU、内存、磁盘），并且自己负责部署上层的中间件和应用。 
IaaS 的使用者通常是数据中心的系统管理员。 
典型的 IaaS 例子有 AWS、Rackspace、阿里云等

PaaS（Platform as a Service）提供的服务是应用的运行环境和一系列中间件服务（比如数据库、消息队列等）。 
使用者只需专注应用的开发，并将自己的应用和数据部署到PaaS环境中。 
PaaS负责保证这些服务的可用性和性能。 
PaaS的使用者通常是应用的开发人员。 
典型的 PaaS 有 Google App Engine、IBM BlueMix 等

SaaS（Software as a Service）提供的是应用服务。 
使用者只需要登录并使用应用，无需关心应用使用什么技术实现，也不需要关系应用部署在哪里。 
SaaS的使用者通常是应用的最终用户。 
典型的 SaaS 有 Google Gmail、Salesforce 等

# 3.OpenStack

OpenStack 对数据中心的计算、存储和网络资源进行统一管理。 
由此可见，OpenStack 针对的是 IT 基础设施，是 IaaS 这个层次的云操作系统。





## 1.OpenStack 架构

![img](01-OpenStack-architecture/1349539-20180707163943300-248453178.png)

中间菱形是虚拟机，围绕 VM 的那些长方形代表 OpenStack 不同的模块（OpenStack 叫服务，后面都用服务这个术语），下面来分别介绍。

Nova：管理 VM 的生命周期，是 OpenStack 中最核心的服务。

Neutron：为 OpenStack 提供网络连接服务，负责创建和管理L2、L3 网络，为 VM 提供虚拟网络和物理网络连接。

Glance：管理 VM 的启动镜像，Nova 创建 VM 时将使用 Glance 提供的镜像。

Cinder：为 VM 提供块存储服务。Cinder 提供的每一个 Volume 在 VM 看来就是一块虚拟硬盘，一般用作数据盘。

Swift：提供对象存储服务。VM 可以通过 RESTful API 存放对象数据。作为可选的方案，Glance 可以将镜像存放在 Swift 中；Cinder 也可以将 Volume 备份到 Swift 中。

Keystone：为 OpenStack 的各种服务提供认证和权限管理服务。简单的说，OpenStack 上的每一个操作都必须通过 Keystone 的审核。

Ceilometer：提供 OpenStac k监控和计量服务，为报警、统计或计费提供数据。

Horizon：为 OpenStack 用户提供一个 Web 的自服务 Portal。

## 2.OpenStack的核心服务

Nova 管理计算资源，是核心服务。
Neutron 管理网络资源，是核心服务。
Glance 为 VM 提供 OS 镜像，属于存储范畴，是核心服务。
Cinder 提供块存储，VM怎么也得需要数据盘吧，是核心服务。
Swift 提供对象存储，不是必须的，是可选服务。
Keystone 认证服务，没它 OpenStack 转不起来，是核心服务。
Ceilometer 监控服务，不是必须的，可选服务。
Horizon 大家都需要一个操作界面吧。

![img](01-OpenStack-architecture/1349539-20180707164005886-1559258316.png)

在 Logical Architecture 中，可以看到每个服务又由若干组件组成。 
以 Neutron 为例，包含

![img](01-OpenStack-architecture/1349539-20180707164020384-388481089.png)

Neutron Server、Neutron plugins 和 Neutron agents
Network provider
消息队列 Queue
数据库 Neutron Database

上面是 Logical Architecture，描述的是 Neutron 服务各个组成部分以及各组件之间的逻辑关系。 
而在实际的部署方案上，各个组件可以部署到不同的物理节点上。

OpenStack 本身是一个分布式系统，不但各个服务可以分布部署，服务中的组件也可以分布部署。 
这种分布式特性让 OpenStack 具备极大的灵活性、伸缩性和高可用性。