---
title: mask rcnn 数据预处理解读
layout: page
date: 2018/8/13
updated: 2018/6/13
comments: false
tags: 
- mask rcnn
categories: 
- deep learning
toc: true
---

<!-- toc -->

[TOC]





```python
import os
import sys
import json
import skimage.draw
import skimage.io
import skimage.color
from imgaug import augmenters as iaa
import numpy as np
import random
import matplotlib.pyplot as plt
# plt.switch_backend('agg')

# Root directory of the project
ROOT_DIR = os.path.abspath("../../")
sys.path.append(ROOT_DIR)

# Import Mask RCNN
from mrcnn import visualize
from mrcnn.config import Config
from mrcnn import model as modellib, utils
from mrcnn.model import log
from samples.cells import run_via_json

COCO_MODEL_PATH = os.path.join(ROOT_DIR, "mask_rcnn_coco.h5")
DEFAULT_LOGS_DIR = os.path.join(ROOT_DIR, "logs")
class_names = ['BG', 'cells1', 'cells2']
train_class_name = ['cells1', 'cells2']
show_class_name = ['cells2']

train_config = run_via_json.TrainConfig()
config = train_config
dataset = run_via_json.CellDataset()
dataset.load_data(train_config.dataset_dir, train_config.train_dir_name, class_names,
                  annotation=train_config.train_annotation_json)
dataset.prepare()
```

