---
title: Deep Learning Video Classification and Captioning
layout: page
date: 2018/7/7
updated: 2018/7/17
comments: false
tags:
- Video Classification
- Video Captioning
categories: 
- DL 
toc: true
---

<!-- toc -->

[TOC]



# 基本术语



1. **视频分类**（Video Classification）：将基于视频的语义内容如人类行为和复杂事件等，将视频片段自动分类至单个或多个类别，视频分类的研究内容主要包括多标签的通用视频分类和人类行为识别等。
2. **视频描述生成**（Video Captioning）：试图基于视频分类的标签，形成完整的自然语句，为视频生成包含最多动态信息的描述说明。
3. 词袋模型（Bag of Words）
4. 3D CNN
5. Two-stream CNN
6. TSN模型
7. LSTM
8. GRU-RNN
9. 



# 研究方法

**一、** **传统视频分类方法研究**

在深度学习方法广泛应用之前，大多数的视频分类方法采用基于人工设计的特征和典型的机器学习方法研究行为识别和事件检测。

传统的视频分类研究专注于采用对局部时空区域的运动信息和表观（Appearance）信息编码的方式获取视频描述符，然后利用词袋模型（Bag of Words）等方式生成视频编码，最后利用视频编码来训练分类器（如SVM），区分视频类别。视频的描述符依赖人工设计的特征，如使用运动信息获取局部时空特征的梯度直方图（Histogram of Oriented Gradients，HOG），使用不同类型的轨迹的光流直方图（Histogram of Optical Flow, HOF）和运动边界直方图（Motion Boundary Histogram，MBH）。通过词袋模型或Fisher向量方法，这些特征可以生成视频编码。

当前，基于轨迹的方法（尤其是DT和IDT）是最高水平的人工设计特征算法的基础[[2](https://zhuanlan.zhihu.com/p/28179049/edit#_ENREF_2)]。许多研究者正在尝试改进IDT，如通过增加字典的大小和融合多种编码方法，通过开发子采样方法生成DT特征的字典，在许多人体行为数据集上取得了不错的性能。

然而，随着深度神经网络的兴起，特别是CNN、LSTM、GRU等在视频分类中的成功应用，其分类性能逐渐超越了基于DT和IDT的传统方法，使得这些传统方法逐渐淡出了人们的视野。



**二、** **深度网络方法研究**

**深度网络为解决大规模视频分类问题提供了新的思路和方法。**近年来得益于深度学习研究的巨大进展，特别是卷积神经网络（Convolutional Neural Networks, CNN）作为一种理解图像内容的有效模型，在图像识别、分割、检测和检索等方面取得了最高水平的研究成果。卷积神经网络CNN在静态图像识别问题中取得了空前的成功，其中包括MNIST、CIFAR和ImageNet大规模视觉识别挑战问题。CNN采用卷积与池化操作，可以自动学习图像中包含的复杂特征，在视觉对象识别任务中表现出很好的性能。基于CNN这些研究成果，国内外开始研究将CNN等深度网络应用到视频和行为分类任务中。

与图像识别相比，视频分类任务中视频比静态图像可以提供更多的信息，包括随时间演化的复杂运动信息等。视频（即使是短视频）中包含成百上千帧图像，但并不是所有图像都有用，处理这些帧图像需要大量的计算。最简单的方法是将这些视频帧视为一张张静态图像，应用CNN识别每一帧，然后对预测结果进行平均处理来作为该视频的最终结果。然而，这个方法使用了不完整的视频信息，因此使得分类器可能容易发生混乱。

**（1）** **监督学习方法**

*i.* 基于图像的视频分类：将视频片段视为视频帧的集合，每个视频帧的特征通过ImageNet数据集上预先训练的最高水平的深度模型（如*AlexNet，VGGNet，GoogLeNet，ResNet*）进行获取*。最终，帧层特征汇聚为视频层特征，作为标准分类器（如SVM）识别的输入。*

ii. 端到端的CNN网络：关注于利用CNN模型学习视频隐含的时空模式，如3D CNN，Two-stream CNN，TSN模型等。

iii. 双流（Two-stream）法中的时间CNN只能获取很短时间窗口内的运动信息，难以处理长时间多种行为组成的复杂事件和行为。因此，**引入RNN来建模长期时间动态过程**，常用的模型有LSTM，GRU-RNN等。LSTM避免了梯度消失的问题，在许多图像和视频摘要、语音分析任务中非常有效。

iv. 视频中包含了很多帧，处理所有的视频帧计算代价很大，也会降低识别那些与类别相关的视频帧的性能。因此，**引入视觉注意力机制**来识别那些与目标语义直接相关的最有判别力的时空特征

**（2）** **非监督学习方法**

采用非监督学习的方法，整合空间和时间上下文信息，是发现和描述视频结构的一种很有前途的方法。







# datasets

近年来为推动视频分类的研究，也陆续发布了相关的视频数据集。小型标注良好的数据集如KTH，Hollywood2，Weizmann；中型的数据集如UCF101，Thumos’14和HMDB51，这些数据集超过了50类行为类别；大型数据集如Sports-1M，YFCC-100M，FCVID数据集，ActivityNet数据集，YouTube-8M等。

![Deep Learning Video Classification and Captioning](02-DeepLearning Video Classification and Captioning/Deep Learning Video Classification and Captioning.jpg)

其中比较有代表性的有YouTube-8M（2016）、ActivityNet（2015）、Sports-1M（2014）、**UCF-101（2012）、HMDB51（2011）**等。

**YouTube-8M的提出标志着视频分类朝大规模通用多标签视频分类的方向发展。**

当前的研究结果表明：

- HMDB51数据集上，DOVF+MIFS方法最高水平的准确度为75%，在该数据集上还有较大的性能提升空间[[3](https://zhuanlan.zhihu.com/p/28179049/edit#_ENREF_3)];
- UCF101数据集上，TLE方法达到最高水平的准确率为95.6%[[4](https://zhuanlan.zhihu.com/p/28179049/edit#_ENREF_4)]。