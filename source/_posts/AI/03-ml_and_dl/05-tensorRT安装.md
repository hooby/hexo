---
title: tensorRT安装
layout: page
date: 2018/7/15
updated: 2018/9/13
comments: false
tags: 
- tensorRT
categories: 
- tensorRT
toc: true
---

<!-- toc -->

[TOC]

## tensorRT简介

深度学习的发展带动了一批深度学习框架，caffe、tensorflow、pytorch等，对于计算量庞大的CNN，效率一直是大家所关注的，接触过深度网络压缩的同学应该知道网络压缩最关键的两个思路，剪枝和量化。
TensorRT就是量化，将FP32位权值数据优化为 FP16 或者 INT8，而推理精度不发生明显的降低。
关于TensorRT首先要清楚以下几点：

1. TensorRT是NVIDIA开发的深度学习推理工具，只支持推理，不支持训练；
   目前TensorRT4已经支持Caffe、Caffe2、TensorFlow、MxNet、Pytorch等主流深度学习库；

2. TensorRT底层针对NVIDIA显卡做了多方面的优化，不仅仅是量化，可以和 CUDA CODEC SDK 结合使用，也就是另一个开发包DeepStream；

3. TensorRT独立于深度学习框架，通过解析框架文件来实现，不需要额外安装DL库；

    参考示意图：

    ![tensorrt_01](05-tensorRT安装/tensorrt_01.png)



## tensorRT安装

1、安装tensorRT之前准备





