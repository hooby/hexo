---
title: Logistic Regression
layout: page
date: 2018/5/3
updated: 2018/6/13
comments: false
tags: 
- coursera
categories: 
- ML
toc: true
---

<!-- toc -->

[TOC]

# 1. Classification and Representation

使用线性回归来处理 0/1 分类问题总是困难重重的，如下两图，在第一幅图中，拟合曲线成功的区分了 0、1 两类，在第二幅图中，如果我们新增了一个输入（右上的 **X** 所示），此时拟合曲线发生变化，由第一幅图中的**紫色线**旋转到第二幅图的**蓝色线**，导致本应被视作 1 类的 X 被误分为了 0 类：

![线性回归处理分类问题1](04_Logistic_Regression/LR_deal_classification_01.png)

![线性回归处理分类问题2](04_Logistic_Regression/LR_deal_classification_02.png)

因此，人们定义了逻辑回归来完成 0/1 分类问题。

## 1.1 sigmoid

用线性回归预测函数hθ(x)来处理回归问题不太有效，特别对于0/1分类问题。新的逻辑回归预测函数

![回归预测函数](04_Logistic_Regression/Hypothesis_Representation.png)

g(z) 称之为 Sigmoid Function，亦称 Logic Function，其函数图像如下：

![sigmoid](04_Logistic_Regression/sigmoid_function.png)



预测函数hθ(x) 被很好地限制在0、1之间。阈值为 0.5，大于则为 1 类，反之为 0 类。函数曲线过渡光滑自然。

## 1.2 Decision Boundary

决策边界，顾名思义，就是用来**划清界限**的边界，边界的形态可以不定，可以是点，可以是线，也可以是平面。Andrew Ng 在公开课中强调：**“决策边界是预测函数 hθ(x)hθ(x) 的属性，而不是训练集属性”**，这是因为能作出“划清”类间界限的只有 hθ(x)，而训练集只是用来训练和调节参数的。

- **线性决策边界**：

  ![线性决策边界](04_Logistic_Regression/liner_decision_boundary.png)

- **非线性决策边界**：

  ![非线性决策边界](04_Logistic_Regression/no_liner_decision_boundary.png)



# 2. Logistic Regression Model

## 2.1 Cost Function

![cost_function](04_Logistic_Regression/cost_function.png)

当y=0时：

![cost_function_y0](/home/alex/data_files/github/hexo/source/_posts/ML_coursera/04_Logistic_Regression/cost_function_y0.png)

当y=1时：

![cost_function_y1](04_Logistic_Regression/cost_function_y1.png)



可以看到，当 hθ(x)≈y 时，cost≈0，预测正确。

![cost_function_y1](04_Logistic_Regression/cost_function_formula.png)

## 2.2 Gradient Descent

首先了解下sigmoid函数的倒数：

![partial_derivative_of_sigmoid](04_Logistic_Regression/partial_derivative_of_sigmoid.png)

由此可以得出使用梯度下降来调节 θ 的公式:

![gradient_descent](04_Logistic_Regression/gradient_descent.png)

向量表示

![gradient_descent_v](04_Logistic_Regression/gradient_descent_v.png)



# 3.Multiclass Classification

通常采用 **One-vs-All** 方法来实现多分类，其将**多分类问题**转化为了**多次二分类**问题。假定完成 K 个分类，One-vs-All 的执行过程如下：

1. 轮流选中某一类型 i ，将其视为正样本，即 “1” 分类，剩下样本都看做是负样本，即 “0” 分类。
2. 训练逻辑回归模型得到参数 θ(1),θ(2),...,θ(K)θ(1),θ(2),...,θ(K) ，即总共获得了 K−1 个决策边界。

![multiclass_1](04_Logistic_Regression/multiclass_1.png)

给定输入 xx，为确定其分类，需要分别计算 h(k)θ(x),k=1,...,Khθ(k)(x),k=1,...,K，h(k)θ(x)hθ(k)(x) 越趋近于 1，xx 越接近是第 kk类：

![multiclass_2](04_Logistic_Regression/multiclass_2.png)





