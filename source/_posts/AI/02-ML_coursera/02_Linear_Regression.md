---
title: Linear Regression with Multiple Variables (week2)
layout: page
date: 2018/4/20
updated: 2018/6/13
comments: false
tags: 
- coursera
categories: 
- ML
toc: true
---

<!-- toc -->

[TOC]

# Multiple Features

多个特征的数据

![Multiple_Features_01](02_Linear_Regression/Multiple_Features_01.png)

对于线性回归：

![Multiple_Features_02](02_Linear_Regression/Multiple_Features_02.png)

其中：X0 = 1

## Gradient Descent for Multiple Variables

对于m个样本，n个特征的，

![Gradient_Descent_for_Multiple_Variables_02](02_Linear_Regression/GD_02.png)



![Gradient_Descent_for_Multiple_Variables_01](02_Linear_Regression/GD_01.png)

## Feature Scaling

对训练数据进行特征缩放可以加快训练速度。

有两种帮助的技术是**特征缩放**和**均值归一化**。特征缩放涉及将输入值除以输入变量的范围（即最大值减去最小值），从而产生仅1的新范围。均值归一化涉及从该值中减去输入变量的平均值。输入变量导致输入变量的新平均值仅为零。要实现这两种技术，请调整输入值，如下面的公式所示：

![Multiple_Features_03](02_Linear_Regression/Multiple_Features_03.png)



## Features and Polynomial Regression

特征和多项式回归不一样，We can improve our features and the form of our hypothesis function in a couple different ways.

可以通过组合特征生成新的特征，We can **combine** multiple features into one. For example, we can combine x_1 and x_2 into a new feature x_3 by taking x_1⋅x_2.

可以通过取平方，立方，平方根等方法增加多项式，We can **change the behavior or curve** of our hypothesis function by making it a quadratic, cubic or square root function (or any other form).



# Normal Equation

正规方程：在通过迭代更新参数theta的时候，当偏导数项为0 的时候，就是参数theta的最终值。所以通过这种方式来求得theta值的方法，就是正规方程。

![Normal_Equation_02](02_Linear_Regression/Normal_Equation_02.png)

下面是例子以及跟梯度下降的对比。

![Normal_Equation_01](02_Linear_Regression/Normal_Equation_01.png)

| 梯度下降                                                     | 正规方程                                                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| 需要选择适当的学习率 α                                       | 不要学习率 α                                                 |
| 需要进行多步迭代                                             | 不需要进行迭代，在 Matlab 等平台上，矩阵运算仅需一行代码就可完成 |
| 对多特征适应性较好，能在特征数量很多时仍然工作良好           | 算法复杂度为 O(n3)，所以如果特征维度太高（特别是超过 10000 维），那么不宜再考虑该方法。 |
| 能应用到一些更加复杂的算法中，如逻辑回归（Logic Regression）等 | 矩阵需要可逆，并且，对于一些更复杂的算法，该方法无法工作     |



注意：

![Normal_Equation_03](02_Linear_Regression/Normal_Equation_03.png)

