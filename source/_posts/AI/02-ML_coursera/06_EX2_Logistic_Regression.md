---
title: Programming Exercise 2 Logistic Regression
layout: page
date: 2018/5/15
updated: 2018/6/13
comments: false
mathjax: true
tags: 
- coursera
categories: 
- ML
toc: true
---

<!-- toc -->

[TOC]

# 1. Introduction

任务所要用到的文件
ex2.m - 运行主文件1
ex2 reg.m - 运行主文件1
ex2data1.txt - 主文件1训练数据
ex2data2.txt - 主文件2训练数据
submit.m - 提交任务函数文件
mapFeature.m - Function to generate polynomial features
plotDecisionBoundary.m - Function to plot classifier’s decision bound-
ary
[?] plotData.m - Function to plot 2D classification data
[?] sigmoid.m - Sigmoid Function
[?] costFunction.m - Logistic Regression Cost Function
[?] predict.m - Logistic Regression Prediction Function
[?] costFunctionReg.m - Regularized Logistic Regression Cost
?  表示完成任务所要修改的文件



# 2. Logistic Regression

该任务要求创建一个逻辑回归模型用来判断学生能否拿到大学录取通知书。

`ex2data1.txt` 文件中记录了之前的学生成绩(两门课的分数)和录取情况(1/0)， 根据该数据判断新生的录取。

## 2.1 Visualizing the data

数据可视化









# 3. Regularized logistic regression

