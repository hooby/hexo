---
title: nvidia jetson TX2 刷机
layout: page
date: 2018/4/7
updated: 2018/6/13
comments: false
tags: 
- tx2 jetson
categories: 
- tx2
toc: true
---

<!-- toc -->

[TOC]

TX2 出厂时，已经自带了 Ubuntu 16.04 系统，可以直接启动。但一般我们会选择刷机，目的是更新到最新的 JetPack L4T，并自动安装最新的驱动、CUDA Toolkit、cuDNN、TensorRT。

刷机注意以下几点：

- **开发板刷机过程全程联网**
- 除了Jetson TX2之外，**您还需要另一台带有Intel或AMD x86处理器的台式机或笔记本电脑。（所以自己要是win电脑，要安装VMware 虚拟机，并在虚拟机上安装Ubuntu 的操作系统，我安装的是Ubuntu -16.04.3 64 位）**
- 这些类型的机器通常被称为PC的个人电脑。该计算机被称为烧录过程的主机。
- **JetPack是一个x86二进制文件，不能在基于ARM的机器上运行**。

------

先推荐一个国外刷机技术视频：<https://v.qq.com/x/page/b0515967lbr.html>
这个视频挺好，虽然是JetPack 3.0刷机，但步骤一模一样。跟着他的步骤刷机就可以，不过有些地方可能不够详细，可以自己先看一遍，有个大致印象。

## 1. 从官网下载JetPack3.1

下载地址为[jetpack](https://developer.nvidia.com/embedded/jetpack)， 理论上此处下载需要NVIDIA的帐号。


### 安装JetPack3.1

- 现在把刚下载的软件包上传的虚拟机中Ubuntu主机中，**可以通过共享文件夹的形式，将JetPack3.1从win电脑copy到vmware虚拟机中的ununtu**，这个方法大家可以网上搜下，这里不做赘述。
- 然后在**虚拟机上，终端定位到Jetpack3.1所在文件夹**
  更改执行权限：
  `$ chmod +x ./JetPack-L4T-3.1-linux-x64.run`
  执行安装
  `$ sudo ./JetPack-L4T-3.1-linux-x64.run`

然后进入了软件界面，

![img](Nvidia_Jetson_TX2_install_ubuntu/jetpack_1_begin_001.png)



![jetpack_1_begin_002](Nvidia_Jetson_TX2_install_ubuntu/jetpack_1_begin_002.png)

- 选择板子

  ![jetpack_1_begin_003](Nvidia_Jetson_TX2_install_ubuntu/jetpack_1_begin_003.png)


- 随后进入了component manager界面，如图所示：

![jetpack_1_begin_004](Nvidia_Jetson_TX2_install_ubuntu/jetpack_1_begin_004.png)



![jetpack_1_begin_005](Nvidia_Jetson_TX2_install_ubuntu/jetpack_1_begin_005.png)



- 成功后，就要下载各种包了







![jetpack_1_begin_006](Nvidia_Jetson_TX2_install_ubuntu/jetpack_1_begin_006.png)









![jetpack_1_begin_007](Nvidia_Jetson_TX2_install_ubuntu/jetpack_1_begin_007.png)





![jetpack_1_begin_008](Nvidia_Jetson_TX2_install_ubuntu/jetpack_1_begin_008.png)

连接方式

![jetpack_1_begin_009](Nvidia_Jetson_TX2_install_ubuntu/jetpack_1_begin_009.png)





![jetpack_1_begin_010](Nvidia_Jetson_TX2_install_ubuntu/jetpack_1_begin_010.png)





![jetpack_1_begin_011](Nvidia_Jetson_TX2_install_ubuntu/jetpack_1_begin_011.png)





![jetpack_1_begin_012](Nvidia_Jetson_TX2_install_ubuntu/jetpack_1_begin_012.png)



![jetpack_1_begin_013](Nvidia_Jetson_TX2_install_ubuntu/jetpack_1_begin_013.png)







![jetpack_1_begin_014](Nvidia_Jetson_TX2_install_ubuntu/jetpack_1_begin_014.png)

