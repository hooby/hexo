---
title: mask rcnn 数据基础介绍
layout: page
date: 2018/8/13
updated: 2018/6/13
comments: false
tags: 
- mask rcnn
categories: 
- deep learning
toc: true
---

<!-- toc -->

[TOC]





# 概念理解

Mask R-CNN (Regional Convolutional Neural Network: 区域卷积神经网络)

![ComputerVisionTasks](mask_rcnn/ComputerVisionTasks.png)

- **图像分类 (Classification)**: 图片中有气球。

- **语义分割 (Semantic Segmentation)**: 这些是图片中组成所有气球的像素。

- **目标检测 (Object Detection)**: 这里是图片中7个气球的位置。我们需要识别被遮挡的物体。

- **实例分割 (Instance Segmentation)**: 这是是图片中7个气球的位置，包括组成每一个气球的像素。


mAP: 均值平均精度(Mean Average Precision)

Backbone: 骨干网络 

backbone feature map : 骨干特征图

Feature Pyramid Network (FPN): 特征金字塔网络 

![FPNFlowChart](mask_rcnn/FPNFlowChart.png)

Mask RCNN 的实现采用的是 ResNet101 + FPN 作为骨干网络

Region Proposal Network (RPN):  区域提议网络

Anchor:  锚点, RPN 通过扫描骨干特征图（backbone feature map ） 得到锚点的. SSD 中称为先验框Prior boxes

Non-max Suppression : 非最大抑制, 在 RPN 预测过程中，我们会选择最可能包含物体的锚点，然后去修正它们的位置以及尺寸。如果有些锚点重叠得太多，我们会保留前景得分最高的一个，而其他的锚点就丢弃掉（这种方式成为非最大抑制 Non-max Suppression ）。

 regions of interest (ROI):  最终的提议区域, 也就是我们感兴趣的区域



Ground Truth : 对于物体检测问题，Ground Truth包括图像，图像中的目标的类别以及图像中每个目标的边界框。 



ROI Pooling: 感兴趣区域的池化

**Batch Normalization** : Batch Normalization可以提升模型收敛速度，而且可以起到一定正则化效果，降低模型的过拟合。

BA DA DS ML CS

BA: business analyst 

DA:  data analyst

DS:  data science 

ML: machine learning

CS: computer science

selective search:  选择性搜索

SSD: Single Shot MultiBox Detector

hard negative mining ： 为了保证正负样本尽量平衡，SSD采用了hard negative mining

Locatization loss， loc： 位置误差

confidence loss, conf ： 置信度误差

SSD loss : 损失函数定义为位置误差（locatization loss， loc）与置信度误差（confidence loss, conf）的加权和

Data Augmentation: 数据扩增

Randomly sample a patch:  随机采集块域

dilation converlutional : 扩张卷积

因果卷积: 因果卷积的理解可以认为是：不管是自然语言处理领域中的预测还是时序预测，都要求对时刻t 的预测yt只能通过t时刻之前的输入x1到xt-1来判别。这种思想有点类似于马尔科夫链。

残差卷积的跳层连接: 微软的残差网络 ResNet 就是经典的跳层连接（skip-connection）,上一层的特征图 x 直接与卷积后的 F(x)对齐加和，变为 F(x)+x （特征图数量不够可用 0 特征补齐，特征图大小不一可用带步长卷积做下采样）。

Fully Convolutional Networks :  全卷积网络



