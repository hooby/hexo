---
title: about me
date: 2021-05-25
type: "about"
tag:
copyright: true
comment: true
---

## biography

爱生活，也爱专心搞工作；
爱交友，也喜欢自我钻研；
爱运动，也爱宅家里看书；
爱音乐，也爱追剧看动漫；
That's me，一个简简单单的运维开发工程。

## connect

- **Blog** 🏠 [blog.lsbgb.com](http://blog.lsbgb.com)

- **Email** 📧 [hoobyhoobee](mailto:hoobyhoobee@gmail.com)

- **GitHub** 🕸 [supermanhuyu](https://github.com/supermanhuyu)

