generate:
	singularity exec /atlas/containers/node-slim.img node_modules/hexo-cli/bin/hexo generate

server:
	docker run -it --rm -v "$PWD":"$PWD" -w "$PWD" -p 4000:4000 node:slim node_modules/hexo-cli/bin/hexo server
